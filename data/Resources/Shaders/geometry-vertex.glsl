#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2  TextureSize;
uniform vec4  SourceRectangle;
// uniform float SourceRecSkew;
// uniform vec4  MaskRectangle;
// uniform vec2  ShiftVec;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec3 fWorldPos;
out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fWorldPos = (WorldMatrix * vec4(inPosition, 1.0)).xyz;
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	gl_Position = ProjectionMatrix * ViewMatrix * WorldMatrix * vec4(inPosition, 1.0);
}