#version 330

uniform sampler2D Texture;
uniform vec2      TextureSize;
uniform sampler2D DitherTexture;
uniform vec4      SourceRectangle;
uniform vec4      MaskRectangle;
uniform vec2      ShiftVec;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;
uniform vec4      ReplaceColor1;
uniform vec4      ReplaceColor2;
uniform vec4      ReplaceColor3;
uniform vec4      ReplaceColor4;

uniform bool      GradientEnabled;
uniform float     CircleRadius;
uniform float     CircleInnerRadius;
uniform float     Saturation;
uniform float     Brightness;
uniform float     Time;

in vec4 gl_FragCoord;
in vec3 fWorldPos;
in float fDepth;
in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;
in vec2 fMaskCoord;

layout(location = 0) out vec4 outColor;

vec3 czm_saturation(vec3 rgb, float adjustment)
{
    // Algorithm from Chapter 16 of OpenGL Shading Language
    const vec3 W = vec3(0.2125, 0.7154, 0.0721);
    vec3 intensity = vec3(dot(rgb, W));
    return mix(intensity, rgb, adjustment);
}

float grayscale(vec3 rgb)
{
	return dot(rgb, vec3(0.299, 0.587, 0.114));
}

void main()
{
	vec4 targetColor1 = vec4(240.0/255.0, 110.0/255.0, 170.0/255.0, 1);
	vec4 targetColor2 = vec4(246.0/255.0, 172.0/255.0, 205.0/255.0, 1);
	vec4 targetColor3 = vec4(236.0/255.0,   0.0/255.0, 140.0/255.0, 1);
	vec4 targetColor4 = vec4(240.0/255.0, 136.0/255.0, 184.0/255.0, 1);
	
	// +==============================+
	// |      Apply Circle Mask       |
	// +==============================+
	float alpha = 1;
	if (CircleRadius != 0)
	{
		float distFromCenter = length(fTexCoord - vec2(0.5, 0.5)) * 2;
		float smoothDelta = fwidth(distFromCenter);
		alpha *= smoothstep(CircleRadius, CircleRadius-smoothDelta, distFromCenter);
		if (CircleInnerRadius != 0)
		{
			alpha *= smoothstep(CircleInnerRadius, CircleInnerRadius+smoothDelta, distFromCenter);
		}
		if (alpha < 1/255.0) { discard; }
	}
	
	// +==============================+
	// |          Apply Mask          |
	// +==============================+
	if (MaskRectangle.z > 0 && MaskRectangle.w > 0)
	{
		vec2 realMaskCoord = fMaskCoord + ShiftVec;
		if (realMaskCoord.x >= MaskRectangle.x + MaskRectangle.z + 1) { realMaskCoord.x -= MaskRectangle.z; }
		if (realMaskCoord.x <= MaskRectangle.x - 1) { realMaskCoord.x += MaskRectangle.z; }
		if (realMaskCoord.y >= MaskRectangle.y + MaskRectangle.w + 1) { realMaskCoord.y -= MaskRectangle.w; }
		if (realMaskCoord.y <= MaskRectangle.y - 1) { realMaskCoord.y += MaskRectangle.w; }
		vec4 maskSampleColor = texture(Texture, realMaskCoord / TextureSize);
		// if (maskSampleColor.a < 0.5f) { discard; }
		alpha *= maskSampleColor.a;
		if (alpha < 1/255.0) { discard; }
	}
	
	// +===============================+
	// | Apply ShiftVec to sampleCoord |
	// +===============================+
	vec2 realSampleCoord = fSampleCoord;
	if (ShiftVec.x != 0 || ShiftVec.y != 0) //TODO: This probably doesn't play nicely with the SourceRecSkew
	{
		realSampleCoord = realSampleCoord + ShiftVec;
		if (realSampleCoord.x > SourceRectangle.x + SourceRectangle.z) { discard; }//realSampleCoord.x -= SourceRectangle.z; }
		if (realSampleCoord.x < SourceRectangle.x)                     { discard; }//realSampleCoord.x += SourceRectangle.z; }
		if (realSampleCoord.y > SourceRectangle.y + SourceRectangle.w) { discard; }//realSampleCoord.y -= SourceRectangle.w; }
		if (realSampleCoord.y < SourceRectangle.y)                     { discard; }//realSampleCoord.y += SourceRectangle.w; }
		// realSampleCoord.x = (round(realSampleCoord.x*TextureSize.x)) / TextureSize.x;
		// realSampleCoord.y = (round(realSampleCoord.y*TextureSize.y)) / TextureSize.y;
	}
	vec4 sampleColor = texture(Texture, realSampleCoord / TextureSize);
	sampleColor.a *= alpha;
	
	// +==============================+
	// |        Replace Colors        |
	// +==============================+
	if (sampleColor.a > 250/256.0f)
	{
		if (abs(sampleColor.r - targetColor1.r) < 1/256.0f &&
			abs(sampleColor.g - targetColor1.g) < 1/256.0f &&
			abs(sampleColor.b - targetColor1.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor1;
		}
		else if (abs(sampleColor.r - targetColor2.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor2.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor2.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor2;
		}
		else if (abs(sampleColor.r - targetColor3.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor3.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor3.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor3;
		}
		else if (abs(sampleColor.r - targetColor4.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor4.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor4.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor4;
		}
	}
	
	// +==============================+
	// |        Apply Gradient        |
	// +==============================+
	if (GradientEnabled)
	{
		float gray = (sampleColor.r + sampleColor.g + sampleColor.b) / 3;
		sampleColor = mix(PrimaryColor.rgba, SecondaryColor.rgba, gray);
	}
	else
	{
		sampleColor = PrimaryColor * sampleColor;
	}
	
	// +==============================+
	// |      Calculate outColor      |
	// +==============================+
	outColor = fColor * sampleColor;
	outColor.rgb = czm_saturation(outColor.rgb, Saturation);
	outColor.rgb *= Brightness;
	
	float zFar = 831.384;
	// outColor.rgba = vec4(fDepth/zFar, fDepth/zFar, fDepth/zFar, 1);
	// outColor.rgba = vec4(fDepth, fDepth, fDepth, 1);
	// float zValue = gl_FragCoord.zValue;
	// outColor.rgb = vec3(zValue, zValue, zValue);
	
	// +==============================+
	// |          Dithering           |
	// +==============================+
	//TODO: Somehow check to see if a dither texture was provided?
	// vec4 ditherSample = texture(DitherTexture, fSampleCoord/16); //TODO: Decide where the dither texture should be sampled
	// if (outColor.a < (1-ditherSample.r)) { discard; }
	// else { outColor.a = 1; }
}