#version 330

uniform sampler2D Texture;
uniform vec2      TextureSize;
uniform sampler2D DitherTexture;
uniform vec4      SourceRectangle;
// uniform vec4      MaskRectangle;
// uniform vec2      ShiftVec;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;
uniform vec4      ReplaceColor1;
uniform vec4      ReplaceColor2;
uniform vec4      ReplaceColor3;
uniform vec4      ReplaceColor4;

uniform bool      GradientEnabled;
// uniform float     CircleRadius;
// uniform float     CircleInnerRadius;
uniform float     Saturation;
uniform float     Brightness;
uniform float     Time;

uniform vec4 Shadows[4];//NOTE: w is radius of shadow

in vec4 gl_FragCoord;
in vec3 fWorldPos;
in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 outColor;

vec3 czm_saturation(vec3 rgb, float adjustment)
{
    // Algorithm from Chapter 16 of OpenGL Shading Language
    const vec3 W = vec3(0.2125, 0.7154, 0.0721);
    vec3 intensity = vec3(dot(rgb, W));
    return mix(intensity, rgb, adjustment);
}

float grayscale(vec3 rgb)
{
	return dot(rgb, vec3(0.299, 0.587, 0.114));
}

void main()
{
	vec4 targetColor1 = vec4(240.0/255.0, 110.0/255.0, 170.0/255.0, 1);
	vec4 targetColor2 = vec4(246.0/255.0, 172.0/255.0, 205.0/255.0, 1);
	vec4 targetColor3 = vec4(236.0/255.0,   0.0/255.0, 140.0/255.0, 1);
	vec4 targetColor4 = vec4(240.0/255.0, 136.0/255.0, 184.0/255.0, 1);
	
	vec4 sampleColor = texture(Texture, fSampleCoord / TextureSize);
	
	// +==============================+
	// |        Replace Colors        |
	// +==============================+
	if (sampleColor.a > 250/256.0f)
	{
		if (abs(sampleColor.r - targetColor1.r) < 1/256.0f &&
			abs(sampleColor.g - targetColor1.g) < 1/256.0f &&
			abs(sampleColor.b - targetColor1.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor1;
		}
		else if (abs(sampleColor.r - targetColor2.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor2.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor2.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor2;
		}
		else if (abs(sampleColor.r - targetColor3.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor3.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor3.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor3;
		}
		else if (abs(sampleColor.r - targetColor4.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor4.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor4.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor4;
		}
	}
	
	// +==============================+
	// |        Apply Gradient        |
	// +==============================+
	if (GradientEnabled)
	{
		float gray = (sampleColor.r + sampleColor.g + sampleColor.b) / 3;
		sampleColor = mix(PrimaryColor.rgba, SecondaryColor.rgba, gray);
	}
	else
	{
		sampleColor = PrimaryColor * sampleColor;
	}
	
	for (int i = 0; i < 4; i++) //TODO: Make 4 a NumShadowsConstant
	{
		vec3 shadowPos = Shadows[i].xyz;
		float shadowRadius = Shadows[i].w;
		if (shadowRadius > 0 && fWorldPos.y <= shadowPos.y+0.1)
		{
			if ((fWorldPos.x - shadowPos.x)*(fWorldPos.x - shadowPos.x) + (fWorldPos.z - shadowPos.z)*(fWorldPos.z - shadowPos.z) < shadowRadius*shadowRadius)
			{
				sampleColor.rgb *= 0.5f;
			}
		}
	}
	
	// if (Shadows[0].w > 0)
	// {
	// 	float offsetY = abs(fWorldPos.y - Shadows[0].y);
	// 	// offsetY = round(offsetY/12) * 12;
	// 	// offsetY = offsetY / 48;
	// 	// sampleColor.rgb *= 1 - offsetY;
	// 	if (offsetY >= 0.1) sampleColor.rgb *= 0.6;
	// }
	
	// +==============================+
	// |      Calculate outColor      |
	// +==============================+
	outColor = fColor * sampleColor;
	outColor.rgb = czm_saturation(outColor.rgb, Saturation);
	outColor.rgb *= Brightness;
	
	// +==============================+
	// |          Dithering           |
	// +==============================+
	//TODO: Somehow check to see if a dither texture was provided?
	vec4 ditherSample = texture(DitherTexture, fSampleCoord/16); //TODO: Decide where the dither texture should be sampled
	if (outColor.a < (1-ditherSample.r)) { discard; }
	else { outColor.a = 1; }
}