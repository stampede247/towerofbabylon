/*
File:   app_helpers.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Contains global functions and macros that can be called from the rest of the application 

#included from app.cpp
*/

#define DEBUG_Write(formatStr) do {      \
	if (platform != nullptr &&           \
		platform->DebugWrite != nullptr) \
	{                                    \
		platform->DebugWrite(formatStr); \
	}                                    \
} while (0)

#define DEBUG_WriteLine(formatStr) do {      \
	if (platform != nullptr &&               \
		platform->DebugWriteLine != nullptr) \
	{                                        \
		platform->DebugWriteLine(formatStr); \
	}                                        \
} while (0)

#define DEBUG_Print(formatStr, ...) do {              \
	if (platform != nullptr &&                        \
		platform->DebugPrint != nullptr)              \
	{                                                 \
		platform->DebugPrint(formatStr, __VA_ARGS__); \
	}                                                 \
} while (0)

#define DEBUG_PrintLine(formatStr, ...) do {              \
	if (platform != nullptr &&                            \
		platform->DebugPrintLine != nullptr)              \
	{                                                     \
		platform->DebugPrintLine(formatStr, __VA_ARGS__); \
	}                                                     \
} while (0)

#define HandleButton(button) do { app->buttonHandled[button] = true; } while(0)

#define ButtonPressed(button) ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonPressedUnhandled(button) (app->buttonHandled[button] == false && ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonReleased(button) ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonReleasedUnhandled(button) (app->buttonHandled[button] == false && ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonDown(button) (input->buttons[button].isDown)
#define ButtonDownUnhandled(button) (app->buttonHandled[button] == false && input->buttons[button].isDown)

#define GamepadPressed(padIndex, button) ((input->gamepads[padIndex].buttons[button].isDown && input->gamepads[padIndex].buttons[button].transCount > 0) || input->gamepads[padIndex].buttons[button].transCount >= 2)
// #define GamepadPressedUnhandled(button) (app->buttonHandled[button] == false && ((input->gamepads[padIndex].buttons[button].isDown && input->gamepads[padIndex].buttons[button].transCount > 0) || input->gamepads[padIndex].buttons[button].transCount >= 2))
#define GamepadReleased(padIndex, button) ((!input->gamepads[padIndex].buttons[button].isDown && input->gamepads[padIndex].buttons[button].transCount > 0) || input->gamepads[padIndex].buttons[button].transCount >= 2)
// #define GamepadReleasedUnhandled(button) (app->buttonHandled[button] == false && ((!input->gamepads[padIndex].buttons[button].isDown && input->gamepads[padIndex].buttons[button].transCount > 0) || input->gamepads[padIndex].buttons[button].transCount >= 2))
#define GamepadDown(padIndex, button) (input->gamepads[padIndex].buttons[button].isDown)
// #define GamepadDownUnhandled(button) (app->buttonHandled[button] == false && input->gamepads[padIndex].buttons[button].isDown)

#define ClickedOnRec(rectangle) (ButtonReleasedUnhandled(MouseButton_Left) && IsInsideRec(rectangle, MousePos) && IsInsideRec(rectangle, MouseStartLeft))

// #define BufferPrint(array, formatStr, ...) do                                    \
// {                                                                                \
// 	int snprintfResult = snprintf(array, sizeof(array), formatStr, ##__VA_ARGS__); \
// 	Assert(snprintfResult >= 0 && snprintfResult < sizeof(array));               \
// 	array[snprintfResult] = '\0';                                                \
// } while (0)

char* DupStrN(const char* str, u32 strLength, MemoryArena_t* memoryArenaPntr)
{
	char* result;
	
	result = PushArray(memoryArenaPntr, char, (u32)strLength+1);
	strncpy(result, str, strLength);
	result[strLength] = '\0';
	
	return result;
}
char* DupStr(const char* str, MemoryArena_t* memoryArenaPntr)
{
	u32 strLength = (u32)strlen(str);
	return DupStrN(str, strLength, memoryArenaPntr);
}

char* FormattedTimeStr(RealTime_t realTime)
{
	char* result;
	
	result = TempPrint("%s %u:%02u:%02u%s (%s %s, %u)",
		GetDayOfWeekStr((DayOfWeek_t)realTime.dayOfWeek),
		Convert24HourTo12Hour(realTime.hour), realTime.minute, realTime.second,
		IsPostMeridian(realTime.hour) ? "pm" : "am",
		GetMonthStr((Month_t)realTime.month), GetDayOfMonthString(realTime.day), realTime.year
	);
	
	return result;
}

char* FormattedTimeStr(u64 timestamp)
{
	RealTime_t realTime = {};
	GetRealTime(timestamp, true, &realTime);
	return FormattedTimeStr(realTime);
}

char* GetElapsedString(u64 timespan)
{
	char* result = nullptr;
	
	u32 numDays = (u32)(timespan/(60*60*24));
	u32 numHours = (u32)(timespan/(60*60)) - (numDays*24);
	u32 numMinutes = (u32)(timespan/60) - (numDays*60*24) - (numHours*60);
	u32 numSeconds = (u32)(timespan) - (numDays*60*60*24) - (numHours*60*60) - (numMinutes*60);
	if (numDays > 0)
	{
		result = TempPrint("%ud %uh %um %us", numDays, numHours, numMinutes, numSeconds);
	}
	else if (numHours > 0)
	{
		result = TempPrint("%uh %um %us", numHours, numMinutes, numSeconds);
	}
	else if (numMinutes > 0)
	{
		result = TempPrint("%um %us", numMinutes, numSeconds);
	}
	else
	{
		result = TempPrint("%us", numSeconds);
	}
	
	return result;
}

//Creates all the directories necassary for a given path to exist
void CreateDirectories(const char* directoryPath)
{
	TempPushMark();
	
	u32 strLength = (u32)strlen(directoryPath);
	char* tempString = PushArray(TempArena, char, strLength+1);
	memcpy(tempString, directoryPath, strLength+1);
	
	for (u32 cIndex = 0; tempString[cIndex] != '\0'; cIndex++)
	{
		if (cIndex > 0 && tempString[cIndex] == '\\' || tempString[cIndex] == '/')
		{
			char oldChar = tempString[cIndex];
			tempString[cIndex] = '\0';
			if (!platform->DoesFolderExist(tempString))
			{
				DEBUG_PrintLine("Created \"%s\"", tempString);
				platform->CreateFolder(tempString);
			}
			else
			{
				// DEBUG_PrintLine("\"%s\" already exists", tempString);
			}
			tempString[cIndex] = oldChar;
		}
	}
	
	TempPopMark();
}

r32 Oscillate(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (SinR32(((platform->programTime % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 OscillateSaw(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (SawR32(((platform->programTime % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 Animate(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (platform->programTime % periodMs) / (r32)periodMs;
	return min + (max - min) * lerpValue;
}
