/*
File:   game_room.cpp
Author: Taylor Robbins
Date:   09\05\2018
Description: 
	** Holds functions that handle dealing with Room_t objects.
	** Rooms are the encapsulating level-like entity that contains
	** tiles and entites that can move, interact, and be drawn to the screen.
	** Potentially a room might be serialized and saved in the future as well
*/

void RoomCreate(Room_t* room, MemoryArena_t* memArena, v3i size, u32 expectedNumEntities)
{
	Assert(room != nullptr);
	Assert(memArena != nullptr);
	ClearPointer(room);
	
	room->size = size;
	room->numTiles = 0;
	room->numTilesAlloc = expectedNumEntities;
	room->allocArena = memArena;
	if (room->numTilesAlloc > 0)
	{
		room->tiles = PushArray(memArena, Tile_t, room->numTilesAlloc);
	}
}

void RoomDestroy(Room_t* room)
{
	Assert(room != nullptr);
	if (room->tiles != nullptr)
	{
		Assert(room->allocArena != nullptr);
		ArenaPop(room->allocArena, room->tiles);
		room->tiles = nullptr;
		room->numTilesAlloc = 0;
		room->numTiles = 0;
	}
	if (room->colliders != nullptr)
	{
		Assert(room->allocArena != nullptr);
		ArenaPop(room->allocArena, room->colliders);
		room->colliders = nullptr;
		room->colGridSize = Vec3i_Zero;
	}
	if (room->entities != nullptr)
	{
		Assert(room->allocArena != nullptr);
		ArenaPop(room->allocArena, room->entities);
		room->entities = nullptr;
		room->numEntities = 0;
		room->numEntitiesAlloc = 0;
	}
}

Tile_t* RoomAddTile(Room_t* room)
{
	Assert(room != nullptr);
	Assert(room->allocArena != nullptr);
	Assert(room->numTiles <= room->numTilesAlloc);
	
	if (room->tiles == nullptr || room->numTiles == room->numTilesAlloc)
	{
		u32 newNumTiles = room->numTilesAlloc + TILE_ALLOC_CHUNK_SIZE;
		Tile_t* newSpace = PushArray(room->allocArena, Tile_t, newNumTiles);
		Assert(newSpace != nullptr);
		if (room->tiles != nullptr)
		{
			memcpy(newSpace, room->tiles, sizeof(Tile_t)*room->numTiles);
			ArenaPop(room->allocArena, room->tiles);
		}
		room->tiles = newSpace;
		room->numTilesAlloc = newNumTiles;
	}
	Assert(room->tiles != nullptr && room->numTiles < room->numTilesAlloc);
	
	Tile_t* newTile = &room->tiles[room->numTiles];
	ClearPointer(newTile);
	room->numTiles++;
	
	return newTile;
}

void RoomRemoveTile(Room_t* room, Tile_t* tile)
{
	Assert(room != nullptr);
	Assert(tile != nullptr);
	Assert(room->tiles != nullptr && room->numTiles > 0);
	Assert(tile >= room->tiles && tile < room->tiles + room->numTiles);
	
	u32 tileIndex = (u32)(tile - room->tiles);
	for (u32 tIndex = tileIndex; tIndex+1 < room->numTiles; tIndex++)
	{
		memcpy(&room->tiles[tIndex], &room->tiles[tIndex+1], sizeof(Tile_t));
	}
	room->numTiles--;
}

void RoomRemoveAllTiles(Room_t* room)
{
	Assert(room != nullptr);
	
	room->numTiles = 0; //Lol that's it
}

void EntityInit(Room_t* room, Entity_t* entity, EntityType_t type)
{
	Assert(entity != nullptr);
	ClearPointer(entity);
	entity->type = type;
	
	if (type == EntityType_Player)
	{
		Assert(room->allocArena != nullptr);
		entity->data = PushStruct(room->allocArena, PlayerData_t);
		Assert(entity->data != nullptr);
		entity->dataSize = sizeof(PlayerData_t);
	}
	memset(entity->data, 0x00, entity->dataSize);
}

Entity_t* RoomAddEntity(Room_t* room, EntityType_t type)
{
	Assert(room != nullptr);
	Assert(room->allocArena != nullptr);
	Assert(room->numEntities <= room->numEntitiesAlloc);
	Assert(room->numNewEntities <= room->numNewEntitiesAlloc);
	
	Entity_t* result = nullptr;
	if (room->loopingOverEntities > 0)
	{
		if (room->newEntities == nullptr || room->numNewEntities == room->numNewEntitiesAlloc)
		{
			u32 newNumNewEntities = room->numNewEntitiesAlloc + ENTITY_ALLOC_CHUNK_SIZE;
			Entity_t* newSpace = PushArray(room->allocArena, Entity_t, newNumNewEntities);
			Assert(newSpace != nullptr);
			if (room->newEntities != nullptr)
			{
				memcpy(newSpace, room->newEntities, sizeof(Entity_t)*room->numNewEntities);
				ArenaPop(room->allocArena, room->newEntities);
			}
			room->newEntities = newSpace;
			room->numNewEntitiesAlloc = newNumNewEntities;
		}
		Assert(room->newEntities != nullptr && room->numNewEntities < room->numNewEntitiesAlloc);
		
		result = &room->newEntities[room->numNewEntities];
		room->numNewEntities++;
	}
	else
	{
		if (room->entities == nullptr || room->numEntities == room->numEntitiesAlloc)
		{
			u32 newNumEntities = room->numEntitiesAlloc + ENTITY_ALLOC_CHUNK_SIZE;
			Entity_t* newSpace = PushArray(room->allocArena, Entity_t, newNumEntities);
			Assert(newSpace != nullptr);
			if (room->entities != nullptr)
			{
				memcpy(newSpace, room->entities, sizeof(Entity_t)*room->numEntities);
				ArenaPop(room->allocArena, room->entities);
			}
			room->entities = newSpace;
			room->numEntitiesAlloc = newNumEntities;
		}
		Assert(room->entities != nullptr && room->numEntities < room->numEntitiesAlloc);
		
		result = &room->entities[room->numEntities];
		room->numEntities++;
	}
	
	Assert(result != nullptr);
	EntityInit(room, result, type);
	
	return result;
}

Entity_t* RoomAddExistingEntity(Room_t* room, const Entity_t* entity)
{
	Assert(room != nullptr);
	Assert(entity != nullptr);
	Entity_t* result = RoomAddEntity(room, entity->type);
	
	Assert(result->type == entity->type);
	Assert(result->dataSize == entity->dataSize);
	void* dataPntr = result->data;
	memcpy(result, entity, sizeof(Entity_t));
	result->data = dataPntr;
	if (entity->dataSize > 0)
	{
		memcpy(result->data, entity->data, entity->dataSize);
	}
	
	return result;
}

bool RoomHasEntity(Room_t* room, Entity_t* entity)
{
	Assert(room != nullptr);
	Assert(entity != nullptr);
	if (entity >= room->entities && entity < room->entities + room->numEntities)
	{
		return true;
	}
	else if (entity >= room->newEntities && entity < room->newEntities + room->numNewEntities)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void RoomRemoveEntity(Room_t* room, Entity_t* entity)
{
	Assert(room != nullptr);
	Assert(entity != nullptr);
	
	if (entity >= room->entities && entity < room->entities + room->numEntities)
	{
		Assert(room->entities != nullptr && room->numEntities > 0);
		u32 entityIndex = (u32)(entity - room->entities);
		for (u32 eIndex = entityIndex; eIndex+1 < room->numEntities; eIndex++)
		{
			memcpy(&room->entities[eIndex], &room->entities[eIndex+1], sizeof(Entity_t));
		}
		room->numEntities--;
	}
	else if (entity >= room->newEntities && entity < room->newEntities + room->numNewEntities)
	{
		Assert(room->newEntities != nullptr && room->numNewEntities > 0);
		u32 entityIndex = (u32)(entity - room->newEntities);
		for (u32 eIndex = entityIndex; eIndex+1 < room->numNewEntities; eIndex++)
		{
			memcpy(&room->newEntities[eIndex], &room->newEntities[eIndex+1], sizeof(Entity_t));
		}
		room->numNewEntities--;
	}
	else
	{
		Assert(false);
	}
}

void RoomRemoveAllEntities(Room_t* room)
{
	Assert(room != nullptr);
	room->numEntities = 0; //Lol that's it
	room->numNewEntities = 0;
}

void RoomRemoveDeadEntities(Room_t* room)
{
	//TODO: Implement this function
}

void RoomAssimilateEntities(Room_t* room)
{
	Assert(room != nullptr);
	Assert(room->loopingOverEntities == 0);
	if (room->numNewEntities == 0) { return; }
	Assert(room->allocArena != nullptr);
	
	if (room->numEntities + room->numNewEntities > room->numEntitiesAlloc)
	{
		u32 newNumEntities = room->numEntitiesAlloc;
		while (newNumEntities < room->numEntities + room->numNewEntities) { newNumEntities += ENTITY_ALLOC_CHUNK_SIZE; }
		Entity_t* newSpace = PushArray(room->allocArena, Entity_t, newNumEntities);
		if (room->numEntities > 0)
		{
			memcpy(newSpace, room->entities, sizeof(Entity_t) * room->numEntities);
		}
		ArenaPop(room->allocArena, room->entities);
		room->entities = newSpace;
		room->numEntitiesAlloc = newNumEntities;
	}
	Assert(room->numEntities + room->numNewEntities <= room->numEntitiesAlloc);
	MyMemCopy(&room->entities[room->numEntities], room->newEntities, sizeof(Entity_t) * room->numNewEntities);
	room->numEntities += room->numNewEntities;
	room->numNewEntities = 0;
}

Tile_t* RoomGetTile(Room_t* room, v3 position, u32 index = 0)
{
	u32 foundIndex = 0;
	for (u32 tIndex = 0; tIndex < room->numTiles; tIndex++)
	{
		Tile_t* tile = &room->tiles[tIndex];
		if (position.x >= tile->position.x && position.x < tile->position.x + tile->size.width &&
			position.y >= tile->position.y && position.y < tile->position.y + tile->size.height &&
			position.z >= tile->position.z && position.z < tile->position.z + tile->size.depth)
		{
			if (foundIndex >= index) { return tile; }
			foundIndex++;
		}
	}
	return nullptr;
}

void RoomApplyViewMatrix(Room_t* room, r32 roomPosZ) //back side
{
	Assert(room != nullptr);
	r32 heightScale = (r32)TILE_HEIGHT / (r32)TILE_SIZE;
	r32 zNear = roomPosZ;
	r32 zFar = roomPosZ + (r32)room->size.depth;
	//NOTE: This matrix is used to accomplish these desired effects:
	// x' = x
	// y' = z - heightScale*y
	// z' = 1 + z/(zNear-zFar) - zNear/(zNear-zFar)
	mat4 viewMatrix = NewMat4(
		1,     0,                 0,                 0,
		0,  -heightScale,         1,                 0,
		0,     0,           1/(zNear - zFar),  1 - (zNear / (zNear - zFar)),
		0,     0,                 0,                 1
	);
	RcSetViewMatrix(viewMatrix);
}

v2 RoomPosToWorldPos(Room_t* room, v3 roomPos)
{
	v2 result = {};
	r32 heightScale = (r32)TILE_HEIGHT / (r32)TILE_SIZE;
	result.x = roomPos.x;
	result.y = roomPos.z - heightScale * roomPos.y;
	return result;
}

v3 WorldPosToRoomPos(Room_t* room, v2 worldPos, r32 roomY)
{
	v3 result = {};
	r32 heightScale = (r32)TILE_HEIGHT / (r32)TILE_SIZE;
	result.x = worldPos.x;
	result.y = roomY;
	result.z = heightScale*roomY + worldPos.y;
	return result;
}

u32 RoomGetColliderIndex(Room_t* room, v3i gridPos)
{
	Assert(room != nullptr);
	if (gridPos.x < 0 || gridPos.x >= room->colGridSize.width ||
		gridPos.y < 0 || gridPos.y >= room->colGridSize.height ||
		gridPos.z < 0 || gridPos.z >= room->colGridSize.depth)
	{
		return 0;
	}
	u32 result = (u32)(gridPos.x + (gridPos.z * room->colGridSize.width) + (gridPos.y * room->colGridSize.width * room->colGridSize.depth));
	return result;
}

Collider_t* RoomGetCollider(Room_t* room, v3i gridPos)
{
	Assert(room != nullptr);
	if (room->colliders == nullptr) { return nullptr; }
	if (gridPos.x < 0 || gridPos.x >= room->colGridSize.width ||
		gridPos.y < 0 || gridPos.y >= room->colGridSize.height ||
		gridPos.z < 0 || gridPos.z >= room->colGridSize.depth)
	{
		return nullptr;
	}
	
	Collider_t* result = &room->colliders[gridPos.x + (gridPos.z * room->colGridSize.width) + (gridPos.y * room->colGridSize.width * room->colGridSize.depth)];
	return result;
}

void RoomFillCollider(Room_t* room, v3i gridPos, bool updateSides = true, bool updateAdjacentSides = true)
{
	Collider_t* colPntr = RoomGetCollider(room, gridPos);
	Assert(colPntr != nullptr);
	
	colPntr->solid = true;
	colPntr->sides = SideBit_All;
	for (u8 side = 0; side < Side_NumSides; side++)
	{
		v3i adjGridPos = gridPos + GetSideVec(side);
		Collider_t* adjColPntr = RoomGetCollider(room, adjGridPos);
		if (updateSides && adjColPntr != nullptr && adjColPntr->solid)
		{
			FlagUnset(colPntr->sides, GetSideBit(side));
		}
		if (updateAdjacentSides && adjColPntr != nullptr)
		{
			FlagUnset(adjColPntr->sides, GetSideBit(GetSideOpposite(side)));
		}
	}
}

void RoomRemoveCollider(Room_t* room, v3i gridPos, bool updateAdjacentSides = true)
{
	Collider_t* colPntr = RoomGetCollider(room, gridPos);
	Assert(colPntr != nullptr);
	
	colPntr->solid = false;
	colPntr->sides = 0x00;
	if (updateAdjacentSides)
	{
		for (u8 side = 0; side < Side_NumSides; side++)
		{
			v3i adjGridPos = gridPos + GetSideVec(side);
			Collider_t* adjColPntr = RoomGetCollider(room, adjGridPos);
			if (adjColPntr != nullptr)
			{
				FlagSet(adjColPntr->sides, GetSideBit(GetSideOpposite(side)));
			}
		}
	}
}

Entity_t* GetPlayer(Room_t* room, u32 playerIndex = 0)
{
	Assert(room != nullptr);
	u32 pIndex = 0;
	for (u32 eIndex = 0; eIndex < room->numEntities; eIndex++)
	{
		if (room->entities[eIndex].type == EntityType_Player)
		{
			if (pIndex >= playerIndex)
			{
				return &room->entities[eIndex];
			}
			pIndex++;
		}
	}
	return nullptr;
}

PlayerData_t* GetPlayerData(Entity_t* entity)
{
	Assert(entity != nullptr);
	Assert(entity->type == EntityType_Player);
	Assert(entity->data != nullptr && entity->dataSize == sizeof(PlayerData_t));
	return (PlayerData_t*)entity->data;
}

PlayerData_t* GetPlayerData(Room_t* room, u32 playerIndex = 0)
{
	Assert(room != nullptr);
	Entity_t* entity = GetPlayer(room, playerIndex);
	if (entity == nullptr) { return nullptr; }
	return GetPlayerData(entity);
}
