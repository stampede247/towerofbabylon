/*
File:   game_collision.h
Author: Taylor Robbins
Date:   09\10\2018
Description:
	** Included from game.h
*/

#ifndef _GAME_COLLISION_H
#define _GAME_COLLISION_H

enum //Side_
{
	SideBit_Left      = 0x01,
	SideBit_Right     = 0x02,
	SideBit_Bottom    = 0x04,
	SideBit_Top       = 0x08,
	SideBit_Back      = 0x10,
	SideBit_Front     = 0x20,
	SideBit_All       = 0x3F,
	
	Side_Left = 0,
	Side_Right,
	Side_Bottom,
	Side_Top,
	Side_Back,
	Side_Front,
	Side_NumSides = 6,
};

u8 GetSideOpposite(u8 side)
{
	switch (side)
	{
		case Side_Left:   return Side_Right;
		case Side_Right:  return Side_Left;
		case Side_Bottom: return Side_Top;
		case Side_Top:    return Side_Bottom;
		case Side_Back:   return Side_Front;
		case Side_Front:  return Side_Back;
		default: Assert(false); return 0x00;
	};
}
u8 GetSideBit(u8 side)
{
	switch (side)
	{
		case Side_Left:   return SideBit_Left;
		case Side_Right:  return SideBit_Right;
		case Side_Bottom: return SideBit_Bottom;
		case Side_Top:    return SideBit_Top;
		case Side_Back:   return SideBit_Back;
		case Side_Front:  return SideBit_Front;
		default: Assert(false); return 0x00;
	};
}
v3i GetSideVec(u8 side)
{
	switch (side)
	{
		case Side_Left:   return Vec3i_Left;
		case Side_Right:  return Vec3i_Right;
		case Side_Bottom: return Vec3i_Bottom;
		case Side_Top:    return Vec3i_Top;
		case Side_Back:   return Vec3i_Back;
		case Side_Front:  return Vec3i_Front;
		default: Assert(false); return Vec3i_Zero;
	};
}
u8 GetRotatedSide(u8 side, Dir2_t rotation)
{
	switch (side)
	{
		case Side_Left:   if (rotation == Dir2_Right) { return Side_Left; } else if (rotation == Dir2_Down) { return Side_Back; } else if (rotation == Dir2_Left) { return Side_Right; } else if (rotation == Dir2_Up) { return Side_Front; } else { Assert(false); return side; }
		case Side_Right:  if (rotation == Dir2_Right) { return Side_Right; } else if (rotation == Dir2_Down) { return Side_Front; } else if (rotation == Dir2_Left) { return Side_Left; } else if (rotation == Dir2_Up) { return Side_Back; } else { Assert(false); return side; }
		case Side_Bottom: return Side_Bottom;
		case Side_Top:    return Side_Top;
		case Side_Back:   if (rotation == Dir2_Right) { return Side_Back; } else if (rotation == Dir2_Down) { return Side_Right; } else if (rotation == Dir2_Left) { return Side_Front; } else if (rotation == Dir2_Up) { return Side_Left; } else { Assert(false); return side; }
		case Side_Front:  if (rotation == Dir2_Right) { return Side_Front; } else if (rotation == Dir2_Down) { return Side_Left; } else if (rotation == Dir2_Left) { return Side_Back; } else if (rotation == Dir2_Up) { return Side_Right; } else { Assert(false); return side; }
		default: Assert(false); return side;
	};
}

struct Collider_t
{
	bool solid;
	u8 sides;
};

#endif //  _GAME_COLLISION_H
