/*
File:   game_tower.cpp
Author: Taylor Robbins
Date:   10\13\2018
Description: 
	** Towers are collections of rooms that are stacked on top of eachother 
*/

void TowerCreate(Tower_t* tower, MemoryArena_t* arenaPntr, u32 numRooms, v3i roomSize, u32 numElevators)
{
	Assert(tower != nullptr);
	ClearPointer(tower);
	
	tower->allocArena = arenaPntr;
	tower->rooms = PushArray(arenaPntr, Room_t, numRooms);
	tower->roomFades = PushArray(arenaPntr, r32, numRooms);
	tower->numRooms = numRooms;
	tower->roomSize = roomSize;
	for (u32 rIndex = 0; rIndex < numRooms; rIndex++)
	{
		RoomCreate(&tower->rooms[rIndex], arenaPntr, roomSize, 0);
		tower->roomFades[rIndex] = 0.0f;
	}
	
	tower->numElevators = numElevators;
	if (tower->numElevators > 0)
	{
		tower->elevators = PushArray(arenaPntr, Elevator_t, tower->numElevators);
		memset(tower->elevators, 0x00, sizeof(Elevator_t) * tower->numElevators);
	}
	
	tower->focusRoom = 0;
}

void TowerDestroy(Tower_t* tower)
{
	Assert(tower != nullptr);
	if (tower->rooms != nullptr)
	{
		Assert(tower->allocArena != nullptr);
		for (u32 rIndex = 0; rIndex < tower->numRooms; rIndex++)
		{
			RoomDestroy(&tower->rooms[rIndex]);
		}
		ArenaPop(tower->allocArena, tower->rooms);
		ArenaPop(tower->allocArena, tower->roomFades);
		tower->rooms = nullptr;
		tower->roomFades = nullptr;
		tower->numRooms = 0;
	}
}

Room_t* GetCurrentRoom(Tower_t* tower)
{
	Assert(tower != nullptr);
	Assert(tower->focusRoom < tower->numRooms);
	Assert(tower->rooms != nullptr);
	return &tower->rooms[tower->focusRoom];
}

r32 TowerGetRoomHeight(Tower_t* tower, u32 roomIndex)
{
	Assert(tower != nullptr);
	Assert(roomIndex <= tower->numRooms);
	Assert(tower->rooms != nullptr);
	
	r32 result = 0.0f;
	for (u32 rIndex = 0; rIndex < roomIndex; rIndex++)
	{
		Room_t* roomPntr = &tower->rooms[rIndex];
		result += roomPntr->size.height;
	}
	return result;
}

Entity_t* TowerMoveEntityToRoom(Tower_t* tower, Entity_t* entity, u32 fromRoomIndex, u32 toRoomIndex)
{
	Assert(tower != nullptr);
	Assert(entity != nullptr);
	Assert(fromRoomIndex < tower->numRooms);
	Assert(toRoomIndex < tower->numRooms);
	Assert(RoomHasEntity(&tower->rooms[fromRoomIndex], entity));
	
	Room_t* fromRoom = &tower->rooms[fromRoomIndex];
	Room_t* toRoom = &tower->rooms[toRoomIndex];
	
	Entity_t* newEntity = RoomAddExistingEntity(toRoom, entity);
	newEntity->position.y += TowerGetRoomHeight(tower, fromRoomIndex) - TowerGetRoomHeight(tower, toRoomIndex);
	
	RoomRemoveEntity(fromRoom, entity);
	
	return newEntity;
}

void TowerUpdateRoomFades(Tower_t* tower)
{
	Assert(tower != nullptr);
	for (u32 rIndex = 0; rIndex < tower->numRooms; rIndex++)
	{
		Room_t* room = &tower->rooms[rIndex];
		r32* fadePntr = &tower->roomFades[rIndex];
		bool shouldShowRoom = false;
		if (rIndex == tower->focusRoom) { shouldShowRoom = true; }
		//TODO: Anything else that should queue the fading in of a room before it becomes the active room?
		
		if (shouldShowRoom && *fadePntr < 1.0f)
		{
			*fadePntr += ROOM_FADE_IN_SPEED;
			if (*fadePntr >= 1.0f)
			{
				DEBUG_PrintLine("Room %u fade in finished", rIndex);
				*fadePntr = 1.0f;
			}
		}
		if (!shouldShowRoom && *fadePntr > 0.0f)
		{
			*fadePntr -= ROOM_FADE_OUT_SPEED;
			if (*fadePntr <= 0.0f)
			{
				DEBUG_PrintLine("Room %u fade out finished", rIndex);
				*fadePntr = 0.0f;
			}
		}
	}
}

Entity_t* TowerGetPlayer(Tower_t* tower, u32 playerIndex = 0, u32* roomIndexOut = nullptr)
{
	Assert(tower != nullptr);
	u32 foundIndex = 0;
	for (u32 rIndex = 0; rIndex < tower->numRooms; rIndex++)
	{
		Room_t* room = &tower->rooms[rIndex];
		for (u32 eIndex = 0; eIndex < room->numEntities; eIndex++)
		{
			Entity_t* entity = &room->entities[eIndex];
			if (IsFlagSet(entity->flags, EntityFlag_Alive) && entity->type == EntityType_Player)
			{
				if (foundIndex >= playerIndex)
				{
					if (roomIndexOut != nullptr) { *roomIndexOut = rIndex; }
					return entity;
				}
				foundIndex++;
			}
		}
		
		for (u32 eIndex = 0; eIndex < room->numNewEntities; eIndex++)
		{
			Entity_t* entity = &room->newEntities[eIndex];
			if (IsFlagSet(entity->flags, EntityFlag_Alive) && entity->type == EntityType_Player)
			{
				if (foundIndex >= playerIndex)
				{
					if (roomIndexOut != nullptr) { *roomIndexOut = rIndex; }
					return entity;
				}
				foundIndex++;
			}
		}
	}
	return nullptr;
}

bool TowerAreRoomsTransitioning(Tower_t* tower)
{
	Assert(tower != nullptr);
	for (u32 rIndex = 0; rIndex < tower->numRooms; rIndex++)
	{
		if (tower->roomFades[rIndex] > 0.0f && tower->roomFades[rIndex] < 1.0f)
		{
			return true;
		}
	}
	return false;
}

void TowerDrawRooms(Tower_t* tower, v3 position, bool draw3d = false)
{
	Assert(tower != nullptr);
	
	for (u32 rIndex = 0; rIndex < tower->numRooms; rIndex++)
	{
		Room_t* room = &tower->rooms[rIndex];
		r32 fade = tower->roomFades[rIndex];
		v3 roomPos = position + NewVec3(0, TowerGetRoomHeight(tower, rIndex), 0);
		
		if (fade > 0.0f)
		{
			if (!draw3d) { RoomApplyViewMatrix(room, position.z); }
			
			RcBindShader(&app->geomShader);
			if (game->drawColliders) { RoomDrawColliders(room, roomPos, fade, draw3d); }
			else { RoomDrawTiles(room, roomPos, fade, draw3d); }
			
			RcBindShader(&app->mainShader);
			RoomDrawEntities(room, roomPos, fade, draw3d);
		}
	}
}
