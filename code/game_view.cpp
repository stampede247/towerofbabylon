/*
File:   game_view.cpp
Author: Taylor Robbins
Date:   09\04\2018
Description: 
	** Holds the functions that handle a view that supports proper pixel scaling for
	** any window size with a target rectangle in mind. This is the view that is
	** used in the majority of the main gameplay. 
*/

void GameViewInit(GameView_t* view, v2 targetSize)
{
	Assert(view != nullptr);
	ClearPointer(view);
	
	view->targetSize = targetSize;
}

//Recalculates the matricies based off gameView members and WindowSize
void GameViewSync(GameView_t* view)
{
	Assert(view != nullptr);
	Assert(WindowSize.width > 0 && WindowSize.height > 0);
	Assert(view->targetSize.width > 0 && view->targetSize.height > 0);
	
	i32 maxScalingX = FloorR32i(WindowSize.width / view->targetSize.width);
	if (maxScalingX < 1) { maxScalingX = 1; }
	i32 maxScalingY = FloorR32i(WindowSize.height / view->targetSize.height);
	if (maxScalingY < 1) { maxScalingY = 1; }
	view->pixelScaling = (maxScalingX <= maxScalingY) ? maxScalingX : maxScalingY;
	view->screenPixelSize = 1.0f / (r32)view->pixelScaling;
	
	view->viewRec.size = view->targetSize;
	view->viewRec.topLeft = view->position - view->viewRec.size/2;
	
	view->screenRec = NewRec(WindowSize/2, view->viewRec.size * (r32)view->pixelScaling);
	view->screenRec.topLeft -= view->screenRec.size/2;
	view->screenRec.topLeft = Vec2Round(view->screenRec.topLeft);
	
	view->projMatrix = Mat4Multiply(
		Mat4Scale(NewVec3(2 / view->viewRec.width, -2 / view->viewRec.height, 1)),
		Mat4Translate(NewVec3(-view->position.x, -view->position.y, 0))
	);
}

void GameViewUpdate(GameView_t* view)
{
	Assert(view != nullptr);
	
	if (Vec2Length(view->targetPos - view->position) > view->screenPixelSize)
	{
		view->position += (view->targetPos - view->position) / (r32)GAME_VIEW_ELASTICITY;
	}
	else
	{
		// if (view->position != view->targetPos) { DEBUG_WriteLine("GameView finished movement"); }
		view->position = view->targetPos;
	}
}

void GameViewApply(GameView_t* view)
{
	Assert(view != nullptr);
	
	RcSetViewport(view->screenRec);
	RcSetProjectionMatrix(view->projMatrix);
}

v2 ScreenPosToWorldPos(GameView_t* view, v2 screenPos)
{
	return Vec2Multiply(Vec2Divide((screenPos - WindowSize/2), view->screenRec.size), view->viewRec.size) + view->position;
}

v2 WorldPosToScreenPos(GameView_t* view, v2 worldPos)
{
	return Vec2Multiply(Vec2Divide(worldPos - view->position, view->viewRec.size), view->screenRec.size) + WindowSize/2;
}

v3 GameViewRound(GameView_t* view, v3 worldPos)
{
	return Vec3Round(worldPos / view->screenPixelSize) * view->screenPixelSize; //TODO: Does this need to be any more complicated?
}
