/*
File:   app_version.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_VERSION_H
#define _APP_VERSION_H

#define APP_VERSION_MAJOR    0
#define APP_VERSION_MINOR    1

#define APP_VERSION_BUILD    1318

#endif //  _APP_VERSION_H
