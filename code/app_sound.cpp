/*
File:   app_sound.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Handles playing, handling, and stopping sounds using OpenAL
*/

#if USE_OPEN_AL
SoundInstance_t* PlayGameSound(const Sound_t* sound, bool looping = false, r32 volume = 1.0f, r32 pitch = 1.0f, bool isSoundEffect = false)
{
	SoundInstance_t* result = nullptr;
	
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		if (app->soundInstances[sIndex].playing == false)
		{
			result = &app->soundInstances[sIndex];
			break;
		}
	}
	
	if (result == nullptr)
	{
		DEBUG_WriteLine("Sound instance buffer is full!");
		return nullptr;
	}
	
	ClearPointer(result);
	
	alGenSources(1, &result->id);
	alSourcef (result->id, AL_PITCH, pitch);
	alSourcef (result->id, AL_GAIN, volume * (isSoundEffect ? app->soundsVolume : 1.0f));
	alSource3f(result->id, AL_POSITION, 0, 0, 0);
	alSource3f(result->id, AL_VELOCITY, 0, 0, 0);
	alSourcei (result->id, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
	alSourcei (result->id, AL_BUFFER, sound->id);
	
	alSourcePlay(result->id);
	
	result->playing = true;
	result->soundId = sound->id;
	return result;
}

void StopSoundInstance(SoundInstance_t* instance)
{
	Assert(instance != nullptr);
	Assert(instance >= &app->soundInstances[0]);
	Assert(instance <= &app->soundInstances[MAX_SOUND_INSTANCES-1]);
	
	if (alIsSource(instance->id))
	{
		alSourceStop(instance->id);
		alDeleteSources(1, &instance->id);
	}
	
	ClearPointer(instance);
}

void StopSound(const Sound_t* sound)
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		
		if (instance->playing && instance->soundId == sound->id)
		{
			StopSoundInstance(instance);
		}
	}
}

void SetSoundVolume(SoundInstance_t* instance, r32 volume, bool isSoundEffect = false)
{
	Assert(instance != nullptr);
	
	if (alIsSource(instance->id))
	{
		alSourcef(instance->id, AL_GAIN, volume * (isSoundEffect ? app->soundsVolume : 1.0f));
	}
}

r32 GetMusicTransitionAmount()
{
	if (app->musicTransStart == 0) { return 1.0f; }
	if (platform->programTime < app->musicTransStart) { return 0.0f; }
	if (platform->programTime - app->musicTransStart >= app->musicTransTime) { return 1.0f; }
	
	u64 transTime = platform->programTime - app->musicTransStart;
	r32 result = (r32)transTime / (r32)app->musicTransTime;
	return result;
}

void GetMusicTransitionVolumes(r32* volume1, r32* volume2)
{
	*volume1 = 1.0f;
	*volume2 = 1.0f;
	
	r32 amount = GetMusicTransitionAmount();
	if (amount >= 1.0f)
	{
		*volume1 = 0.0f; *volume2 = 1.0f;
		return;
	}
	
	switch (app->transitionType)
	{
		case SoundTransition_Immediate:
		{
			*volume1 = 1.0f; *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeOut:
		{
			*volume1 = (1.0f - amount); *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeIn:
		{
			*volume1 = 0.0f; *volume2 = amount;
		} break;
		
		case SoundTransition_FadeOutFadeIn:
		{
			if (amount < 0.5f) { *volume1 = (1.0f - amount*2); *volume2 = 0.0f; }
			else               { *volume1 = 0.0f; *volume2 = (amount - 0.5f) * 2; }
		} break;
		
		case SoundTransition_CrossFade:
		{
			*volume1 = (1.0f - amount); *volume2 = amount;
		} break;
	};
}

void QueueMusicChange(const Sound_t* newSong, SoundTransition_t transitionType = SoundTransition_CrossFade, u64 transitionTime = 2000)
{
	r32 currentTransitionTime = GetMusicTransitionAmount();
	if (currentTransitionTime < 1.0f)
	{
		DEBUG_WriteLine("Last music transition was cut short");
		if (app->lastMusic != nullptr) { StopSoundInstance(app->lastMusic); }
		if (app->currentMusic != nullptr) { SetSoundVolume(app->currentMusic, 1.0f * app->musicVolume, false); }
	}
	
	app->transitionType = transitionType;
	app->musicTransTime = transitionTime;
	app->musicTransStart = platform->programTime;
	app->lastMusic = app->currentMusic;
	app->currentMusic = nullptr;
	
	if (newSong != nullptr)
	{
		app->currentMusic = PlayGameSound(newSong, true, 0.0f);
	}
	
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic    != nullptr) { SetSoundVolume(app->lastMusic,    volume1 * app->musicVolume, false); }
	if (app->currentMusic != nullptr) { SetSoundVolume(app->currentMusic, volume2 * app->musicVolume, false); }
}

void SoftQueueMusicChange(const Sound_t* newSong, SoundTransition_t transitionType = SoundTransition_CrossFade, u64 transitionTime = 2000)
{
	if (app->currentMusic != nullptr && app->currentMusic->soundId == newSong->id)
	{
		return;
	}
	
	QueueMusicChange(newSong, transitionType, transitionTime);
}

void UpdateSounds()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		
		if (instance->playing)
		{
			ALenum audioState = 0;
			alGetSourcei(instance->id, AL_SOURCE_STATE, &audioState);
			if (audioState != AL_PLAYING)
			{
				StopSoundInstance(instance);
			}
		}
	}
	
	r32 transitionAmount = GetMusicTransitionAmount();
	if (transitionAmount >= 1.0f && app->lastMusic != nullptr)
	{
		DEBUG_WriteLine("Finished transition");
		StopSoundInstance(app->lastMusic);
		app->lastMusic = nullptr;
	}
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic    != nullptr) { SetSoundVolume(app->lastMusic,    volume1 * app->musicVolume, false); }
	if (app->currentMusic != nullptr) { SetSoundVolume(app->currentMusic, volume2 * app->musicVolume, false); }
}
#endif
