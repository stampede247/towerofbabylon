/*
File:   app_structs.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_STRUCTS_H
#define _APP_STRUCTS_H

typedef enum
{
	AppState_Game,
} AppState_t;

const char* GetAppStateName(AppState_t appState)
{
	switch (appState)
	{
		case AppState_Game: return "Game";
		default: return "Unknown";
	};
}

typedef enum
{
	SoundTransition_Immediate,
	SoundTransition_FadeOut,
	SoundTransition_FadeIn,
	SoundTransition_FadeOutFadeIn,
	SoundTransition_CrossFade,
} SoundTransition_t;

struct Texture_t
{
	GLuint id;
	
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
};

struct Sound_t
{
	ALuint id;
	ALenum format;
	ALsizei frequency;
	u32 channelCount;
	u32 sampleCount;
};

struct SoundInstance_t
{
	bool playing;
	ALuint id;
	ALuint soundId;
};

struct SpriteSheet_t
{
	Texture_t texture;
	
	union
	{
		v2i padding;
		struct { i32 padX, padY; };
	};
	union
	{
		v2i frameSize;
		struct { i32 frameX, frameY; };
	};
	union
	{
		v2i innerFrameSize;
		struct { i32 innerX, innerY; };
	};
	union
	{
		v2i sheetSize;
		v2i numFrames;
		struct { i32 framesX, framesY; };
	};
};

struct Shader_t
{
	GLuint vertId;
	GLuint fragId;
	GLuint programId;
	
	GLuint vertexArray;
	
	struct
	{
		//Attributes
		GLint positionAttrib;
		GLint colorAttrib;
		GLint texCoordAttrib;
		
		//Vertex Shader
		GLint worldMatrix;
		GLint viewMatrix;
		GLint projectionMatrix;
		
		//Both
		GLint texture;
		GLint textureSize;
		GLint ditherTexture;
		GLint sourceRectangle;
		GLint sourceRecSkew;
		GLint maskRectangle;
		GLint shiftVec;
		
		//Fragment Shader
		GLint primaryColor;
		GLint secondaryColor;
		GLint replaceColor1;
		GLint replaceColor2;
		GLint replaceColor3;
		GLint replaceColor4;
		GLint gradientEnabled;
		GLint circleRadius;
		GLint circleInnerRadius;
		GLint saturation;
		GLint brightness;
		GLint time;
		
		GLint shadows;
	} locations;
};

struct Vertex_t
{
	union
	{
		v3 position;
		struct { r32 x, y, z; };
	};
	union
	{
		v4 color;
		struct { r32 r, g, b, a; };
	};
	union
	{
		v2 texCoord;
		struct { r32 tX, tY; };
	};
};

struct VertexBuffer_t
{
	GLuint id;
	
	u32 numVertices;
};

struct FrameBuffer_t
{
	GLuint id;
	GLuint depthBuffId;
	Texture_t texture;
};

union Range_t
{
	r32 values[2];
	struct
	{
		r32 min;
		r32 max;
	};
	struct
	{
		r32 a;
		r32 b;
	};
};

// +--------------------------------------------------------------+
// |                            Fonts                             |
// +--------------------------------------------------------------+
typedef enum
{
	Alignment_Left,
	Alignment_Center,
	Alignment_Right,
} Alignment_t;

typedef enum
{
	FontStyle_Default = 0x0000,
	
	FontStyle_Bold         = 0x0001,
	FontStyle_Italic       = 0x0002,
	FontStyle_Underline    = 0x0004,
	FontStyle_Wave         = 0x0008, //0x1
	FontStyle_Bubble       = 0x0010, //0x2
	FontStyle_Bounce       = 0x0020, //0x3
	FontStyle_Jitter       = 0x0040, //0x4
	FontStyle_Wobble       = 0x0080, //0x5
	FontStyle_Faded        = 0x0100, //0x6
	FontStyle_Rainbow      = 0x0200, //0x7
	FontStyle_Emboss       = 0x0400, //0x8
	
	FontStyle_CreateGlyphs = 0x2000,
	FontStyle_StrictStyle  = 0x4000,
	FontStyle_StrictSize   = 0x8000,
	
	FontStyle_BoldItalic = FontStyle_Bold|FontStyle_Italic,
	FontStyle_BoldUnderline = FontStyle_Bold|FontStyle_Underline,
	FontStyle_ItalicUnderline = FontStyle_Italic|FontStyle_Underline,
	FontStyle_BoldItalicUnderline = FontStyle_Bold|FontStyle_Italic|FontStyle_Underline,
	
	FontStyle_BakeMask = FontStyle_Bold|FontStyle_Italic,
	FontStyle_StrictSizeStyle = FontStyle_StrictSize|FontStyle_StrictStyle,
} FontStyle_t;

struct FontCharInfo_t
{
	union
	{
		reci bakeRec;
		struct
		{
			v2i bakePos;
			v2i bakeSize;
		};
	};
	v2 size;
	v2 origin;
	r32 advanceX;
};

struct FontFile_t
{
	u16 styleFlags;
	u32 fileLength;
	u8* fileData;
	stbtt_fontinfo stbInfo;
};

struct FontBake_t
{
	u8 firstChar;
	u8 numChars;
	r32 size; //in pixels
	u16 styleFlags;
	FontCharInfo_t* charInfos;
	r32 maxExtendUp;
	r32 maxExtendDown;
	r32 lineHeight;
	Texture_t texture;
};

struct FontGlyph_t
{
	u8 c;
	r32 size; //in pixels
	u16 styleFlags;
	FontCharInfo_t charInfo;
	Texture_t texture;
};

struct Font_t
{
	MemoryArena_t* allocArena;
	
	u32 numFiles;
	FontFile_t* files;
	
	u32 numBakes;
	FontBake_t* bakes;
	
	u32 numGlyphs;
	u32 maxGlyphs;
	FontGlyph_t* glyphs;
};

struct FontFlowInfo_t
{
	//Passed arguments
	const char* strPntr;
	u32 strLength;
	v2 position;
	Font_t* fontPntr;
	Alignment_t alignment;
	r32 fontSize;
	u16 styleFlags;
	
	//Results
	rec extents;
	v2 endPos;
	u32 numLines;
	u32 numRenderables;
	r32 extentRight;
	r32 extentDown;
	v2 totalSize;
};

struct FontChar_t
{
	bool baked;
	Texture_t* texture;
	FontCharInfo_t* info;
};


#endif //  _APP_STRUCTS_H
