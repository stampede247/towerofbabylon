/*
File:   game_entity.cpp
Author: Taylor Robbins
Date:   09\14\2018
Description: 
	** Holds the functions that handle entities in the general sense (including collision and movement) 
*/

Entity_t* CreateEntity(Room_t* room, EntityType_t type, v3 position)
{
	Assert(room != nullptr);
	
	Entity_t* result = RoomAddEntity(room, type);
	result->flags = EntityFlag_Alive|EntityFlag_Solid|EntityFlag_Physics;
	result->position = position;
	result->velocity = Vec3_Zero;
	result->height = TILE_SIZE;
	result->radius = TILE_SIZE/2;
	
	switch (type)
	{
		case EntityType_Player:
		{
			result->debugColor = ColorSapGreen;
			result->radius = 5;
			result->height = 24;
			// FlagUnset(result->flags, EntityFlag_Physics);
		} break;
		case EntityType_Enemy:
		{
			result->debugColor = ColorOrangeRed;
			FlagSet(result->flags, EntityFlag_GroundFriction);
			result->radius = 6;
			result->height = 12;
		} break;
		case EntityType_Grenade:
		{
			result->debugColor = ColorLightGrey;
			FlagSet(result->flags, EntityFlag_GroundFriction);
			FlagSet(result->flags, EntityFlag_Bouncy);
			result->radius = 2;
			result->height = 4;
		} break;
		default: Assert(false); DEBUG_WriteLine("Unknown entity type created"); break;
	}
	
	return result;
}

void KillEntity(Entity_t* entity)
{
	Assert(entity != nullptr);
	FlagUnset(entity->flags, EntityFlag_Alive);
}

void EntityResolveCollision(Room_t* room, Entity_t* entity)
{
	Assert(room != nullptr);
	Assert(entity != nullptr);
	
	v3 minPos = entity->position - NewVec3(entity->radius, 0, entity->radius);
	v3 maxPos = entity->position + NewVec3(entity->radius, entity->height, entity->radius);
	v3i minGrid = NewVec3i(FloorR32i(minPos.x / COLLIDER_SIZE), FloorR32i(minPos.y / COLLIDER_SIZE), FloorR32i(minPos.z / COLLIDER_SIZE));
	v3i maxGrid = NewVec3i(CeilR32i(maxPos.x / COLLIDER_SIZE), CeilR32i(maxPos.y / COLLIDER_SIZE), CeilR32i(maxPos.z / COLLIDER_SIZE));
	minGrid = Vec3iClamp(minGrid, Vec3i_Zero, room->colGridSize);
	maxGrid = Vec3iClamp(maxGrid, Vec3i_Zero, room->colGridSize);
	
	// entity->debugColor = NewColor(Color_Purple);
	
	u32 numCollisionsResolved = 0;
	while (numCollisionsResolved < MAX_COLLISION_RESOLVES)
	{
		bool foundCollision = false;
		for (i32 yPos = minGrid.y; yPos < maxGrid.y; yPos++)
		{
			for (i32 zPos = minGrid.z; zPos < maxGrid.z; zPos++)
			{
				for (i32 xPos = minGrid.x; xPos < maxGrid.x; xPos++)
				{
					v3i gridPos = NewVec3i(xPos, yPos, zPos);
					Collider_t* collider = RoomGetCollider(room, gridPos);
					Assert(collider != nullptr);
					if (collider->solid)
					{
						v3 resolveAxis = Vec3_Zero;
						r32 resolveDepth = GetCylinderColliderIntersectionDepth(collider, NewVec3(gridPos*COLLIDER_SIZE), entity->position, entity->radius, entity->height, &resolveAxis);
						if (resolveDepth < 0) { resolveDepth = -resolveDepth; resolveAxis = -resolveAxis; }
						if (resolveDepth != 0)
						{
							if (resolveAxis == Vec3_Bottom)      { entity->debugColor = PureYellow; }
							else if (resolveAxis == Vec3_Top)    { /*entity->debugColor = NewColor(Color_Orange);*/ }
							else if (resolveAxis == Vec3_Left)   { entity->debugColor = ColorCyan; }
							else if (resolveAxis == Vec3_Right)  { entity->debugColor = PureBlue; }
							else if (resolveAxis == Vec3_Back)   { entity->debugColor = ColorLime; }
							else if (resolveAxis == Vec3_Front)  { entity->debugColor = ColorGreen; }
							else { entity->debugColor = ColorLightGrey; }
							
							entity->position += resolveAxis * resolveDepth;
							r32 velocityInDir = Vec3Dot(entity->velocity, -resolveAxis);
							if (velocityInDir > 0)
							{
								entity->velocity -= velocityInDir * -resolveAxis;
							}
							foundCollision = true;
							numCollisionsResolved++;
							break;
						}
					}
				}
				if (foundCollision) { break; }
			}
			if (foundCollision) { break; }
		}
		
		if (!foundCollision) { break; }
	}
	
	if (numCollisionsResolved == MAX_COLLISION_RESOLVES)
	{
		DEBUG_WriteLine("Max collisions hit!");
	}
}

r32 EntityResolveCollisionHorizontally(Entity_t* entity, rec colliderRec, u8 sides, v2* resolveAxisOut = nullptr)
{
	v2 circleCenter = NewVec2(entity->position.x, entity->position.z);
	v2 colliderCenter = colliderRec.topLeft + colliderRec.size/2;
	Assert(RecOverlapsCircle(colliderRec, circleCenter, entity->radius));
	
	//inside the rectangle
	if (circleCenter.x >= colliderRec.x && circleCenter.x <= colliderRec.x + colliderRec.width &&
		circleCenter.y >= colliderRec.y && circleCenter.y <= colliderRec.y + colliderRec.height)
	{
		//TODO: Make this preferential to which side bits are set!
		if (AbsR32(circleCenter.x - colliderCenter.x) >= AbsR32(circleCenter.y - colliderCenter.y))
		{
			if (circleCenter.x >= colliderCenter.x)
			{
				if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Right; }
				return (colliderRec.x + colliderRec.width - circleCenter.x) + entity->radius;
			}
			else
			{
				if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Left; }
				return (circleCenter.x - colliderRec.x) + entity->radius;
			}
		}
		else
		{
			if (circleCenter.y >= colliderCenter.y)
			{
				if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Right; }
				return (colliderRec.y + colliderRec.height - circleCenter.y) + entity->radius;
			}
			else
			{
				if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Left; }
				return (circleCenter.y - colliderRec.y) + entity->radius;
			}
		}
	}
	//otherwise 8 Verona regions
	else if (circleCenter.x < colliderRec.x && circleCenter.y < colliderRec.y && IsFlagSet(sides, SideBit_Left) && IsFlagSet(sides, SideBit_Back)) //top left
	{
		v2 resolveVec = circleCenter - NewVec2(colliderRec.x, colliderRec.y);
		r32 resolveLength = Vec2Length(resolveVec);
		if (resolveAxisOut != nullptr) { *resolveAxisOut = resolveVec / resolveLength; }
		return entity->radius - resolveLength;
	}
	else if (circleCenter.x > colliderRec.x+colliderRec.width && circleCenter.y < colliderRec.y && IsFlagSet(sides, SideBit_Right) && IsFlagSet(sides, SideBit_Back)) //top right
	{
		v2 resolveVec = circleCenter - NewVec2(colliderRec.x + colliderRec.width, colliderRec.y);
		r32 resolveLength = Vec2Length(resolveVec);
		if (resolveAxisOut != nullptr) { *resolveAxisOut = resolveVec / resolveLength; }
		return entity->radius - resolveLength;
	}
	else if (circleCenter.x < colliderRec.x && circleCenter.y > colliderRec.y+colliderRec.height && IsFlagSet(sides, SideBit_Left) && IsFlagSet(sides, SideBit_Front)) //bottom left
	{
		v2 resolveVec = circleCenter - NewVec2(colliderRec.x, colliderRec.y + colliderRec.height);
		r32 resolveLength = Vec2Length(resolveVec);
		if (resolveAxisOut != nullptr) { *resolveAxisOut = resolveVec / resolveLength; }
		return entity->radius - resolveLength;
	}
	else if (circleCenter.x > colliderRec.x+colliderRec.width && circleCenter.y > colliderRec.y+colliderRec.height && IsFlagSet(sides, SideBit_Right) && IsFlagSet(sides, SideBit_Front)) //bottom right
	{
		v2 resolveVec = circleCenter - NewVec2(colliderRec.x + colliderRec.width, colliderRec.y + colliderRec.height);
		r32 resolveLength = Vec2Length(resolveVec);
		if (resolveAxisOut != nullptr) { *resolveAxisOut = resolveVec / resolveLength; }
		return entity->radius - resolveLength;
	}
	else if (circleCenter.x < colliderRec.x && IsFlagSet(sides, SideBit_Left)) //left side
	{
		if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Left; }
		return entity->radius - (colliderRec.x - circleCenter.x);
	}
	else if (circleCenter.x > colliderRec.x+colliderRec.width && IsFlagSet(sides, SideBit_Right)) //right side
	{
		if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Right; }
		return entity->radius - (circleCenter.x - (colliderRec.x + colliderRec.width));
	}
	else if (circleCenter.y < colliderRec.y && IsFlagSet(sides, SideBit_Back)) //top side
	{
		if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Up; }
		return entity->radius - (colliderRec.y - circleCenter.y);
	}
	else if (circleCenter.y > colliderRec.y+colliderRec.width && IsFlagSet(sides, SideBit_Front)) //bottom side
	{
		if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Down; }
		return entity->radius - (circleCenter.y - (colliderRec.y + colliderRec.height));
	}
	else
	{
		// Assert(false);
		if (resolveAxisOut != nullptr) { *resolveAxisOut = Vec2_Zero; }
		return 0;
	}
}

void Explosion(Room_t* room, v3 position, r32 radius, r32 power)
{
	Assert(room != nullptr);
	
	for (u32 eIndex = 0; eIndex < room->numEntities; eIndex++)
	{
		Entity_t* entity = &room->entities[eIndex];
		v3 normal = (entity->position + NewVec3(0, entity->height/2, 0)) - position;
		r32 distance = Vec3Length(normal);
		normal = normal / distance;
		r32 entityRadius = SqrtR32(entity->radius*entity->radius + (entity->height/2)*(entity->height/2));
		distance -= entityRadius;
		if (distance < radius && radius > 0)
		{
			entity->velocity += normal * power * (1 - (distance / radius));
		}
	}
}

void EntityMoveHorizontally(Room_t* room, Entity_t* entity)
{
	entity->position += NewVec3(entity->velocity.x, 0, entity->velocity.z);
	
	u32 numLoops = 0;
	while (numLoops < 5)
	{
		r32 aboveRadius = entity->radius+SMALL_NUMBER;
		v3i minGridPos = Vec3Floori((entity->position + NewVec3(-aboveRadius, SMALL_NUMBER, -aboveRadius)) / (r32)COLLIDER_SIZE);
		v3i maxGridPos = Vec3Floori((entity->position + NewVec3(aboveRadius, entity->height - SMALL_NUMBER, aboveRadius)) / (r32)COLLIDER_SIZE);
		v2 circleCenter = NewVec2(entity->position.x, entity->position.z);
		
		bool collided = false;
		v2 resolveVec = Vec2_Zero;
		r32 resolveDepth = 0.0f;
		for (i32 yPos = minGridPos.y; yPos <= maxGridPos.y; yPos++)
		{
			for (i32 zPos = minGridPos.z; zPos <= maxGridPos.z; zPos++)
			{
				for (i32 xPos = minGridPos.x; xPos <= maxGridPos.x; xPos++)
				{
					v3i gridPos = NewVec3i(xPos, yPos, zPos);
					Collider_t* collider = RoomGetCollider(room, gridPos);
					if (collider != nullptr && collider->solid)
					{
						rec colliderRec = NewRec(gridPos.x*(r32)COLLIDER_SIZE, gridPos.z*(r32)COLLIDER_SIZE, COLLIDER_SIZE, COLLIDER_SIZE);
						v2 meetingPoint = Vec2_Zero;
						if (RecOverlapsCircle(colliderRec, circleCenter, entity->radius+SMALL_NUMBER, &meetingPoint) && Vec2Length(meetingPoint - circleCenter) < entity->radius - SMALL_NUMBER)
						{
							v2 newResolveVec = Vec2_Zero;
							r32 newResolveDepth = EntityResolveCollisionHorizontally(entity, colliderRec, collider->sides, &newResolveVec);
							if (!collided || newResolveDepth < resolveDepth)
							{
								resolveDepth = newResolveDepth;
								resolveVec = newResolveVec;
								collided = true;
							}
						}
					}
				}
			}
		}
		
		if (collided)
		{
			// DEBUG_PrintLine("Resolving (%.1f,%.1f) x %f", resolveVec.x, resolveVec.y, resolveDepth);
			entity->position.x += resolveVec.x * (resolveDepth + SMALL_NUMBER);
			entity->position.z += resolveVec.y * (resolveDepth + SMALL_NUMBER);
			if (IsFlagSet(entity->flags, EntityFlag_Bouncy))
			{
				Explosion(room, entity->position, EXPLOSION_RADIUS, EXPLOSION_POWER);
				FlagUnset(entity->flags, EntityFlag_Alive);
				entity->velocity.x -= (1 + ENTITY_RESTITUTION) * Vec2Dot(NewVec2(entity->velocity.x, entity->velocity.z), resolveVec) * resolveVec.x;
				entity->velocity.z -= (1 + ENTITY_RESTITUTION) * Vec2Dot(NewVec2(entity->velocity.x, entity->velocity.z), resolveVec) * resolveVec.y;
			}
			else
			{
				entity->velocity.x -= Vec2Dot(NewVec2(entity->velocity.x, entity->velocity.z), resolveVec) * resolveVec.x;
				entity->velocity.z -= Vec2Dot(NewVec2(entity->velocity.x, entity->velocity.z), resolveVec) * resolveVec.y;
			}
			
			numLoops++;
		}
		else
		{
			// DEBUG_PrintLine("Resolved %u times", numLoops);
			break;
		}
	}
	if (numLoops >= 5)
	{
		DEBUG_WriteLine("Resolved too many times!");
	}
}

void EntityUpdate(Room_t* room, Entity_t* entity, bool doCollision)
{
	Assert(room != nullptr);
	Assert(entity != nullptr);
	Assert(entity >= room->entities && entity < room->entities + room->numEntities);
	Assert(IsFlagSet(entity->flags, EntityFlag_Alive));
	
	if (IsFlagSet(entity->flags, EntityFlag_Physics) && doCollision)
	{
		// +==============================+
		// |       Ground Friction        |
		// +==============================+
		if (IsFlagSet(entity->flags, EntityFlag_Grounded) && IsFlagSet(entity->flags, EntityFlag_GroundFriction))
		{
			entity->velocity.x *= GROUND_FRICTION;
			entity->velocity.z *= GROUND_FRICTION;
		}
		
		// +==============================+
		// |      Move Horizontally       |
		// +==============================+
		EntityMoveHorizontally(room, entity);
		
		// +==============================+
		// |         Find Footing         |
		// +==============================+
		entity->floorPos = RoomFindFloor(room, entity->position);
		entity->footingPos = RoomFindFooting(room, entity->position, entity->radius);
		// if (entity->position.y < entity->footingPos.y) { entity->position.y = entity->footingPos.y; }
		if (entity->velocity.y <= 0 && (entity->footingPos.y > entity->position.y || AbsR32(entity->footingPos.y - entity->position.y) < SMALL_NUMBER))
		{ FlagSet(entity->flags, EntityFlag_Grounded); }
		else { FlagUnset(entity->flags, EntityFlag_Grounded); }
		
		// +==============================+
		// |        Apply Gravity         |
		// +==============================+
		if (!IsFlagSet(entity->flags, EntityFlag_Grounded))
		{
			entity->velocity.y -= GRAVITY_ACCELERATION;
			entity->position.y += entity->velocity.y;
			if (entity->velocity.y < 0)
			{
				r32 spaceBelow = entity->position.y - entity->footingPos.y;
				if (spaceBelow <= -entity->velocity.y)
				{
					entity->position.y = entity->footingPos.y;
					if (IsFlagSet(entity->flags, EntityFlag_Bouncy))
					{
						entity->velocity.y = -entity->velocity.y * ENTITY_RESTITUTION;
						Explosion(room, entity->position, EXPLOSION_RADIUS, EXPLOSION_POWER);
						FlagUnset(entity->flags, EntityFlag_Alive);
						if (IsFlagSet(entity->flags, EntityFlag_GroundFriction))
						{
							entity->velocity.x *= GROUND_FRICTION;
							entity->velocity.z *= GROUND_FRICTION;
						}
					}
					else
					{
						entity->velocity.y = 0;
						FlagSet(entity->flags, EntityFlag_Grounded);
					}
				}
			}
		}
		else if (entity->velocity.y < 0)
		{
			if (IsFlagSet(entity->flags, EntityFlag_Bouncy))
			{
				entity->velocity.y = -entity->velocity.y * ENTITY_RESTITUTION;
				Explosion(room, entity->position, EXPLOSION_RADIUS, EXPLOSION_POWER);
				FlagUnset(entity->flags, EntityFlag_Alive);
				if (IsFlagSet(entity->flags, EntityFlag_GroundFriction))
				{
					entity->velocity.x *= GROUND_FRICTION;
					entity->velocity.z *= GROUND_FRICTION;
				}
			}
			else
			{
				entity->velocity.y = 0;
			}
		}
		
		if (IsFlagSet(entity->flags, EntityFlag_Grounded))
		{
			entity->lastStoodPos = entity->footingPos;
		}
		
		// entity->debugColor = IsFlagSet(entity->flags, EntityFlag_Grounded) ? NewColor(Color_Blue) : NewColor(Color_Green);
	}
}

void EntityDraw(v3 roomPos, Entity_t* entity, r32 transitionAlpha, bool draw3d)
{
	Assert(entity != nullptr);
	v3 drawPos = GameViewRound(&game->view, roomPos + entity->position);
	
	if (entity->type == EntityType_Player)
	{
		PlayerData_t* player = GetPlayerData(entity);
		v2i tileSheetPos = Vec2i_Zero;
		if (player->facingDir == Dir2_Up)    { tileSheetPos.y = 0; }
		if (player->facingDir == Dir2_Down)  { tileSheetPos.y = 1; }
		if (player->facingDir == Dir2_Left)  { tileSheetPos.y = 2; }
		if (player->facingDir == Dir2_Right) { tileSheetPos.y = 3; }
		r32 moveSpeed = Vec2Length(NewVec2(entity->velocity.x, entity->velocity.z)) / PLAYER_MAX_SPEED;
		// moveSpeed = RoundR32(moveSpeed*2) / 2.0f;
		u32 period = (moveSpeed >= PLAYER_MAX_SPEED/2.0f) ? 600 : 1200;
		if (moveSpeed > 0 && player->isRunning) { tileSheetPos.x = (i32)Animate(0, 11, period); }
		Color_t drawColor = White;
		// drawColor.a = (u8)Oscillate(0, 255, 1000);
		RcDrawSheetFrame3D(&app->hyperGuySheet, tileSheetPos, drawPos, drawColor, 1.0f);
		
		// RcDrawCircle3D(entity->position + NewVec3(0, entity->height/2, 0), entity->radius*2, White);
		v3 ovalCenter = entity->position + NewVec3(CosR32(player->facingAngle)*entity->radius, entity->height/2-1, SinR32(player->facingAngle)*entity->radius);
		v2 ovalRadius = NewVec2(25, 20);
		// RcDrawRotatedOval3D(ovalCenter, ovalRadius.x, ovalRadius.y, player->facingAngle, NewColor(Color_SaddleBrown));
		
		// RcDrawLine3D(ovalCenter, ovalCenter + NewVec3(CosR32(player->facingAngle), 0, SinR32(player->facingAngle)) * 100, 1, NewColor(Color_Cyan));
	}
	else
	{
		RcDrawCylinder(drawPos, entity->height, entity->radius, entity->debugColor);
	}
	
	// RcDrawLine3D(NewVec3(entity->floorPos.x, entity->position.y, entity->floorPos.z), entity->floorPos, 3, NewColor(Color_Blue));
	// RcDrawSphere(entity->floorPos, 0.5f, NewColor(Color_Blue));
	
	// RcDrawLine3D(NewVec3(entity->footingPos.x, entity->position.y, entity->footingPos.z), entity->footingPos, 3, NewColor(Color_Cyan));
	// RcDrawSphere(entity->footingPos, 0.5f, NewColor(Color_Cyan));
}

void RoomDrawEntities(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d)
{
	Assert(room != nullptr);
	
	if (room->entities != nullptr && room->numEntities > 0)
	{
		for (u32 eIndex = 0; eIndex < room->numEntities; eIndex++)
		{
			Entity_t* entityPntr = &room->entities[eIndex];
			if (IsFlagSet(entityPntr->flags, EntityFlag_Alive))
			{
				EntityDraw(roomPos, entityPntr, transitionAlpha, draw3d);
			}
		}
	}
}

