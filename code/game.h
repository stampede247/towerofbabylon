/*
File:   game.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _GAME_H
#define _GAME_H

#include "game_collision.h"
#include "game_structs.h"

struct GameData_t
{
	bool initialized;
	
	bool use3dView;
	bool drawColliders;
	u32 use3dViewLastChange;
	GameView_t view;
	View3d_t view3d;
	
	Tower_t tower;
	
	bool insideElevator;
	bool exitingElevator;
	r32 elevatorEnterAnim;
	v3 playerEnterStartPos;
	u32 activeElevatorIndex;
	r32 elevatorCurrentFloor;
	r32 elevatorGotoFloor;
	
	//Test Stuff
	r32 roomY;
	v3 roomPos;
	v2 worldPos;
	v2 screenPos;
	v3 testPos;
	quat testRot;
};

#endif //  _GAME_H
