/*
File:   app.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_H
#define _APP_H

struct AppData_t
{
	bool initialized;
	MemoryArena_t mainHeap;
	MemoryArena_t tempArena;
	u32 appInitTempHighWaterMark;
	
	GameData_t gameData;
	
	RenderContext_t renderContext;
	bool buttonHandled[Buttons_NumButtons];
	AppState_t appState;
	AppState_t oldAppState;
	bool skipInitialization;
	bool skipDeinitialization;
	u64  transitionStartTime;
	u64  minTransitionTime;
	
	SoundInstance_t soundInstances[MAX_SOUND_INSTANCES];
	SoundTransition_t transitionType;
	u64 musicTransStart;
	u64 musicTransTime;
	SoundInstance_t* currentMusic;
	SoundInstance_t* lastMusic;
	r32 musicVolume;
	r32 soundsVolume;
	
	Texture_t testSprite;
	Texture_t testTexture;
	Texture_t alphaTexture;
	Texture_t tileSheet1;
	Sound_t testSound;
	Sound_t testMusic;
	Shader_t mainShader;
	Shader_t geomShader;
	SpriteSheet_t hyperGuySheet;
	Font_t mainFont;
};

#endif //  _APP_DATA_H
