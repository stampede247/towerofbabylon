/*
File:   win32_version.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _WIN_32_VERSION_H
#define _WIN_32_VERSION_H

#define PLATFORM_VERSION_MAJOR    1
#define PLATFORM_VERSION_MINOR    0

#define PLATFORM_VERSION_BUILD    363

#endif //  _WIN_32_VERSION_H
