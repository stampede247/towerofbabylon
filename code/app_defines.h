/*
File:   app_defines.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_DEFINES_H
#define _APP_DEFINES_H

#if OSX_COMPILATION
	#if DOUBLE_RESOLUTION
	#define GUI_SCALE 2.0f
	#else
	#define GUI_SCALE 1.0f
	#endif
#else
#define GUI_SCALE 1.0f
#endif

//Application Wide
#define STARTING_APPSTATE            AppState_Game
#define TAB_WIDTH                    4 //spaces
#define TRANSIENT_MAX_NUMBER_MARKS   16
#define MAX_SOUND_INSTANCES          16
#define DEFAULT_LOADING_DELAY        10 //ms
#define DEF_MUSIC_VOLUME             0 //0.4f
#define DEF_SOUND_EFFECTS_VOLUME     0.5f
#define CYLINDER_NUM_SEGMENTS        15  

#define FONTS_FOLDER    "Resources/Fonts/"
#define LEVELS_FOLDER   "Resources/Levels/"
#define MUSIC_FOLDER    "Resources/Music/"
#define SHADERS_FOLDER  "Resources/Shaders/"
#define SHEETS_FOLDER   "Resources/Sheets/"
#define SOUNDS_FOLDER   "Resources/Sounds/"
#define SPRITES_FOLDER  "Resources/Sprites/"
#define TEXTURES_FOLDER "Resources/Textures/"

#define MISSING_TEXTURE_NAME "missing.jpg"

// 240, 110, 170 (FFF06EAA)
// #define DRESS_TARGET_COLOR NewColor(0xF0, 0x6E, 0xAA)
// #define PEACH_TARGET_COLOR NewColor(0xE0, 0x6F, 0x8B)

//NOTE: If these are going to be changed then you need to
//      change them in the fragment shader as well
#define TARGET_COLOR_1 NewColor(0xFFF06EAA) // Pink
#define TARGET_COLOR_2 NewColor(0xFFF6ACCD) // Light Pink
#define TARGET_COLOR_3 NewColor(0xFFEC008C) // Dark Pink
#define TARGET_COLOR_4 NewColor(0xFFF088B8) // Slightly Light Pink


// +--------------------------------------------------------------+
// |                         Game Defines                         |
// +--------------------------------------------------------------+
#define GAME_VIEW_ELASTICITY    10
#define TILE_SIZE               24 //pixels
#define TILE_HEIGHT             24 //pixels
#define COLLIDER_SIZE           12 //pixels
#define TILE_HEIGHT_RATIO       ((r32)TILE_HEIGHT / (r32)TILE_SIZE)
#define TILE_ALLOC_CHUNK_SIZE   16 //tiles (in game_room.cpp)
#define ENTITY_ALLOC_CHUNK_SIZE 16 //entities (in game_room.cpp)
#define MAX_COLLISION_RESOLVES  6
#define SMALL_NUMBER            0.1f
#define JOYSTICK_DEADZONE       0.2f
#define MAX_DROP_SHADOWS        4 //NOTE: Should match geometry-fragment.glsl
#define NUM_CIRCLE_VERTS        20

#define GROUND_FRICTION         0.9f
#define ENTITY_RESTITUTION      0.9f
#define GRAVITY_ACCELERATION    0.2f
#define PLAYER_START_SPEED      0.2f
#define PLAYER_GROUND_ACCEL     0.1f
#define PLAYER_AIR_ACCEL        0.05f
#define PLAYER_MAX_SPEED        1.5f
#define PLAYER_JUMP_SPEED       3.0f
#define THROW_SPEED             3.0f
#define EXPLOSION_RADIUS        48
#define EXPLOSION_POWER         20
#define ROOM_FADE_IN_SPEED      0.03f
#define ROOM_FADE_OUT_SPEED     0.03f

#endif //  _APP_DEFINES_H
