/*
File:   game_player.cpp
Author: Taylor Robbins
Date:   10\07\2018
Description: 
	** Holds functions that handle player movement and other controls as well as player related events 
*/

void PlayerUpdate(Room_t* room, Entity_t* entity)
{
	Assert(entity != nullptr);
	PlayerData_t* player = GetPlayerData(entity);
	
	const GamepadState_t* gamePad = nullptr;
	u8 padIndex = 0;
	for (u8 gIndex = 0; gIndex < Gamepads_MaxNum; gIndex++)
	{
		if (input->gamepads[gIndex].isConnected)
		{
			gamePad = &input->gamepads[gIndex];
			padIndex = gIndex;
			break;
		}
	}
	
	v2 moveDir = Vec2_Zero;
	bool jumped = false;
	bool threw = false;
	bool attacked = false;
	if (gamePad != nullptr) // Gamepad Movement
	{
		GamepadButtons_t jumpBtn = Gamepad_A;
		
		moveDir = gamePad->leftStick;
		if (GamepadPressed(padIndex, jumpBtn))
		{
			jumped = true;
		}
	}
	else //Keyboard Movement
	{
		Buttons_t upBtn    = Button_Up;
		Buttons_t leftBtn  = Button_Left;
		Buttons_t downBtn  = Button_Down;
		Buttons_t rightBtn = Button_Right;
		Buttons_t jumpBtn  = Button_Z;
		Buttons_t attackBtn  = Button_X;
		Buttons_t throwBtn  = Button_C;
		
		if (ButtonDown(upBtn))
		{
			moveDir += Vec2_Up;
		}
		if (ButtonDown(leftBtn))
		{
			moveDir += Vec2_Left;
		}
		if (ButtonDown(downBtn))
		{
			moveDir += Vec2_Down;
		}
		if (ButtonDown(rightBtn))
		{
			moveDir += Vec2_Right;
		}
		moveDir = Vec2Normalize(moveDir);
		
		if (ButtonPressed(jumpBtn))
		{
			jumped = true;
		}
		if (ButtonPressed(attackBtn))
		{
			attacked = true;
		}
		if (ButtonPressed(throwBtn))
		{
			threw = true;
		}
	}
	
	// +==============================+
	// |       Ladder Climbing        |
	// +==============================+
	if (player->climbingLadder)
	{
		entity->velocity = Vec3_Zero;
		player->ladderAnimAmount += 0.05f; //TODO: Adjust this time based off length of the ladder
		if (player->ladderAnimAmount >= 1.0f)
		{
			player->ladderAnimAmount = 1.0f;
			player->climbingLadder = false;
			DEBUG_WriteLine("Finished climbing ladder");
			entity->position = player->ladderEndPos;
		}
		else
		{
			entity->position = Vec3Lerp(player->ladderStartPos, player->ladderEndPos, player->ladderAnimAmount);
		}
	}
	// +==============================+
	// |       Regular Movement       |
	// +==============================+
	else
	{
		// +==============================+
		// |     Find Nearby Ladders      |
		// +==============================+
		Tile_t* ladder = nullptr;
		v3 ladderBottom = Vec3_Zero;
		v3 ladderTop = Vec3_Zero;
		if (IsFlagSet(entity->flags, EntityFlag_Grounded))
		{
			for (u32 tIndex = 0; tIndex < room->numTiles; tIndex++)
			{
				Tile_t* newLadder = &room->tiles[tIndex];
				if (IsFlagSet(newLadder->flags, TileFlag_Ladder))
				{
					v3 newLadderBottom = NewVec3(newLadder->position) + NewVec3((r32)newLadder->size.width/2.0f, 0, entity->radius);
					v3 newLadderTop = NewVec3(newLadder->position) + NewVec3((r32)newLadder->size.width/2.0f, (r32)newLadder->size.height, -entity->radius);
					if (entity->position.y >= newLadderBottom.y - 2 && entity->position.y < newLadderTop.y - 1)
					{
						if (entity->position.x >= newLadder->position.x && entity->position.x <= newLadder->position.x + newLadder->size.width &&
							entity->position.z >= newLadder->position.z && entity->position.z <= newLadder->position.z + entity->radius + 5)
						{
							ladder = newLadder;
							ladderBottom = newLadderBottom;
							ladderTop = newLadderTop;
							//TODO: we could continue and find the closest one if we care about distinguishing between two close ladders
							break;
						}
					}
				}
			}
		}
		
		// +==============================+
		// |       Facing Direction       |
		// +==============================+
		if (Vec2Length(moveDir) > JOYSTICK_DEADZONE)
		{
			player->facingAngle = AtanR32(moveDir.y, moveDir.x);
			if (AbsR32(moveDir.x) >= AbsR32(moveDir.y))
			{
				if (moveDir.x >= 0) { player->facingDir = Dir2_Right; }
				else { player->facingDir = Dir2_Left; }
			}
			else
			{
				if (moveDir.y >= 0) { player->facingDir = Dir2_Down; }
				else { player->facingDir = Dir2_Up; }
			}
		}
		
		// +==============================+
		// |     Horizontal Movement      |
		// +==============================+
		bool grounded = IsFlagSet(entity->flags, EntityFlag_Grounded);
		if (Vec2Length(moveDir) > JOYSTICK_DEADZONE)
		{
			r32 currentSpeed = Vec2Length(NewVec2(entity->velocity.x, entity->velocity.z));
			if (currentSpeed > 0)
			{
				if (grounded)
				{
					#if 0
					//Complicated movement
					r32 newSpeed = currentSpeed;
					r32 newDir = AtanR32(moveDir.y, moveDir.x);
					r32 currentDir = AtanR32(entity->velocity.y, entity->velocity.x);
					r32 angleDiff = AngleDiff(currentDir, newDir);
					r32 angleDamp = ClampR32(angleDiff/Pi32, 0, 1.0f); //TODO: use me?
					
					if (AbsR32(angleDiff) <= Pi32/2)
					{
						if (newSpeed < PLAYER_MAX_SPEED)
						{
							newSpeed += PLAYER_GROUND_ACCEL;
							if (newSpeed >= PLAYER_MAX_SPEED)
							{
								newSpeed = PLAYER_MAX_SPEED;
							}
						}
					}
					else
					{
						newSpeed = PLAYER_GROUND_ACCEL;
					}
					entity->velocity.x = newSpeed * CosR32(newDir);
					entity->velocity.z = newSpeed * SinR32(newDir);
					#else
					// entity->velocity.x += moveDir.x * PLAYER_GROUND_ACCEL;
					// entity->velocity.z += moveDir.y * PLAYER_GROUND_ACCEL;
					entity->velocity.x = moveDir.x * PLAYER_MAX_SPEED;
					entity->velocity.z = moveDir.y * PLAYER_MAX_SPEED;
					
					r32 newSpeed = Vec2Length(NewVec2(entity->velocity.x, entity->velocity.z));
					if (newSpeed > PLAYER_MAX_SPEED)
					{
						entity->velocity.x = (entity->velocity.x / newSpeed) * PLAYER_MAX_SPEED;
						entity->velocity.z = (entity->velocity.z / newSpeed) * PLAYER_MAX_SPEED;
					}
					#endif
				}
				else
				{
					entity->velocity.x += moveDir.x * PLAYER_AIR_ACCEL;
					entity->velocity.z += moveDir.y * PLAYER_AIR_ACCEL;
					r32 newSpeed = Vec2Length(NewVec2(entity->velocity.x, entity->velocity.z));
					if (newSpeed > PLAYER_MAX_SPEED)
					{
						entity->velocity.x = (entity->velocity.x / newSpeed) * PLAYER_MAX_SPEED;
						entity->velocity.z = (entity->velocity.z / newSpeed) * PLAYER_MAX_SPEED;
					}
				}
			}
			else
			{
				if (grounded)
				{
					entity->velocity.x = moveDir.x * PLAYER_START_SPEED;
					entity->velocity.z = moveDir.y * PLAYER_START_SPEED;
				}
				else
				{
					entity->velocity.x = moveDir.x * PLAYER_AIR_ACCEL;
					entity->velocity.z = moveDir.y * PLAYER_AIR_ACCEL;
				}
			}
		}
		else
		{
			if (grounded)
			{
				entity->velocity.x *= 0.7f;
				entity->velocity.z *= 0.7f;
			}
			else
			{
				entity->velocity.x *= 0.98f;
				entity->velocity.z *= 0.98f;
			}
		}
		
		player->isRunning = (Vec2Length(NewVec2(entity->velocity.x, entity->velocity.z)) >= PLAYER_GROUND_ACCEL);
		
		// +==============================+
		// |      Jump/Climb Ladder       |
		// +==============================+
		if (jumped && grounded)
		{
			if (player->facingDir == Dir2_Up && ladder != nullptr)
			{
				DEBUG_WriteLine("Started climbing ladder");
				player->climbingLadder = true;
				player->ladderAnimAmount = 0.0f;
				player->ladderStartPos = ladderBottom;
				player->ladderStartPos.y = entity->position.y;
				player->ladderEndPos = ladderTop;
			}
			else
			{
				// DEBUG_WriteLine("Jumped!");
				entity->velocity.y = PLAYER_JUMP_SPEED;
			}
		}
		
		// +==============================+
		// |            Attack            |
		// +==============================+
		if (attacked) //TODO: Make sure we aren't already attacking
		{
			
		}
		
		// +==============================+
		// |        Throw Grenade         |
		// +==============================+
		if (threw)
		{
			Entity_t* grenade = CreateEntity(room, EntityType_Grenade, entity->position + NewVec3(0, entity->height*3/4, 0));
			r32 upAngle = Pi32/4;
			grenade->velocity.x = CosR32(player->facingAngle) * THROW_SPEED * CosR32(upAngle);
			grenade->velocity.y = SinR32(upAngle) * THROW_SPEED;
			grenade->velocity.z = SinR32(player->facingAngle) * THROW_SPEED * CosR32(upAngle);
		}
	}
}
