/*
File:   app_render_functions.cpp
Author: Taylor Robbins
Date:   09\01\2018
Description: 
	** Holds all the functions that help us draw various kinds of things using the RenderState_t 
*/

//srcRec, color, world
void RcDrawTexturedRec(rec rectangle, Color_t color)
{
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)), //Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f)));  //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}
//srcRec, color, world
void RcDrawTexturedRec(rec rectangle, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),//Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}

void RcDrawTexturedTop(v3 position, v2 size, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(position),//Position
		Mat4Scale(NewVec3(size.x, 1.0f, size.y))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->topBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->topBuffer.numVertices);
}

void RcDrawGradientTop(v3 position, v2 size, Color_t color1, Color_t color2, Dir2_t direction)
{
	RcBindTexture(&rc->gradientTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color1);
	RcSetSecondaryColor(color2);
	bool gradientsWereEnabled = rc->gradientEnabled;
	RcSetGradientEnabled(true);
	
	//TODO: Rotate based off direction input
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(position),//Position
		Mat4Scale(NewVec3(size.x, 1.0f, size.y))); //Scale
	RcSetWorldMatrix(worldMatrix);
	
	RcBindBuffer(&rc->topBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->topBuffer.numVertices);
	
	if (!gradientsWereEnabled) { RcSetGradientEnabled(false); }
}

void RcDrawTexturedFace(v3 position, v2 size, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(position),//Position
		Mat4Scale(NewVec3(size.x, size.y, 1.0f))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->faceBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->faceBuffer.numVertices);
}

void RcDrawCube(v3 position, v3 size, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(position), Mat4Scale(size));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->cubeBuffer.numVertices);
}

void RcDrawOBB(v3 center, v3 size, v3 rotation, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(-Vec3_Half), worldMatrix); //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(size),           worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateX(rotation.x),   worldMatrix); //RotationX
	worldMatrix = Mat4Multiply(Mat4RotateY(rotation.y),   worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4RotateZ(rotation.z),   worldMatrix); //RotationZ
	worldMatrix = Mat4Multiply(Mat4Translate(center),     worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->cubeBuffer.numVertices);
}

void RcDrawTexturedCubeFaces(v3 position, v3 size, u8 sides, Color_t color, rec sideSourceRectangle, rec topSourceRectangle)
{
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(position), Mat4Scale(size));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	
	RcSetSourceRectangle(sideSourceRectangle);
	if (IsFlagSet(sides, SideBit_Left))
	{
		glDrawArrays(GL_TRIANGLES, 6*0, 6);
	}
	if (IsFlagSet(sides, SideBit_Right))
	{
		glDrawArrays(GL_TRIANGLES, 6*1, 6);
	}
	if (IsFlagSet(sides, SideBit_Back))
	{
		glDrawArrays(GL_TRIANGLES, 6*2, 6);
	}
	if (IsFlagSet(sides, SideBit_Front))
	{
		glDrawArrays(GL_TRIANGLES, 6*3, 6);
	}
	RcSetSourceRectangle(topSourceRectangle);
	if (IsFlagSet(sides, SideBit_Bottom))
	{
		glDrawArrays(GL_TRIANGLES, 6*4, 6);
	}
	if (IsFlagSet(sides, SideBit_Top))
	{
		glDrawArrays(GL_TRIANGLES, 6*5, 6);
	}
}

void RcDrawCubeFaces(v3 position, v3 size, u8 sides, Color_t leftColor, Color_t rightColor, Color_t backColor, Color_t frontColor, Color_t bottomColor, Color_t topColor)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(position), Mat4Scale(size));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	
	if (IsFlagSet(sides, SideBit_Left))
	{
		RcSetColor(leftColor);
		glDrawArrays(GL_TRIANGLES, 6*0, 6);
	}
	if (IsFlagSet(sides, SideBit_Right))
	{
		RcSetColor(rightColor);
		glDrawArrays(GL_TRIANGLES, 6*1, 6);
	}
	if (IsFlagSet(sides, SideBit_Back))
	{
		RcSetColor(backColor);
		glDrawArrays(GL_TRIANGLES, 6*2, 6);
	}
	if (IsFlagSet(sides, SideBit_Front))
	{
		RcSetColor(frontColor);
		glDrawArrays(GL_TRIANGLES, 6*3, 6);
	}
	if (IsFlagSet(sides, SideBit_Bottom))
	{
		RcSetColor(bottomColor);
		glDrawArrays(GL_TRIANGLES, 6*4, 6);
	}
	if (IsFlagSet(sides, SideBit_Top))
	{
		RcSetColor(topColor);
		glDrawArrays(GL_TRIANGLES, 6*5, 6);
	}
}

void RcDrawQuatCubeFaces(v3 center, v3 size, quat rotation, u8 sides, Color_t leftColor, Color_t rightColor, Color_t backColor, Color_t frontColor, Color_t bottomColor, Color_t topColor)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(-Vec3_Half), worldMatrix);  //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(size),           worldMatrix);  //Scale
	worldMatrix = Mat4Multiply(Mat4Quaternion(rotation),  worldMatrix); //Rotation
	// worldMatrix = Mat4Multiply(Mat4RotateY(rotation.angle),  worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(center),     worldMatrix);  //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	
	if (IsFlagSet(sides, SideBit_Left))
	{
		RcSetColor(leftColor);
		glDrawArrays(GL_TRIANGLES, 6*0, 6);
	}
	if (IsFlagSet(sides, SideBit_Right))
	{
		RcSetColor(rightColor);
		glDrawArrays(GL_TRIANGLES, 6*1, 6);
	}
	if (IsFlagSet(sides, SideBit_Back))
	{
		RcSetColor(backColor);
		glDrawArrays(GL_TRIANGLES, 6*2, 6);
	}
	if (IsFlagSet(sides, SideBit_Front))
	{
		RcSetColor(frontColor);
		glDrawArrays(GL_TRIANGLES, 6*3, 6);
	}
	if (IsFlagSet(sides, SideBit_Bottom))
	{
		RcSetColor(bottomColor);
		glDrawArrays(GL_TRIANGLES, 6*4, 6);
	}
	if (IsFlagSet(sides, SideBit_Top))
	{
		RcSetColor(topColor);
		glDrawArrays(GL_TRIANGLES, 6*5, 6);
	}
}

void RcDrawRectangle3D(v3 center, v3 normal, v3 tangent, v2 size, Color_t color)
{
	r32 rotationX = 0;//AtanR32(tangent.z - normal.z, tangent.x - normal.x);
	r32 rotationY = AtanR32(normal.z, normal.x);
	r32 rotationZ = AtanR32(normal.y, Vec2Length(NewVec2(normal.x, normal.z)));
	
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(-0.5f, -1.0f, -0.5f)), worldMatrix); //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(size.y, 1.0f, size.x)),    worldMatrix); //Scale
	// worldMatrix = Mat4Multiply(Mat4RotateX(rotationX), worldMatrix); //RotationX
	worldMatrix = Mat4Multiply(Mat4RotateZ(-Pi32/2 + rotationZ), worldMatrix); //RotationZ
	worldMatrix = Mat4Multiply(Mat4RotateY(-rotationY), worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(center),  worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	glDrawArrays(GL_TRIANGLES, 6*5, 6); //Draw top face of cubeBuffer
}

void RcDrawTexturedRectangle3D(v3 center, v3 normal, v3 tangent, v2 size, Color_t color, rec sourceRectangle)
{
	r32 rotationX = 0;//AtanR32(tangent.z - normal.z, tangent.x - normal.x);
	r32 rotationY = AtanR32(normal.z, normal.x);
	r32 rotationZ = AtanR32(normal.y, Vec2Length(NewVec2(normal.x, normal.z)));
	
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(-0.5f, -1.0f, -0.5f)), worldMatrix); //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(size.y, 1.0f, size.x)),    worldMatrix); //Scale
	// worldMatrix = Mat4Multiply(Mat4RotateX(rotationX), worldMatrix); //RotationX
	worldMatrix = Mat4Multiply(Mat4RotateZ(-Pi32/2 + rotationZ), worldMatrix); //RotationZ
	worldMatrix = Mat4Multiply(Mat4RotateY(-rotationY), worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(center),  worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	glDrawArrays(GL_TRIANGLES, 6*5, 6); //Draw top face of cubeBuffer
}

void RcDrawRamp(v3 position, v3 size, Dir2_t rotation, u8 sides, Color_t topColor, Color_t frontColor, Color_t backColor, Color_t rightColor, Color_t bottomColor)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(size), worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateY(GetDir2RotationFrom(Dir2_Right, rotation)), worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(position), worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->rampBuffer);
	
	if (IsFlagSet(sides, SideBit_Top))
	{
		RcSetColor(topColor);
		glDrawArrays(GL_TRIANGLES, 3, 6); //Draw Top
	}
	
	if (IsFlagSet(sides, SideBit_Front))
	{
		RcSetColor(frontColor);
		glDrawArrays(GL_TRIANGLES, 0, 3); //Draw Front Side
	}
	
	if (IsFlagSet(sides, SideBit_Back))
	{
		RcSetColor(backColor);
		glDrawArrays(GL_TRIANGLES, 9, 3); //Draw Back Side
	}
	
	RcDrawCubeFaces(position, size, sides & (SideBit_Right|SideBit_Bottom), White, rightColor, backColor, frontColor, bottomColor, topColor);
}

void RcDrawTexturedRamp(v3 position, v3 size, Dir2_t rotation, Color_t color, rec topSourceRec, r32 topSourceSkew, rec frontSourceRec, bool draw3dSides = false)
{
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(size), worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateY(GetDir2RotationFrom(Dir2_Right, rotation)), worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(position), worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->rampBuffer);
	
	RcSetSourceRectangle(topSourceRec);
	RcSetSourceRecSkew(topSourceSkew);
	glDrawArrays(GL_TRIANGLES, 3, 6); //Draw Top
	RcSetSourceRecSkew(0.0f);
	
	RcSetSourceRectangle(frontSourceRec);
	glDrawArrays(GL_TRIANGLES, 0, 3); //Draw Side
	
	if (draw3dSides)
	{
		RcSetSourceRectangle(frontSourceRec);
		glDrawArrays(GL_TRIANGLES, 9, 3); //Draw Back Side
		
		RcDrawTexturedCubeFaces(position, size, SideBit_Right|SideBit_Bottom, color, topSourceRec, frontSourceRec);
	}
}

void RcDrawCubeOutline(v3 position, v3 size, Color_t color, r32 thickness = 1)
{
	RcDrawCube(NewVec3(position.x, position.y, position.z), NewVec3(thickness, thickness, size.depth), color);
	RcDrawCube(NewVec3(position.x, position.y, position.z), NewVec3(size.width, thickness, thickness), color);
	RcDrawCube(NewVec3(position.x+size.width-thickness, position.y, position.z), NewVec3(thickness, thickness, size.depth), color);
	RcDrawCube(NewVec3(position.x, position.y, position.z+size.depth-thickness), NewVec3(size.width, thickness, thickness), color);
	
	RcDrawCube(NewVec3(position.x, position.y, position.z), NewVec3(thickness, size.height, thickness), color);
	RcDrawCube(NewVec3(position.x+size.width-thickness, position.y, position.z), NewVec3(thickness, size.height, thickness), color);
	RcDrawCube(NewVec3(position.x+size.width-thickness, position.y, position.z+size.depth-thickness), NewVec3(thickness, size.height, thickness), color);
	RcDrawCube(NewVec3(position.x, position.y, position.z+size.depth-thickness), NewVec3(thickness, size.height, thickness), color);
	
	RcDrawCube(NewVec3(position.x, position.y+size.height-thickness, position.z), NewVec3(thickness, thickness, size.depth), color);
	RcDrawCube(NewVec3(position.x, position.y+size.height-thickness, position.z), NewVec3(size.width, thickness, thickness), color);
	RcDrawCube(NewVec3(position.x+size.width-thickness, position.y+size.height-thickness, position.z), NewVec3(thickness, thickness, size.depth), color);
	RcDrawCube(NewVec3(position.x, position.y+size.height-thickness, position.z+size.depth-thickness), NewVec3(size.width, thickness, thickness), color);
}

void RcDrawTextureDebugCubePart(v3 position, v3 size, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(position), Mat4Scale(size));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubePartBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->cubePartBuffer.numVertices);
}

void RcDrawSprite3D(v3 bottomCenter, v2 size, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(bottomCenter), Mat4Scale(NewVec3(size.x, size.y, 0)));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubePartBuffer);
	glDrawArrays(GL_TRIANGLES, 6*3, 6); //Draw Front Face of Cube primitive
}

void RcDrawSheetFrame3D(const SpriteSheet_t* spriteSheet, v2i frame, v3 bottomCenter, Color_t color, r32 scale = 1.0f)
{
	RcBindTexture(&spriteSheet->texture);
	rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
	// rec sourceRec = NewRec(Vec2_Zero, NewVec2(spriteSheet->texture.size));
	RcSetSourceRectangle(sourceRec);
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(-0.5f, 0, -1)), worldMatrix);
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(spriteSheet->innerFrameSize.x*scale, spriteSheet->innerFrameSize.y*scale, 1.0f)), worldMatrix);
	worldMatrix = Mat4Multiply(Mat4Translate(bottomCenter), worldMatrix);
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cubeBuffer);
	glDrawArrays(GL_TRIANGLES, 6*3, 6); //Draw Front Face of Cube primitive
}

void RcDrawSphere(v3 position, r32 radius, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(position), Mat4Scale(NewVec3(radius, radius, radius)));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->sphereBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->sphereBuffer.numVertices);
}

void RcDrawCylinder(v3 bottomCenter, r32 height, r32 radius, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(Mat4Translate(bottomCenter), Mat4Scale(NewVec3(radius, height, radius)));
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->cylinderBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->cylinderBuffer.numVertices);
}

//Texture, srcRec, color, world
void RcDrawRectangle(rec rectangle, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),//Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}

//Texture, srcRec, color, world
void RcDrawTriangle(v2 point, v2 dirVec, r32 height, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 triangleHeight = SqrtR32(3)/2;
	r32 direction = AtanR32(dirVec.y, dirVec.x);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(point.x, point.y, rc->depth)),//Position
		Mat4Scale(NewVec3(height/triangleHeight, height/triangleHeight, 1.0f)), //Scale
		Mat4RotateZ(Pi32/2.0f + direction)); //Rotation
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->equilTriangleBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->equilTriangleBuffer.numVertices);
}

//Texture, srcRec, color, world
void RcDrawLine(v2 p1, v2 p2, r32 thickness, Color_t color)
{
	r32 length = Vec2Length(p2 - p1);
	r32 rotation = AtanR32(p2.y - p1.y, p2.x - p1.x); 
	
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(0.0f, -0.5f, 0.0f)),       worldMatrix); //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(length, thickness, 1.0f)),     worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateZ(rotation),                           worldMatrix); //Rotation
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(p1.x, p1.y, rc->depth)),   worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}

//TODO: Can this be done better with immediate mode or quaternions?
void RcDrawLine3D(v3 p1, v3 p2, r32 thickness, Color_t color)
{
	r32 length = Vec3Length(p2 - p1);
	r32 angleZ = AtanR32(p2.y - p1.y, Vec2Length(NewVec2(p2.x - p1.x, p2.z - p1.z)));
	r32 angleY = AtanR32(p2.z - p1.z, p2.x - p1.x);
	
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(length, 1.0f, 1.0f)), worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateZ(angleZ),                    worldMatrix); //RotationZ
	worldMatrix = Mat4Multiply(Mat4RotateY(-angleY),                    worldMatrix); //RotationY
	worldMatrix = Mat4Multiply(Mat4Translate(p1),                      worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);//TODO: Make this into a render context thing?
	glLineWidth(thickness);
	RcBindBuffer(&rc->lineBuffer);
	glDrawArrays(GL_LINES, 0, rc->lineBuffer.numVertices);
	// glBegin(GL_LINES);
	// v4 colorVec = NewVec4(color);
	// glColor4f(colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	// glTexCoord2f(0.0f, 0.0f);
	// glVertex3f(p1.x, p1.y, p1.z);
	// glVertex3f(p2.x, p2.y, p2.z);
	// glEnd();
	glLineWidth(1.0f);
	glPolygonMode(GL_BACK, GL_FILL);
	glPolygonMode(GL_FRONT, GL_LINE);
}

//Texture, srcRec, color, world
void RcDrawSheetFrame(const SpriteSheet_t* spriteSheet, v2i frame, rec drawRec, Color_t color, Dir2_t rotation = Dir2_Down)
{
	RcBindTexture(&spriteSheet->texture);
	rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
	RcSetSourceRectangle(sourceRec);
	RcSetColor(color);
	if (rotation == Dir2_Left)
	{
		mat4 worldMatrix = Mat4Multiply(
			Mat4Translate(NewVec3(drawRec.x + drawRec.width, drawRec.y, rc->depth)),//Position
			Mat4Scale(NewVec3(drawRec.width, drawRec.height, 1.0f)), //Scale
			Mat4RotateZ(ToRadians(90))); //Rotation
		RcSetWorldMatrix(worldMatrix);
	}
	else if (rotation == Dir2_Right)
	{
		mat4 worldMatrix = Mat4Multiply(
			Mat4Translate(NewVec3(drawRec.x, drawRec.y + drawRec.height, rc->depth)),//Position
			Mat4Scale(NewVec3(drawRec.width, drawRec.height, 1.0f)), //Scale
			Mat4RotateZ(ToRadians(-90))); //Rotation
		RcSetWorldMatrix(worldMatrix);
	}
	else if (rotation == Dir2_Up)
	{
		mat4 worldMatrix = Mat4Multiply(
			Mat4Translate(NewVec3(drawRec.x + drawRec.width, drawRec.y + drawRec.height, rc->depth)),//Position
			Mat4Scale(NewVec3(drawRec.width, drawRec.height, 1.0f)), //Scale
			Mat4RotateZ(ToRadians(180))); //Rotation
		RcSetWorldMatrix(worldMatrix);
	}
	else
	{
		mat4 worldMatrix = Mat4Multiply(
			Mat4Translate(NewVec3(drawRec.x, drawRec.y, rc->depth)),//Position
			Mat4Scale(NewVec3(drawRec.width, drawRec.height, 1.0f))); //Scale
		RcSetWorldMatrix(worldMatrix);
	}
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}

//Texture, srcRec, color, secColor, [gradientEnabled], world
void RcDrawGradient(rec rectangle, Color_t color1, Color_t color2, Dir2_t direction)
{
	RcBindTexture(&rc->gradientTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color1);
	RcSetSecondaryColor(color2);
	bool gradientsWereEnabled = rc->gradientEnabled;
	RcSetGradientEnabled(true);
	
	mat4 worldMatrix = Mat4_Identity;
	switch (direction)
	{
		case Dir2_Right:
		default:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),
				Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f)));
		} break;
		
		case Dir2_Left:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y, rc->depth)),
				Mat4Scale(NewVec3(-rectangle.width, rectangle.height, 1.0f)));
		} break;
		
		case Dir2_Down:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y, rc->depth)),
				Mat4RotateZ(ToRadians(90)),
				Mat4Scale(NewVec3(rectangle.height, rectangle.width, 1.0f)));
		} break;
		
		case Dir2_Up:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y + rectangle.height, rc->depth)),
				Mat4RotateZ(ToRadians(90)),
				Mat4Scale(NewVec3(-rectangle.height, rectangle.width, 1.0f)));
		} break;
	};
	RcSetWorldMatrix(worldMatrix);
	
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
	
	if (!gradientsWereEnabled) { RcSetGradientEnabled(false); }
}

void RcDrawCircle3D(v3 center, r32 radius, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(radius, 1, radius)), worldMatrix); //Scaling
	worldMatrix = Mat4Multiply(Mat4Translate(center), worldMatrix); //Translation
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->circleBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->circleBuffer.numVertices);
}

void RcDrawOval3D(v3 center, r32 radiusX, r32 radiusZ, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(radiusX, 1, radiusZ)), worldMatrix); //Scaling
	worldMatrix = Mat4Multiply(Mat4Translate(center), worldMatrix); //Translation
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->circleBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->circleBuffer.numVertices);
}

void RcDrawRotatedOval3D(v3 center, r32 radius1, r32 radius2, r32 rotation, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(radius1, 1, radius2)), worldMatrix); //Scaling
	worldMatrix = Mat4Multiply(Mat4RotateY(-rotation), worldMatrix); //Rotation
	worldMatrix = Mat4Multiply(Mat4Translate(center), worldMatrix); //Translation
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->circleBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->circleBuffer.numVertices);
}

// +--------------------------------------------------------------+
// |                     Derivative Functions                     |
// +--------------------------------------------------------------+
void RcDrawTriangleBordered(v2 point, v2 dirVec, r32 height, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 normDirVec = Vec2Normalize(dirVec);
	r32 pointDist = Vec2Length(NewVec2(borderWidth * (1 + CosR32(ToRadians(60))), borderWidth * SinR32(ToRadians(60))));
	RcDrawTriangle(point + normDirVec*pointDist, dirVec, height + borderWidth + pointDist, borderColor);
	RcDrawTriangle(point, dirVec, height, color);
}

void RcDrawButton(rec rectangle, Color_t backgroundColor, Color_t borderColor, r32 borderWidth = 1.0f)
{
	RcDrawRectangle(rectangle, backgroundColor);
	
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y, borderWidth, rectangle.height), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + rectangle.height - borderWidth, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + rectangle.width - borderWidth, rectangle.y, borderWidth, rectangle.height), borderColor);
}

void RcDrawCircle(v2 center, r32 radius, Color_t color)
{
	r32 oldRadius = rc->circleRadius;
	r32 oldInnerRadius = rc->circleInnerRadius;
	RcSetCircleRadius(1.0f, 0.0f);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(oldRadius, oldInnerRadius);
}

void RcDrawDonut(v2 center, r32 radius, r32 innerRadius, Color_t color)
{
	r32 oldRadius = rc->circleRadius;
	r32 oldInnerRadius = rc->circleInnerRadius;
	r32 realInnerRadius = ClampR32(innerRadius / radius, 0.0f, 1.0f);
	RcSetCircleRadius(1.0f, realInnerRadius);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(oldRadius, oldInnerRadius);
}

// +==============================+
// |        Bezier Curves         |
// +==============================+
void RcDrawBezierCurve3(v2 start, v2 control, v2 end, Color_t color, u32 numVerts,
	r32 thickness = 1.0f, bool drawCircles = true, bool roundedEnds = false)
{
	for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
	{
		r32 t1 = (r32)vIndex * (1.0f/numVerts);
		r32 t2 = (r32)(vIndex+1) * (1.0f/numVerts);
		
		r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
		r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
		v2 pos1 = NewVec2(xPos1, yPos1);
		
		r32 xPos2 = ((1 - t2)*(1 - t2)*start.x) + (2*t2*(1 - t2)*control.x) + t2*t2*end.x;
		r32 yPos2 = ((1 - t2)*(1 - t2)*start.y) + (2*t2*(1 - t2)*control.y) + t2*t2*end.y;
		v2 pos2 = NewVec2(xPos2, yPos2);
		
		if (drawCircles)
		{
			if (vIndex == 0 && roundedEnds)
			{
				RcDrawCircle(pos1, thickness/2, color);
			}
			if (vIndex+1 < numVerts || roundedEnds)
			{
				RcDrawCircle(pos2, thickness/2, color);
			}
		}
		RcDrawLine(pos1, pos2, thickness, color);
	}
}

void RcDrawBezierArrow3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	
	RcDrawTriangle(end + normal*(arrowSize/2), end - pos1, arrowSize, color);
}

void RcDrawBezierArrowBordered3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	
	RcDrawTriangleBordered(end + normal*(arrowSize/2), end - pos1, arrowSize, color, borderColor, borderWidth);
}

void RcDrawBezierCurve4(v2 start, v2 control1, v2 control2, v2 end, Color_t color, u32 numVerts,
	r32 thickness = 1.0f, bool drawCircles = true, bool roundedEnds = false)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierCurve3(start, control1, midPoint, color, numVerts, thickness, drawCircles, roundedEnds);
	RcDrawBezierCurve3(midPoint, control2, end, color, numVerts, thickness, drawCircles, roundedEnds);
}

void RcDrawBezierArrow4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrow3(midPoint, control2, end, numVerts, arrowSize, color);
}

void RcDrawBezierArrowBordered4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrowBordered3(midPoint, control2, end, numVerts, arrowSize, color, borderColor, borderWidth);
}

