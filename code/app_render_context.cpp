/*
File:   app_render_context.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Lots of functions that facilitate drawing different
	** primitives and common things 
*/

void RcCreatePrimitives()
{
	if (rc->lineBuffer.numVertices != 0)     { DestroyVertexBuffer(&rc->lineBuffer);     rc->lineBuffer.numVertices = 0;     }
	if (rc->squareBuffer.numVertices != 0)   { DestroyVertexBuffer(&rc->squareBuffer);   rc->squareBuffer.numVertices = 0;   }
	if (rc->faceBuffer.numVertices != 0)     { DestroyVertexBuffer(&rc->faceBuffer);     rc->faceBuffer.numVertices = 0;     }
	if (rc->topBuffer.numVertices != 0)      { DestroyVertexBuffer(&rc->topBuffer);      rc->topBuffer.numVertices = 0;      }
	if (rc->cubeBuffer.numVertices != 0)     { DestroyVertexBuffer(&rc->cubeBuffer);     rc->cubeBuffer.numVertices = 0;     }
	if (rc->cubePartBuffer.numVertices != 0) { DestroyVertexBuffer(&rc->cubePartBuffer); rc->cubePartBuffer.numVertices = 0; }
	if (rc->sphereBuffer.numVertices != 0)   { DestroyVertexBuffer(&rc->sphereBuffer);   rc->sphereBuffer.numVertices = 0;   }
	if (rc->cylinderBuffer.numVertices != 0) { DestroyVertexBuffer(&rc->cylinderBuffer); rc->cylinderBuffer.numVertices = 0; }
	if (rc->rampBuffer.numVertices != 0)     { DestroyVertexBuffer(&rc->rampBuffer); rc->rampBuffer.numVertices = 0; }
	if (rc->circleBuffer.numVertices != 0)   { DestroyVertexBuffer(&rc->circleBuffer); rc->circleBuffer.numVertices = 0; }
	
	Vertex_t lineVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
	};
	rc->lineBuffer = CreateVertexBuffer(lineVertices, ArrayCount(lineVertices));
	
	Vertex_t squareVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, Vec4_One, {0.0f, 1.0f} },
		
		{  {0.0f, 1.0f, 0.0f}, Vec4_One, {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, Vec4_One, {1.0f, 1.0f} },
	};
	rc->squareBuffer = CreateVertexBuffer(squareVertices, ArrayCount(squareVertices));
	
	Vertex_t faceVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 1.0f} },
		{  {0.0f, 1.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 1.0f} },
		{  {1.0f, 1.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 1.0f} },
	};
	rc->faceBuffer = CreateVertexBuffer(faceVertices, ArrayCount(faceVertices));
	
	Vertex_t topVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {0.0f, 0.0f, 1.0f}, Vec4_One, {0.0f, 1.0f} },
		
		{  {0.0f, 0.0f, 1.0f}, Vec4_One, {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {1.0f, 0.0f, 1.0f}, Vec4_One, {1.0f, 1.0f} },
	};
	rc->topBuffer = CreateVertexBuffer(topVertices, ArrayCount(topVertices));
	
	Vertex_t rampVertices[] =
	{
		{  {0.0f, 0.0f, 1.0f}, Vec4_One, {0.0f, 1.0f} }, //Front side
		{  {1.0f, 1.0f, 1.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {1.0f, 0.0f, 1.0f}, Vec4_One, {1.0f, 1.0f} },
		
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} }, //Top Face
		{  {1.0f, 1.0f, 0.0f}, Vec4_One, {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 1.0f}, Vec4_One, {1.0f, 1.0f} },
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {1.0f, 1.0f, 1.0f}, Vec4_One, {1.0f, 1.0f} },
		{  {0.0f, 0.0f, 1.0f}, Vec4_One, {0.0f, 1.0f} },
		
		{  {1.0f, 0.0f, 0.0f}, Vec4_One, {0.0f, 1.0f} }, //Back side
		{  {1.0f, 1.0f, 0.0f}, Vec4_One, {0.0f, 0.0f} },
		{  {0.0f, 0.0f, 0.0f}, Vec4_One, {1.0f, 1.0f} },
	};
	rc->rampBuffer = CreateVertexBuffer(rampVertices, ArrayCount(rampVertices));
	
	Vertex_t circleVertices[3*NUM_CIRCLE_VERTS];
	for (int i = 0; i < NUM_CIRCLE_VERTS; i++)
	{
		Vertex_t* vert = &circleVertices[i*3];
		r32 angle1 = ((2*Pi32)/(r32)NUM_CIRCLE_VERTS) * i;
		r32 angle2 = ((2*Pi32)/(r32)NUM_CIRCLE_VERTS) * (i+1);
		vert[0].position = NewVec3(CosR32(angle2), 0, SinR32(angle2));
		vert[0].texCoord = NewVec2(0.5f) + NewVec2(CosR32(angle2), SinR32(angle2)) * 0.5f;
		vert[0].color = Vec4_One;
		vert[1].position = Vec3_Zero;
		vert[1].texCoord = NewVec2(0.5f);
		vert[1].color = Vec4_One;
		vert[2].position = NewVec3(CosR32(angle1), 0, SinR32(angle1));
		vert[2].texCoord = NewVec2(0.5f) + NewVec2(CosR32(angle1), SinR32(angle1)) * 0.5f;
		vert[2].color = Vec4_One;
	}
	rc->circleBuffer = CreateVertexBuffer(circleVertices, ArrayCount(circleVertices));
	
	if (rc->cubeBuffer.numVertices != 0) { DestroyVertexBuffer(&rc->cubeBuffer); }
	if (rc->cubePartBuffer.numVertices != 0) { DestroyVertexBuffer(&rc->cubePartBuffer); }
	
	TempPushMark();
	{
		u32 numVerts;
		PrimitiveVertex_t* primVerts = CubePrimitiveCreateArena(Vec3_Zero, Vec3_One, White, NewRec(0, 0, 1, 1), TempArena, &numVerts);
		Vertex_t* verts = PushArray(TempArena, Vertex_t, numVerts);
		Vertex_t* partVerts = PushArray(TempArena, Vertex_t, numVerts);
		u32 numPartVerts = 0;
		for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
		{
			PrimitiveVertex_t* primVert = &primVerts[vIndex];
			Vertex_t* vert = &verts[vIndex];
			vert->position = primVert->position;
			vert->color = NewVec4FromColor(primVert->color);
			vert->texCoord = primVert->texCoord;
			
			if (primVert->faceIndex != 3 && primVert->faceIndex != 5) //exclude top and front
			{
				Vertex_t* partVert = &partVerts[numPartVerts];
				partVert->position = primVert->position;
				partVert->color = NewVec4FromColor(primVert->color);
				partVert->texCoord = primVert->texCoord;
				numPartVerts++;
			}
		}
		rc->cubeBuffer = CreateVertexBuffer(verts, numVerts);
		rc->cubePartBuffer = CreateVertexBuffer(partVerts, numPartVerts);
	}
	TempPopMark();
	
	TempPushMark();
	{
		u32 numVerts;
		PrimitiveVertex_t* primVerts = SpherePrimitiveCreateArena(Vec3_Zero, 1.0f, 20, 20, White, NewRec(0, 0, 1, 1), TempArena, &numVerts);
		Vertex_t* verts = PushArray(TempArena, Vertex_t, numVerts);
		for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
		{
			PrimitiveVertex_t* primVert = &primVerts[vIndex];
			Vertex_t* vert = &verts[vIndex];
			vert->position = primVert->position;
			vert->color = NewVec4FromColor(primVert->color);
			if (primVert->faceIndex == 0 || primVert->faceIndex == 20 || primVert->faceIndex == 20/2)
			{
				vert->color = vert->color * 0.5f;
				vert->color.a = 1.0f;
			}
			// vert->color.r *= (primVert->faceIndex / 10.0f);
			// vert->color.g *= (primVert->faceIndex / 10.0f);
			// vert->color.b *= (primVert->faceIndex / 10.0f);
			vert->texCoord = primVert->texCoord;
		}
		rc->sphereBuffer = CreateVertexBuffer(verts, numVerts);
	}
	TempPopMark();
	
	TempPushMark();
	{
		u32 numVerts;
		PrimitiveVertex_t* primVerts = CylinderPrimitiveCreateArena(Vec3_Zero, 1.0f, 1.0f, 20, White, NewRec(0, 0, 1, 1), TempArena, &numVerts);
		Vertex_t* verts = PushArray(TempArena, Vertex_t, numVerts);
		for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
		{
			PrimitiveVertex_t* primVert = &primVerts[vIndex];
			Vertex_t* vert = &verts[vIndex];
			vert->position = primVert->position;
			vert->color = NewVec4FromColor(primVert->color);
			if (primVert->faceIndex == 0 || primVert->faceIndex == 2)
			{
				vert->color = vert->color * 0.5f;
				vert->color.a = 1.0f;
			}
			vert->texCoord = primVert->texCoord;
		}
		rc->cylinderBuffer = CreateVertexBuffer(verts, numVerts);
	}
	TempPopMark();
}

void InitializeRenderContext()
{
	Assert(rc != nullptr);
	ClearPointer(rc);
	
	RcCreatePrimitives();
	
	r32 triangleHeight = SqrtR32(3)/2.0f;
	Vertex_t equilTriangleVertices[] =
	{
		{  { 0.0f, 0.0f,           0.0f}, Vec4_One, {0.5f, 0.0f} },
		{  { 0.5f, triangleHeight, 0.0f}, Vec4_One, {1.0f, 1.0f} },
		{  {-0.5f, triangleHeight, 0.0f}, Vec4_One, {0.0f, 1.0f} },
	};
	rc->equilTriangleBuffer = CreateVertexBuffer(equilTriangleVertices, ArrayCount(equilTriangleVertices));
	
	rc->gradientTexture = LoadTexture(TEXTURES_FOLDER "gradient.png", false, false);
	rc->circleTexture   = LoadTexture(SPRITES_FOLDER  "circle.png", false, false);
	rc->ditherTexture   = LoadTexture(TEXTURES_FOLDER "dither.png", true, true);
	
	Color_t textureData = White;
	rc->dotTexture = CreateTexture((u8*)&textureData, 1, 1);
	
	rc->fontAlignment = Alignment_Left;
	rc->fontStyle = FontStyle_Default;
	rc->fontSize = 12;
	rc->viewport = NewRec(0, 0, (r32)WindowSize.x, (r32)WindowSize.y);
	rc->worldMatrix = Mat4_Identity;
	rc->viewMatrix = Mat4_Identity;
	rc->projectionMatrix = Mat4_Identity;
	rc->gradientEnabled = false;
	rc->sourceRectangle = NewRec(0, 0, 1, 1);
	rc->sourceRecSkew = 0.0f;
	rc->depth = 1.0f;
	rc->saturation = 1.0f;
	rc->brightness = 1.0f;
	rc->time = 0.0f;
	rc->circleRadius = 0.0f;
	rc->circleInnerRadius = 0.0f;
	rc->primaryColor = White;
	rc->secondaryColor = White;
	rc->replaceColor1 = TARGET_COLOR_1;
	rc->replaceColor2 = TARGET_COLOR_2;
	rc->replaceColor3 = TARGET_COLOR_3;
	rc->replaceColor4 = TARGET_COLOR_4;
	rc->maskRectangle = NewRec(0, 0, 0, 0);
	rc->shiftVec = Vec2_Zero;
	
	ClearArray(rc->shadows);
}

// +--------------------------------------------------------------+
// |                        Main Functions                        |
// +--------------------------------------------------------------+
void RcSetWorldMatrix(const mat4& worldMatrix)
{
	rc->worldMatrix = worldMatrix;
	glUniformMatrix4fv(rc->boundShader->locations.worldMatrix, 1, GL_FALSE, &worldMatrix.values[0][0]);
}
void RcSetViewMatrix(const mat4& viewMatrix)
{
	rc->viewMatrix = viewMatrix;
	glUniformMatrix4fv(rc->boundShader->locations.viewMatrix, 1, GL_FALSE, &viewMatrix.values[0][0]);
}
void RcSetProjectionMatrix(const mat4& projectionMatrix)
{
	rc->projectionMatrix = projectionMatrix;
	glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &projectionMatrix.values[0][0]);
}

void RcSetViewport(rec viewport, bool flipYAxis = true)
{
	rc->viewport = viewport;
	
	reci openglRec = NewReci(
		(i32)viewport.x,
		0,
		(i32)(viewport.width * GUI_SCALE),
		(i32)(viewport.height * GUI_SCALE)
	);
	if (rc->boundFrameBuffer != nullptr)
	{
		openglRec.y = (i32)(rc->boundFrameBuffer->texture.height - (viewport.y + viewport.height));
	}
	else
	{
		openglRec.y = (i32)(WindowSize.height - (viewport.y + viewport.height));	
	}
	glViewport(openglRec.x, openglRec.y, openglRec.width, openglRec.height);
	
	mat4 projMatrix = Mat4_Identity;
	projMatrix = Mat4Scale(NewVec3(2.0f/viewport.width, (flipYAxis ? -1 : 1) * 2.0f/viewport.height, 1.0f));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-viewport.width/2.0f, -viewport.height/2.0f, 0.0f)));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-viewport.x, -viewport.y, 0.0f)));
	RcSetProjectionMatrix(projMatrix);
}

void RcUpdateShader()
{
	if (rc->boundShader == nullptr) { return; }
	
	if (rc->boundBuffer != nullptr)
	{
		glBindVertexArray(rc->boundShader->vertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, rc->boundBuffer->id);
		glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
		glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
		glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
	}
	
	glUniformMatrix4fv(rc->boundShader->locations.worldMatrix,      1, GL_FALSE, &rc->worldMatrix.values[0][0]);
	glUniformMatrix4fv(rc->boundShader->locations.viewMatrix,       1, GL_FALSE, &rc->viewMatrix.values[0][0]);
	glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &rc->projectionMatrix.values[0][0]);
	
	v4 colorVec = NewVec4FromColor(rc->primaryColor);
	glUniform4f(rc->boundShader->locations.primaryColor,   colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4FromColor(rc->secondaryColor);
	glUniform4f(rc->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4FromColor(rc->replaceColor1);
	glUniform4f(rc->boundShader->locations.replaceColor1,  colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4FromColor(rc->replaceColor2);
	glUniform4f(rc->boundShader->locations.replaceColor2,  colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4FromColor(rc->replaceColor3);
	glUniform4f(rc->boundShader->locations.replaceColor3,  colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4FromColor(rc->replaceColor4);
	glUniform4f(rc->boundShader->locations.replaceColor4,  colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	
	if (rc->boundTexture != nullptr)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
		glUniform1i(rc->boundShader->locations.texture,      0);
		glUniform2f(rc->boundShader->locations.textureSize,  (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
	}
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, rc->ditherTexture.id);
	glUniform1i(rc->boundShader->locations.ditherTexture, 1);
	
	glUniform4f(rc->boundShader->locations.sourceRectangle,   rc->sourceRectangle.x, rc->sourceRectangle.y, rc->sourceRectangle.width, rc->sourceRectangle.height);
	glUniform1f(rc->boundShader->locations.sourceRecSkew,     rc->sourceRecSkew);
	glUniform4f(rc->boundShader->locations.maskRectangle,     rc->maskRectangle.x, rc->maskRectangle.y, rc->maskRectangle.width, rc->maskRectangle.height);
	
	glUniform2f(rc->boundShader->locations.shiftVec,          rc->shiftVec.x, rc->shiftVec.y);
	glUniform1i(rc->boundShader->locations.gradientEnabled,   rc->gradientEnabled ? 1 : 0);
	glUniform1f(rc->boundShader->locations.circleRadius,      rc->circleRadius);
	glUniform1f(rc->boundShader->locations.circleInnerRadius, rc->circleInnerRadius);
	glUniform1f(rc->boundShader->locations.saturation,        rc->saturation);
	glUniform1f(rc->boundShader->locations.brightness,        rc->brightness);
	
	glUniform4fv(rc->boundShader->locations.shadows, MAX_DROP_SHADOWS, &rc->shadows[0].values[0]);
}

void RcBegin(rec viewport, Shader_t* shaderPntr, FrameBuffer_t* frameBufferPntr)
{
	if (rc->boundFrameBuffer == nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, rc->boundFrameBuffer->id);
	}
	
	#if 1
	glEnable(GL_CULL_FACE); glCullFace(GL_FRONT);
	#else
	glDisable(GL_CULL_FACE);
	glLineWidth(1.0f);
	glPolygonMode(GL_BACK, GL_FILL);
	glPolygonMode(GL_FRONT, GL_LINE);
	#endif
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST); glDepthFunc(GL_LEQUAL);
	glEnable(GL_ALPHA_TEST); glAlphaFunc(GL_GEQUAL, 0.1f);
	
	rc->boundFrameBuffer  = frameBufferPntr;
	rc->boundShader       = shaderPntr;
	rc->boundBuffer       = nullptr;
	rc->boundTexture      = nullptr;
	rc->boundAlphaTexture = nullptr;
	rc->boundFont         = nullptr;
	
	rc->fontAlignment = Alignment_Left;
	rc->fontStyle = FontStyle_Default;
	rc->fontSize = 12;
	
	rc->worldMatrix       = Mat4_Identity;
	rc->viewMatrix        = Mat4_Identity;
	rc->projectionMatrix  = Mat4_Identity;
	
	rc->depth             = 1.0f;
	rc->sourceRectangle   = NewRec(0, 0, 1, 1);
	rc->sourceRecSkew     = 0.0f;
	rc->maskRectangle     = NewRec(0, 0, 0, 0);
	rc->shiftVec          = Vec2_Zero;
	
	rc->primaryColor      = White;
	rc->secondaryColor    = White;
	rc->replaceColor1     = TARGET_COLOR_1;
	rc->replaceColor2     = TARGET_COLOR_2;
	rc->replaceColor3     = TARGET_COLOR_3;
	rc->replaceColor4     = TARGET_COLOR_4;
	
	rc->gradientEnabled   = false;
	rc->circleRadius      = 0.0f;
	rc->circleInnerRadius = 0.0f;
	rc->saturation        = 1.0f;
	rc->brightness        = 1.0f;
	rc->time              = 0.0f;
	
	ClearArray(rc->shadows);
	
	glBindFramebuffer(GL_FRAMEBUFFER, (rc->boundFrameBuffer != nullptr) ? rc->boundFrameBuffer->id : 0);
	RcSetViewport(viewport);
	glUseProgram((rc->boundShader != nullptr) ? rc->boundShader->programId : 0);
	RcUpdateShader();
}

void RcClearColorBuffer(Color_t clearColor)
{
	v4 colorVec = NewVec4FromColor(clearColor);
	glClearColor(colorVec.x, colorVec.y, colorVec.z, colorVec.w);
	glClear(GL_COLOR_BUFFER_BIT);
}

void RcClearDepthBuffer(r32 clearDepth)
{
	glClearDepth(clearDepth);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void RcStartStencilDrawing()
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glDepthMask(GL_FALSE);
	glStencilMask(0xFF);
	glStencilFunc(GL_NEVER, 1, 0xFF);
	glStencilOp(GL_REPLACE, GL_KEEP, GL_KEEP);
}

void RcUseStencil()
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glStencilMask(0x00);
	glStencilFunc(GL_EQUAL, 1, 0xFF);
}

void RcDisableStencil()
{
	glDisable(GL_STENCIL_TEST);
}

// +--------------------------------------------------------------+
// |                        Bind Functions                        |
// +--------------------------------------------------------------+
void RcBindShader(const Shader_t* shaderPntr)
{
	rc->boundShader = shaderPntr;
	
	if (shaderPntr != nullptr)
	{
		glUseProgram(shaderPntr->programId);
		RcUpdateShader();
	}
	else { glUseProgram(0); }
}

void RcBindTexture(const Texture_t* texturePntr)
{
	rc->boundTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
	glUniform1i(rc->boundShader->locations.texture, 0);
	glUniform2f(rc->boundShader->locations.textureSize, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
}

void RcBindBuffer(const VertexBuffer_t* vertBufferPntr)
{
	rc->boundBuffer = vertBufferPntr;
	
	glBindVertexArray(rc->boundShader->vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, vertBufferPntr->id);
	glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
	glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
	glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
}

void RcBindFrameBuffer(const FrameBuffer_t* frameBuffer)
{
	rc->boundFrameBuffer = frameBuffer;
	
	if (frameBuffer != nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->id);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	RcSetViewport(rc->viewport);
}

void RcBindFont(Font_t* fontPntr)
{
	rc->boundFont = fontPntr;
}

// +--------------------------------------------------------------+
// |                 Set State Variable Functions                 |
// +--------------------------------------------------------------+

void RcSetColor(Color_t color)
{
	rc->primaryColor = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.primaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcSetSecondaryColor(Color_t color)
{
	rc->secondaryColor = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}

void RcSetReplaceColor1(Color_t color)
{
	rc->replaceColor1 = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.replaceColor1, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcSetReplaceColor2(Color_t color)
{
	rc->replaceColor2 = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.replaceColor2, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcSetReplaceColor3(Color_t color)
{
	rc->replaceColor3 = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.replaceColor3, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcSetReplaceColor4(Color_t color)
{
	rc->replaceColor4 = color;
	v4 colorVec = NewVec4FromColor(color);
	glUniform4f(rc->boundShader->locations.replaceColor4, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcDefaultReplaceColors()
{
	rc->replaceColor1 = TARGET_COLOR_1;
	rc->replaceColor2 = TARGET_COLOR_2;
	rc->replaceColor3 = TARGET_COLOR_3;
	rc->replaceColor4 = TARGET_COLOR_4;
	RcSetReplaceColor1(TARGET_COLOR_1);
	RcSetReplaceColor2(TARGET_COLOR_2);
	RcSetReplaceColor3(TARGET_COLOR_3);
	RcSetReplaceColor4(TARGET_COLOR_4);
}

void RcSetSourceRectangle(rec sourceRectangle)
{
	rc->sourceRectangle = sourceRectangle;
	glUniform4f(rc->boundShader->locations.sourceRectangle, sourceRectangle.x, sourceRectangle.y, sourceRectangle.width, sourceRectangle.height);
}
void RcDefaultSourceRectangle()
{
	RcSetSourceRectangle(NewRec(0, 0, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height));
}

void RcSetSourceRecSkew(r32 sourceRecSkew)
{
	rc->sourceRecSkew = sourceRecSkew;
	glUniform1f(rc->boundShader->locations.sourceRecSkew, sourceRecSkew);
}

void RcSetMaskRectangle(rec maskRectangle)
{
	rc->maskRectangle = maskRectangle;
	glUniform4f(rc->boundShader->locations.maskRectangle, maskRectangle.x, maskRectangle.y, maskRectangle.width, maskRectangle.height);
}

void RcSetGradientEnabled(bool gradientEnabled)
{
	rc->gradientEnabled = gradientEnabled;
	glUniform1i(rc->boundShader->locations.gradientEnabled, gradientEnabled ? 1 : 0);
}

void RcSetCircleRadius(r32 radius, r32 innerRadius = 0.0f)
{
	rc->circleRadius = radius;
	rc->circleInnerRadius = innerRadius;
	glUniform1f(rc->boundShader->locations.circleRadius, radius);
	glUniform1f(rc->boundShader->locations.circleInnerRadius, innerRadius);
}

void RcSetDepth(r32 depth)
{
	rc->depth = depth;
}

void RcSetSaturation(r32 saturation)
{
	rc->saturation = saturation;
	glUniform1f(rc->boundShader->locations.saturation, saturation);
}
void RcSetBrightness(r32 brightness)
{
	rc->brightness = brightness;
	glUniform1f(rc->boundShader->locations.brightness, brightness);
}

void RcSetTime(r32 time)
{
	rc->time = time;
	glUniform1f(rc->boundShader->locations.time, time);
}

void RcSetShadow(u32 shadowIndex, v3 position, r32 radius)
{
	Assert(shadowIndex < MAX_DROP_SHADOWS);
	rc->shadows[shadowIndex] = NewVec4(position.x, position.y, position.z, radius);
	glUniform4fv(rc->boundShader->locations.shadows, MAX_DROP_SHADOWS, &rc->shadows[0].values[0]);
}

void RcDisableShadow(u32 shadowIndex)
{
	Assert(shadowIndex < MAX_DROP_SHADOWS);
	RcSetShadow(shadowIndex, Vec3_Zero, 0.0f);
}

void RcSetShiftVec(v2 shiftVec)
{
	rc->shiftVec = shiftVec;
	glUniform2f(rc->boundShader->locations.shiftVec, shiftVec.x, shiftVec.y);
}

void RcSetFontSize(r32 fontSize)
{
	rc->fontSize = fontSize;
}

void RcSetFontStyle(u16 fontStyle)
{
	rc->fontStyle = fontStyle;
}

void RcSetFontAlignment(Alignment_t alignment)
{
	rc->fontAlignment = alignment;
}
