/*
File:   app.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Contains all the exported functions and #includes
	** the rest of the source code files.
*/

#define RELOAD_APPLICATION          false
#define USE_ASSERT_FAILURE_FUNCTION true

#include "plat_interface.h"
#include "app_version.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include "my_tempMemory.h"
#include "my_tempMemory.cpp"
#include "my_linkedList.h"
#include "my_boundedStrList.h"
#include "my_primitives3D.h"

// +--------------------------------------------------------------+
// |                     Application Includes                     |
// +--------------------------------------------------------------+
#include "app_defines.h"
#include "app_structs.h"
#include "app_sound.h"
#include "app_render_context.h"

#include "game.h"
#include "app.h"

// +--------------------------------------------------------------+
// |                 Application Global Variables                 |
// +--------------------------------------------------------------+
const PlatformInfo_t* platform = nullptr;
const AppInput_t* input = nullptr;
AppOutput_t* appOutput = nullptr;

AppData_t*  app  = nullptr;
GameData_t* game = nullptr;

MemoryArena_t* mainHeap = nullptr;
RenderContext_t* rc = nullptr;
v2 WindowSize = {};
v2 MousePos = {};
v2 MouseStartLeft = {};
v2 MouseStartRight = {};

// +--------------------------------------------------------------+
// |                   Application Source Files                   |
// +--------------------------------------------------------------+
#include "app_helpers.cpp" //everybody expects helpers ;)

#include "app_wav.cpp"
#include "app_loading_functions.cpp"
#include "app_font.cpp"
#include "app_sound.cpp"
#include "app_render_context.cpp"
#include "app_render_functions.cpp"
#include "app_font_flow.cpp"

// +--------------------------------------------------------------+
// |                    App State Source Files                    |
// +--------------------------------------------------------------+
#include "game.cpp"

// +--------------------------------------------------------------+
// |                       Internal Helpers                       |
// +--------------------------------------------------------------+
void App_LoadShaders()
{
	if (app->initialized)
	{
		DestroyShader(&app->mainShader);
		DestroyShader(&app->geomShader);
	}
	
	//                           folder          vertex name        folder          fragment name
	app->mainShader = LoadShader(SHADERS_FOLDER "main-vertex.glsl", SHADERS_FOLDER "main-fragment.glsl");
	app->geomShader = LoadShader(SHADERS_FOLDER "geometry-vertex.glsl", SHADERS_FOLDER "geometry-fragment.glsl");
}

void App_LoadFonts()
{
	if (app->initialized)
	{
		DestroyFont(&app->mainFont);
	}
	
	CreateGameFont(&app->mainFont, mainHeap);
	FontLoadFile(&app->mainFont, FONTS_FOLDER "consola.ttf", FontStyle_Default);
	FontBake(&app->mainFont, 12);
	FontBake(&app->mainFont, 16);
	FontBake(&app->mainFont, 18);
	FontBake(&app->mainFont, 20);
	FontBake(&app->mainFont, 24);
	FontDropFiles(&app->mainFont);
}

void App_LoadTextures()
{
	if (app->initialized)
	{
		DestroyTexture(&app->testSprite);
		DestroyTexture(&app->testTexture);
		DestroyTexture(&app->alphaTexture);
		
		DestroyTexture(&app->tileSheet1);
		DestroySpriteSheet(&app->hyperGuySheet);
	}
	
	//                               folder              name        pixelated  repeat
	app->testSprite    = LoadTexture(SPRITES_FOLDER     "weird.png",    true,   false);
	app->testTexture   = LoadTexture(TEXTURES_FOLDER    "test.png",     true,   false);
	app->alphaTexture  = LoadTexture(TEXTURES_FOLDER    "alpha.png",    true,   true);
	
	
	app->tileSheet1    = LoadTexture(SHEETS_FOLDER      "envTest1.png", true,   false);
	app->hyperGuySheet = LoadSpriteSheet(SHEETS_FOLDER  "hyperGuy.png", 1, NewVec2i(12, 4), true);
}

void App_LoadAudio()
{
	//                    folder         name
	app->testSound = LoadSound(SOUNDS_FOLDER "test.wav");
	
	app->testMusic = LoadSound(MUSIC_FOLDER  "test.wav");
}

void AppState_Initialize(AppState_t state)
{
	DEBUG_PrintLine("[Initializing %s...]", GetAppStateName(state));
	switch (state)
	{
		case AppState_Game: Game_Initialize(); break;
	};
}
void AppState_Start(AppState_t state, AppState_t oldAppState)
{
	DEBUG_PrintLine("[Starting %s...]", GetAppStateName(state));
	switch (state)
	{
		case AppState_Game: Game_Start(oldAppState); break;
	};
}
AppState_t AppState_UpdateAndRender(AppState_t state)
{
	switch (state)
	{
		case AppState_Game: return Game_UpdateAndRender();
		default: return state;
	};
}
void AppState_Deinitialize(AppState_t state)
{
	DEBUG_PrintLine("[Deinitializing %s...]", GetAppStateName(state));
	switch (state)
	{
		case AppState_Game: Game_Deinitialize(); break;
	};
}


// +--------------------------------------------------------------+
// |                       App Get Version                        |
// +--------------------------------------------------------------+
// Version_t App_GetVersion(bool* resetApplication)
EXPORT AppGetVersion_DEFINITION(App_GetVersion)
{
	Version_t version = { APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD };
	if (resetApplication != nullptr) { *resetApplication = RELOAD_APPLICATION; }
	return version;
}

// +--------------------------------------------------------------+
// |                        App Initialize                        |
// +--------------------------------------------------------------+
// void App_Initialize(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppInitialize_DEFINITION(App_Initialize)
{
	platform = PlatformInfo;
	WindowSize = NewVec2((r32)platform->screenSize.x / GUI_SCALE, (r32)platform->screenSize.y / GUI_SCALE);
	appOutput = nullptr;
	
	DEBUG_WriteLine("Initializing App...");
	
	// +==================================+
	// |          Memory Arenas           |
	// +==================================+
	app = (AppData_t*)AppMemory->permanantPntr;
	game = &app->gameData;
	TempArena = &app->tempArena;
	mainHeap = &app->mainHeap;
	rc = &app->renderContext;
	ClearPointer(app);
	u32 mainHeapSize = AppMemory->permanantSize - sizeof(AppData_t);
	InitializeMemoryArenaHeap(&app->mainHeap, app + 1, mainHeapSize);
	InitializeMemoryArenaTemp(&app->tempArena, AppMemory->transientPntr, AppMemory->transientSize, TRANSIENT_MAX_NUMBER_MARKS);
	
	TempPushMark();
	
	DEBUG_PrintLine("AppData_t:  %s", FormattedSizeStr(sizeof(AppData_t)));
	DEBUG_PrintLine("Main Heap:  %s", FormattedSizeStr(mainHeapSize));
	DEBUG_PrintLine("Temp Arena: %s", FormattedSizeStr(AppMemory->transientSize));
	
	// +================================+
	// |          Load Content          |
	// +================================+
	App_LoadShaders();
	App_LoadFonts();
	App_LoadTextures();
	App_LoadAudio();
	
	app->musicVolume = DEF_MUSIC_VOLUME;
	app->soundsVolume = DEF_SOUND_EFFECTS_VOLUME;
	
	// +================================+
	// |    External Initializations    |
	// +================================+
	InitializeRenderContext();
	
	app->appState = STARTING_APPSTATE;
	AppState_Initialize(app->appState);
	AppState_Start(app->appState, app->appState);
	
	TempPopMark();
	DEBUG_WriteLine("Initialization Done!");
	app->appInitTempHighWaterMark = ArenaGetHighWaterMark(TempArena);
	ArenaResetHighWaterMark(TempArena);
	app->initialized = true;
}

// +--------------------------------------------------------------+
// |                         App Reloaded                         |
// +--------------------------------------------------------------+
// void App_Reloaded(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloaded_DEFINITION(App_Reloaded)
{
	platform = PlatformInfo;
	app = (AppData_t*)AppMemory->permanantPntr;
	game = &app->gameData;
	TempArena = &app->tempArena;
	mainHeap = &app->mainHeap;
	rc = &app->renderContext;
	WindowSize = NewVec2((r32)platform->screenSize.x / GUI_SCALE, (r32)platform->screenSize.y / GUI_SCALE);
	
	DEBUG_WriteLine("App Reloaded");
	
	//TODO: Any function pointers that need to be remapped?
}

// +--------------------------------------------------------------+
// |                          App Update                          |
// +--------------------------------------------------------------+
// void App_Update(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
EXPORT AppUpdate_DEFINITION(App_Update)
{
	platform = PlatformInfo;
	input = AppInput;
	appOutput = AppOutput;
	app = (AppData_t*)AppMemory->permanantPntr;
	game = &app->gameData;
	TempArena = &app->tempArena;
	mainHeap = &app->mainHeap;
	rc = &app->renderContext;
	WindowSize = NewVec2((r32)platform->screenSize.x / GUI_SCALE, (r32)platform->screenSize.y / GUI_SCALE);
	MousePos = NewVec2(input->mousePos.x, input->mousePos.y) / (r32)GUI_SCALE;
	MouseStartLeft = NewVec2(input->mouseStartPos[MouseButton_Left].x, input->mouseStartPos[MouseButton_Left].y) / (r32)GUI_SCALE;
	MouseStartRight = NewVec2(input->mouseStartPos[MouseButton_Right].x, input->mouseStartPos[MouseButton_Right].y) / (r32)GUI_SCALE;
	AppOutput->cursorType = Cursor_Default;
	TempPushMark();
	ClearArray(app->buttonHandled);
	
	// +==============================+
	// |    Close Window Shortcut     |
	// +==============================+
	if (ButtonPressed(Button_W) && ButtonDown(Button_Control) && ButtonDown(Button_Shift))
	{
		AppOutput->closeWindow = true;
	}
	
	// +==============================+
	// |   Reload Content Shortcuts   |
	// +==============================+
	if (ButtonPressed(Button_F1))
	{
		DEBUG_WriteLine("Reloading Textures");
		App_LoadTextures();
	}
	if (ButtonPressed(Button_F2))
	{
		DEBUG_WriteLine("Reloading Audio");
		App_LoadAudio();
	}
	if (ButtonPressed(Button_F3))
	{
		DEBUG_WriteLine("Reloading Shaders");
		App_LoadShaders();
	}
	if (ButtonPressed(Button_F4))
	{
		DEBUG_WriteLine("Reloading Fonts");
		App_LoadFonts();
	}
	if (ButtonPressed(Button_F5))
	{
		DEBUG_WriteLine("Recreating Render Primitives");
		RcCreatePrimitives();
	}
	
	// +==============================+
	// |       External Updates       |
	// +==============================+
	UpdateSounds();
	
	// +--------------------------------------------------------------+
	// |             Update The Current Application State             |
	// +--------------------------------------------------------------+
	AppState_t newAppState = app->appState;
	if (app->transitionStartTime == 0)
	{
		newAppState = AppState_UpdateAndRender(app->appState);
	}
	
	// +--------------------------------------------------------------+
	// |                Draw Application Overlay Stuff                |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |     Overlay Render Setup     |
		// +==============================+
		{
			RcBegin(NewRec(Vec2_Zero, WindowSize), &app->mainShader, nullptr);
			RcClearDepthBuffer(1.0f);
			// RsBindFont(&app->mainFont);
		}
		
		//TODO: Draw Overlay
	}
	
	// +--------------------------------------------------------------+
	// |                      Change Game State                       |
	// +--------------------------------------------------------------+
	if (newAppState != app->appState)
	{
		DEBUG_PrintLine("[Changing application state to %s]", GetAppStateName(newAppState));
		app->transitionStartTime = platform->programTime;
		
		if (app->skipDeinitialization)
		{
			DEBUG_PrintLine("[Skipping Deinitialization of %s]", GetAppStateName(app->appState));
			app->skipDeinitialization = false;
		}
		else
		{
			AppState_Deinitialize(app->appState);
		}
		
		app->oldAppState = app->appState;
		app->appState = newAppState;
		
		if (app->skipInitialization)
		{
			DEBUG_PrintLine("[Skipping Initialization of %s]", GetAppStateName(app->appState));
			app->skipInitialization = false;
		}
		else
		{
			AppState_Initialize(app->appState);
		}
	}
	
	if (app->transitionStartTime != 0 &&
		platform->programTime >= app->transitionStartTime &&
		platform->programTime - app->transitionStartTime >= app->minTransitionTime)
	{
		app->transitionStartTime = 0;
		app->minTransitionTime = 0;
		AppState_Start(app->appState, app->oldAppState);
	}
	
	TempPopMark();
	if (ButtonPressed(Button_T) && ButtonDown(Button_Control)) { ArenaResetHighWaterMark(TempArena); } //TODO: Rebind this?
}

// +--------------------------------------------------------------+
// |                         App Closing                          |
// +--------------------------------------------------------------+
// void App_Closing(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppClosing_DEFINITION(App_Closing)
{
	platform = PlatformInfo;
	app = (AppData_t*)AppMemory->permanantPntr;
	game = &app->gameData;
	TempArena = &app->tempArena;
	mainHeap = &app->mainHeap;
	rc = &app->renderContext;
	WindowSize = NewVec2((r32)platform->screenSize.x / GUI_SCALE, (r32)platform->screenSize.y / GUI_SCALE);
	
	DEBUG_WriteLine("Application closing!");
	
	//TODO: Anything that needs to be released?
	app->initialized = false;
}

#if (DEBUG && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	DEBUG_PrintLine("Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr);
}
#endif