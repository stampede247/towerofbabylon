/*
File:   app_render_context.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_RENDER_CONTEXT_H
#define _APP_RENDER_CONTEXT_H

struct RenderContext_t
{
	VertexBuffer_t lineBuffer;
	VertexBuffer_t squareBuffer;
	VertexBuffer_t faceBuffer;
	VertexBuffer_t topBuffer;
	VertexBuffer_t equilTriangleBuffer;
	VertexBuffer_t cubeBuffer;
	VertexBuffer_t cubePartBuffer;
	VertexBuffer_t sphereBuffer;
	VertexBuffer_t cylinderBuffer;
	VertexBuffer_t rampBuffer;
	VertexBuffer_t circleBuffer;
	Texture_t dotTexture;
	Texture_t gradientTexture;
	Texture_t circleTexture;
	Texture_t ditherTexture;
	
	const FrameBuffer_t* boundFrameBuffer;
	const Shader_t* boundShader;
	const VertexBuffer_t* boundBuffer;
	const Texture_t* boundTexture;
	const Texture_t* boundAlphaTexture;
	
	r32 fontSize;
	u16 fontStyle;
	Alignment_t fontAlignment;
	Font_t* boundFont; //Can't be const since rendering might generate new glyphs
	
	mat4 worldMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	
	rec viewport;
	r32 depth;
	rec sourceRectangle;
	r32 sourceRecSkew;
	rec maskRectangle;
	v2 shiftVec;
	
	Color_t primaryColor;
	Color_t secondaryColor;
	Color_t replaceColor1;
	Color_t replaceColor2;
	Color_t replaceColor3;
	Color_t replaceColor4;
	
	bool gradientEnabled;
	r32  circleRadius;
	r32  circleInnerRadius;
	r32  saturation;
	r32  brightness;
	r32  time;
	
	v4 shadows[MAX_DROP_SHADOWS];
};

#endif //  _APP_RENDER_CONTEXT_H
