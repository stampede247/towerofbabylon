/*
File:   game.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Holds the code that handles AppState_Game and #includes "game_..." source files 
*/

// +--------------------------------------------------------------+
// |                       Helper Functions                       |
// +--------------------------------------------------------------+
//TODO: Add helper functions to be used by the other game files?

// +--------------------------------------------------------------+
// |                       Pre-Declarations                       |
// +--------------------------------------------------------------+
void EntityDraw(v3 roomPos, Entity_t* entity, r32 transitionAlpha, bool draw3d = false);
void RoomDrawEntities(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d = false);
void DrawTile(Room_t* room, Tile_t* tile, v3 roomPos, r32 transitionAlpha, bool draw3d = false);
void RoomDrawTiles(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d = false);
void DrawCollider(Collider_t* collider, v3 position, v3 size, r32 transitionAlpha, bool draw3d = false);
void RoomDrawColliders(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d = false);

// +--------------------------------------------------------------+
// |                      Game_ Source Files                      |
// +--------------------------------------------------------------+
#include "game_view3d.cpp"
#include "game_view.cpp"
#include "game_room.cpp"
#include "game_tower.cpp"
#include "game_tile.cpp"
#include "game_collision.cpp"
#include "game_entity.cpp"
#include "game_player.cpp"

// +--------------------------------------------------------------+
// |                      Private Functions                       |
// +--------------------------------------------------------------+
void GameFillRoom(Tower_t* tower, Room_t* room, u32 roomIndex)
{
	RoomRemoveAllTiles(room);
	
	for (i32 zPos = 0; zPos < room->size.depth; zPos += TILE_SIZE)
	{
		for (i32 xPos = 0; xPos < room->size.width; xPos += TILE_SIZE)
		{
			if (xPos == 0 || zPos == 0 || xPos >= room->size.width - TILE_SIZE || zPos >= room->size.depth - TILE_SIZE)
			{
				Tile_t* newTile = CreateTileBlock(room, NewVec3i(xPos, TILE_SIZE/2, zPos), NewVec3i(TILE_SIZE, TILE_SIZE, TILE_SIZE), NewVec2i(0, 0));
			}
			else if (RandU32(0, 10) >= 8)
			{
				v3i tileSize = NewVec3i(TILE_SIZE, (RandU32(0, 10) >= 5) ? TILE_SIZE : TILE_SIZE/2, TILE_SIZE);
				Tile_t* newTile = CreateTileBlock(room, NewVec3i(xPos, TILE_SIZE/2, zPos), tileSize, NewVec2i(0, 0));
				
				if (RandU32(0 , 10) >= 7 && tileSize.y == TILE_SIZE)
				{
					// v3i ladderSize = NewVec3i(TILE_SIZE, tileSize.y, 1);
					Tile_t* newLadder = CreateTileWallDecor(room, NewVec3i(xPos+5, TILE_SIZE/2, zPos + tileSize.z), NewRec(197, 24, 14, 24));
					FlagSet(newLadder->flags, TileFlag_Ladder);
				}
			}
		}
	}
	for (i32 zPos = 0; zPos < room->size.depth; zPos += TILE_SIZE)
	{
		for (i32 xPos = 0; xPos < room->size.width; xPos += TILE_SIZE)
		{
			if (xPos < 4*TILE_SIZE || xPos > 5*TILE_SIZE || zPos < 4*TILE_SIZE || zPos >5*TILE_SIZE)
			{
				Tile_t* newTile = CreateTileFloor(room, NewVec3i(xPos, COLLIDER_SIZE, zPos), NewVec2i(TILE_SIZE), NewVec2i(2, 0));
			}
		}
	}
	
	Tile_t* bookshelf = CreateTileBlock(room, NewVec3i(1*TILE_SIZE, TILE_SIZE/2, 1*TILE_SIZE), NewVec3i(TILE_SIZE*2, TILE_SIZE*3/2, TILE_SIZE/2), NewVec2i(0, 6));
	
	TowerAddElevatorTiles(tower, room);
	
	RoomGenerateColliders(tower, room);
}

// +--------------------------------------------------------------+
// |                       Initialize Game                        |
// +--------------------------------------------------------------+
void Game_Initialize()
{
	Assert(game != nullptr);
	
	GameViewInit(&game->view, NewVec2(100, 100));
	View3dInit(&game->view3d, Pi32/4, 3, 240);
	
	game->testPos = NewVec3(100, 100, 100);
	game->testRot = NewQuat(Vec3_Right, 0);
	
	TowerCreate(&game->tower, mainHeap, 5, NewVec3i(20, 10, 20) * TILE_SIZE, 2);
	game->tower.elevators[0].startFloor = 0;
	game->tower.elevators[0].endFloor = 2;
	game->tower.elevators[0].position = NewVec2i(5, 5) * TILE_SIZE;
	game->tower.elevators[0].size = NewVec2i(2*TILE_SIZE);
	game->tower.elevators[1].startFloor = 1;
	game->tower.elevators[1].endFloor = 4;
	game->tower.elevators[1].position = NewVec2i(10, 12) * TILE_SIZE;
	game->tower.elevators[1].size = NewVec2i(2*TILE_SIZE);
	for (u32 rIndex = 0; rIndex < game->tower.numRooms; rIndex++)
	{
		Room_t* room = &game->tower.rooms[rIndex];
		GameFillRoom(&game->tower, room, rIndex);
	}
	
	game->initialized = true;
}

// +--------------------------------------------------------------+
// |                          Start Game                          |
// +--------------------------------------------------------------+
void Game_Start(AppState_t oldAppState)
{
	Assert(game->initialized);
	
	game->view.targetPos = NewVec2(0, 0);
	game->view.position = game->view.targetPos;
	GameViewSync(&game->view);
	game->view3d.position = NewVec3(0, 0, 0);
	game->view3d.rotationHori = 0;
	game->view3d.rotationVert = 0;
	View3dSync(&game->view3d);
}

// +--------------------------------------------------------------+
// |                      Deinitialize Game                       |
// +--------------------------------------------------------------+
void Game_Deinitialize()
{
	Assert(game != nullptr);
	Assert(game->initialized == true);
	
	TowerDestroy(&game->tower);
	
	game->initialized = false;
	ClearPointer(game);
}

// +--------------------------------------------------------------+
// |                    Game Update and Render                    |
// +--------------------------------------------------------------+
AppState_t Game_UpdateAndRender()
{
	Assert(game != nullptr);
	Assert(game->initialized);
	
	AppState_t newAppState = AppState_Game;
	
	// +--------------------------------------------------------------+
	// |                         Game Update                          |
	// +--------------------------------------------------------------+
	{
		Room_t* currentRoom = GetCurrentRoom(&game->tower);
		v3 currentRoomPos = NewVec3(0, TowerGetRoomHeight(&game->tower, game->tower.focusRoom), 0);
		
		game->view.targetSize = NewVec2(640, 320);
		GameViewSync(&game->view);
		game->view3d.zFar = Vec3Length(NewVec3(currentRoom->size)*2);
		game->view3d.zNear = 2;
		View3dSync(&game->view3d);
		
		if (platform->timeDelta > 0)
		{
			IncrementU32By(game->use3dViewLastChange, (u32)platform->timeDelta);
		}
		
		if (ButtonPressed(Button_Enter))
		{
			GameFillRoom(&game->tower, currentRoom, game->tower.focusRoom);
		}
		if (ButtonPressed(Button_Backspace))
		{
			DEBUG_PrintLine("Reset everything");
			game->view.position = Vec2_Zero;
			game->view.targetPos = Vec2_Zero;
			game->view3d.position = currentRoomPos;
			game->view3d.position.x += (r32)currentRoom->size.width / 2;
			game->view3d.position.y += (r32)currentRoom->size.height+5;
			game->view3d.position.z += (r32)currentRoom->size.depth;
			game->view3d.rotationHori = Pi32*3/2;
			game->view3d.rotationVert = 0;
			RoomRemoveAllEntities(currentRoom);
		}
		if (ButtonPressed(Button_Tab))
		{
			game->use3dView = !game->use3dView;
			game->use3dViewLastChange = 0;
			game->view3d.position = currentRoomPos;
			game->view3d.position.x += (r32)currentRoom->size.width / 2;
			game->view3d.position.y += (r32)currentRoom->size.height+5;
			game->view3d.position.z += (r32)currentRoom->size.depth;
			game->view3d.rotationHori = Pi32*3/2;
			game->view3d.rotationVert = 0;
		}
		if (ButtonPressed(Button_Pipe))
		{
			game->drawColliders = !game->drawColliders;
		}
		
		// +==============================+
		// |  Check for Elevator Enters   |
		// +==============================+
		if (!game->insideElevator)
		{
			u32 playerRoomIndex = 0;
			Entity_t* player = TowerGetPlayer(&game->tower, 0, &playerRoomIndex);
			if (player != nullptr)
			{
				for (u32 eIndex = 0; eIndex < game->tower.numElevators; eIndex++)
				{
					Elevator_t* elevator = &game->tower.elevators[eIndex];
					if (playerRoomIndex >= elevator->startFloor && playerRoomIndex <= elevator->endFloor)
					{
						rec elevatorRec = NewRec(NewVec2(elevator->position), NewVec2(elevator->size));
						if (AbsR32(player->position.y - COLLIDER_SIZE) < 2.0f && //is on the ground
							RecOverlapsCircle(RecInflate(elevatorRec, 1), NewVec2(player->position.x, player->position.z), player->radius))
						{
							DEBUG_PrintLine("Entering elevator %u", eIndex);
							game->insideElevator = true;
							game->exitingElevator = false;
							game->elevatorEnterAnim = 0.0f;
							game->activeElevatorIndex = eIndex;
							game->elevatorCurrentFloor = (r32)playerRoomIndex;
							game->elevatorGotoFloor = game->elevatorCurrentFloor;
							game->playerEnterStartPos = player->position;
						}
						
					}
				}
			}
		}
		// +==============================+
		// |   Update Elevator Movement   |
		// +==============================+
		else
		{
			u32 playerRoomIndex = 0;
			Entity_t* player = TowerGetPlayer(&game->tower, 0, &playerRoomIndex); Assert(player != nullptr);
			PlayerData_t* playerData = GetPlayerData(player); Assert(playerData != nullptr);
			Elevator_t* elevator = &game->tower.elevators[game->activeElevatorIndex];
			
			if (game->exitingElevator)
			{
				game->elevatorEnterAnim -= 0.03f;
				if (game->elevatorEnterAnim <= 0.0f)
				{
					DEBUG_WriteLine("Finished exiting elevator");
					game->elevatorEnterAnim = 0.0f;
					game->insideElevator = false;
					game->exitingElevator = false;
					player->velocity = Vec3_Zero;
					playerData->isRunning = false;
				}
				else
				{
					player->velocity = NewVec3(0, 0, PLAYER_MAX_SPEED); //for animation purposes
					playerData->isRunning = true;
				}
				v2 elevatorCenter = NewVec2(elevator->position) + NewVec2(elevator->size)/2.0f;
				player->position.x = elevatorCenter.x;
				player->position.z = LerpR32(elevatorCenter.y, elevatorCenter.y + (r32)elevator->size.y/2 + player->radius + 5, 1-game->elevatorEnterAnim);
				playerData->facingDir = Dir2_Down;
				playerData->facingAngle = Pi32/2;
			}
			else if (game->elevatorEnterAnim < 1.0f)
			{
				game->elevatorEnterAnim += 0.02f;
				if (game->elevatorEnterAnim >= 1.0f)
				{
					DEBUG_WriteLine("Finished entering elevator");
					game->elevatorEnterAnim = 1.0f;
					v3 elevatorCenter = NewVec3(elevator->position.x + elevator->size.x/2.0f, game->playerEnterStartPos.y, elevator->position.y + elevator->size.y/2.0f);
					player->position = Vec3Lerp(game->playerEnterStartPos, elevatorCenter, game->elevatorEnterAnim);
					player->velocity = Vec3_Zero;
					playerData->facingDir = Dir2_Down;
					playerData->facingAngle = Pi32/2;
					playerData->isRunning = false;
				}
				else
				{
					v3 elevatorCenter = NewVec3(elevator->position.x + elevator->size.x/2.0f, game->playerEnterStartPos.y, elevator->position.y + elevator->size.y/2.0f);
					player->position = Vec3Lerp(game->playerEnterStartPos, elevatorCenter, game->elevatorEnterAnim);
					player->velocity = Vec3Normalize(elevatorCenter - player->position) * PLAYER_MAX_SPEED;
					playerData->facingAngle = AtanR32(player->velocity.z, player->velocity.x);
					playerData->facingDir = NewDir2(NewVec2(player->velocity.x, player->velocity.z));
					playerData->isRunning = true;
				}
			}
			else
			{
				if (game->elevatorCurrentFloor == game->elevatorGotoFloor)
				{
					if (ButtonPressed(Button_Up) && game->elevatorGotoFloor < (r32)elevator->endFloor)
					{
						game->elevatorGotoFloor += 1.0f;
					}
					if (ButtonPressed(Button_Down) && game->elevatorGotoFloor > 0.0f)
					{
						game->elevatorGotoFloor -= 1.0f;
					}
				}
				
				if (game->elevatorCurrentFloor < game->elevatorGotoFloor)
				{
					game->elevatorCurrentFloor += 0.03f;
					if (game->elevatorCurrentFloor >= game->elevatorGotoFloor)
					{
						DEBUG_WriteLine("Elevator finished moving up");
						game->elevatorCurrentFloor = game->elevatorGotoFloor;
					}
				}
				else if (game->elevatorCurrentFloor > game->elevatorGotoFloor)
				{
					game->elevatorCurrentFloor -= 0.03f;
					if (game->elevatorCurrentFloor <= game->elevatorGotoFloor)
					{
						DEBUG_WriteLine("Elevator finished moving down");
						game->elevatorCurrentFloor = game->elevatorGotoFloor;
					}
				}
				
				if (game->elevatorCurrentFloor != game->elevatorGotoFloor)
				{
					u32 lastFloor, nextFloor;
					r32 floorLerp;
					if (game->elevatorGotoFloor < game->elevatorCurrentFloor)
					{
						lastFloor = CeilR32i(game->elevatorCurrentFloor);
						nextFloor = FloorR32i(game->elevatorGotoFloor);
						floorLerp = -(game->elevatorCurrentFloor - (r32)lastFloor);
					}
					else
					{
						lastFloor = FloorR32i(game->elevatorCurrentFloor);
						nextFloor = FloorR32i(game->elevatorGotoFloor);
						floorLerp = (game->elevatorCurrentFloor - (r32)lastFloor);
					}
					r32 playerRoomHeight = TowerGetRoomHeight(&game->tower, playerRoomIndex);
					r32 lastFloorHeight = TowerGetRoomHeight(&game->tower, lastFloor);
					r32 nextFloorHeight = TowerGetRoomHeight(&game->tower, nextFloor);
					player->position.y = LerpR32(lastFloorHeight + COLLIDER_SIZE, nextFloorHeight + COLLIDER_SIZE, floorLerp) - playerRoomHeight;
				}
				else
				{
					u32 currentFloor = (u32)RoundR32i(game->elevatorGotoFloor);
					if (playerRoomIndex != currentFloor)
					{
						DEBUG_PrintLine("Moving player from floor %u to floor %u", playerRoomIndex, currentFloor);
						player = TowerMoveEntityToRoom(&game->tower, player, playerRoomIndex, currentFloor);
						playerRoomIndex = currentFloor;
						playerData = GetPlayerData(player);
					}
					player->position.y = COLLIDER_SIZE;
					
					Buttons_t exitBtn = Button_Z;
					if (ButtonPressed(exitBtn))
					{
						DEBUG_WriteLine("Exiting elevator...");
						game->exitingElevator = true;
						game->elevatorEnterAnim = 1.0f;
					}
				}
				game->tower.focusRoom = RoundR32i(game->elevatorGotoFloor);
			}
		}
		
		// +==============================+
		// |       Room Transitions       |
		// +==============================+
		if (!game->insideElevator)
		{
			Entity_t* player = GetPlayer(currentRoom);
			if (ButtonPressed(Button_Plus) && game->tower.focusRoom < game->tower.numRooms-1)
			{
				DEBUG_WriteLine("Going to next room");
				game->tower.focusRoom++;
				player = TowerMoveEntityToRoom(&game->tower, player, game->tower.focusRoom-1, game->tower.focusRoom);
				player->position.y = TILE_SIZE/2;
			}
			else if (ButtonPressed(Button_Minus) && game->tower.focusRoom > 0)
			{
				DEBUG_WriteLine("Going to previous room");
				game->tower.focusRoom--;
				player = TowerMoveEntityToRoom(&game->tower, player, game->tower.focusRoom+1, game->tower.focusRoom);
			}
			
			if (player != nullptr && player->position.y + player->height <= 0.0f && game->tower.focusRoom > 0)
			{
				DEBUG_WriteLine("Dropping to previous room");
				game->tower.focusRoom--;
				player = TowerMoveEntityToRoom(&game->tower, player, game->tower.focusRoom+1, game->tower.focusRoom);
			}
		}
		
		TowerUpdateRoomFades(&game->tower);
		currentRoom = GetCurrentRoom(&game->tower);
		currentRoomPos = NewVec3(0, TowerGetRoomHeight(&game->tower, game->tower.focusRoom), 0);
		
		// +==============================+
		// |         Update View          |
		// +==============================+
		GameViewUpdate(&game->view);
		GameViewSync(&game->view);
		bool doMouseMovement = (game->use3dView && game->use3dViewLastChange >= 200);
		View3dUpdate(&game->view3d, game->use3dView, doMouseMovement);
		View3dSync(&game->view3d);
		
		// +==============================+
		// |        View Movement         |
		// +==============================+
		if (game->use3dView)
		{
			appOutput->recenterMouse = true;
		}
		else
		{
			appOutput->recenterMouse = false;
			
			u32 playerRoomIndex = 0;
			Entity_t* player = TowerGetPlayer(&game->tower, 0, &playerRoomIndex);
			if (player != nullptr)
			{
				v3 playerRoomPos = NewVec3(0, TowerGetRoomHeight(&game->tower, playerRoomIndex), 0);
				game->view.targetPos = RoomPosToWorldPos(&game->tower.rooms[playerRoomIndex], playerRoomPos + player->position);
			}
			else
			{
				r32 viewMoveSpeed = 2;
				if (ButtonDown(Button_Shift)) { viewMoveSpeed = 10; }
				if (ButtonDown(Button_W)) { game->view.targetPos.y -= viewMoveSpeed; }
				if (ButtonDown(Button_A)) { game->view.targetPos.x -= viewMoveSpeed; }
				if (ButtonDown(Button_S)) { game->view.targetPos.y += viewMoveSpeed; }
				if (ButtonDown(Button_D)) { game->view.targetPos.x += viewMoveSpeed; }
			}
		}
		
		if (game->use3dView)
		{
			if (ButtonPressed(MouseButton_Left))
			{
				Entity_t* newEntity = CreateEntity(currentRoom, EntityType_Player, game->view3d.position);
				newEntity->velocity = game->view3d.forwardVec * 2;
			}
			if (ButtonPressed(MouseButton_Right))
			{
				Entity_t* newEntity = CreateEntity(currentRoom, EntityType_Enemy, game->view3d.position);
				newEntity->velocity = game->view3d.forwardVec * 2;
			}
		}
		else
		{
			v3 mouseRoomPos = WorldPosToRoomPos(currentRoom, ScreenPosToWorldPos(&game->view, MousePos), currentRoomPos.y + TILE_SIZE/2);
			v3i mouseGridPos = Vec3Floori(Vec3Divide(mouseRoomPos, NewVec3(TILE_SIZE)));
			v3 mouseGridCenter = NewVec3(mouseGridPos * TILE_SIZE) + NewVec3(TILE_SIZE/2.0f);
			v3i mousePlacePos = NewVec3i(mouseGridPos.x * TILE_SIZE, TILE_SIZE/2, mouseGridPos.z * TILE_SIZE);
			// +==============================+
			// |       Debug Add Tiles        |
			// +==============================+
			if (ButtonPressed(MouseButton_Left))
			{
				if (mouseGridPos.x >= 0 && mouseGridPos.x < currentRoom->colGridSize.width &&
					mouseGridPos.y >= 0 && mouseGridPos.y < currentRoom->colGridSize.height &&
					mouseGridPos.z >= 0 && mouseGridPos.z < currentRoom->colGridSize.depth)
				{
					DEBUG_WriteLine("Adding tile");
					// Tile_t* newTile = CreateTileTree(currentRoom, Vec3Roundi(mouseRoomPos), NewVec2i(1, 3), NewRec(84, 0, 47, 48));
					// Tile_t* newTile = CreateTileTree(currentRoom, Vec3Roundi(mouseRoomPos), NewVec2i(2, 4), NewRec(137, 5, 16, 55));
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(25, 25, 17, 15)); //Snow Decor
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(48, 30, 5, 6)); //Dirt Divot 1
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(55, 32, 3, 4)); //Dirt Divot 2
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(48, 48, 12, 12)); //Tile Full
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(60, 48, 12, 12)); //Tile Half
					// Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(48, 60, 12, 12)); //Tile Design
					Tile_t* newTile = CreateTileFloorDecor(currentRoom, Vec3Roundi(mouseRoomPos), NewRec(60, 60, 12, 12)); //Tile Quarter
					RoomGenerateColliders(&game->tower, currentRoom);
				}
				else
				{
					DEBUG_PrintLine("Outside (%d, %d, %d)", mouseGridPos.x, mouseGridPos.y, mouseGridPos.z);
				}
			}
			if (ButtonDown(MouseButton_Right))
			{
				Tile_t* existingTile = RoomGetTile(currentRoom, mouseRoomPos + NewVec3(0, 1, 0));
				if (existingTile != nullptr)
				{
					DEBUG_WriteLine("Removing tile");
					RoomRemoveTile(currentRoom, existingTile);
					RoomGenerateColliders(&game->tower, currentRoom);
				}
			}
		}
		
		// +==============================+
		// |        Entity Updates        |
		// +==============================+
		if (currentRoom != nullptr)
		{
			currentRoom->loopingOverEntities++;
			for (u32 eIndex = 0; eIndex < currentRoom->numEntities; eIndex++)
			{
				Entity_t* entityPntr = &currentRoom->entities[eIndex];
				if (entityPntr->type == EntityType_Player && game->insideElevator) { continue; } //don't update the player entity while doing elevator stuff
				
				if (IsFlagSet(entityPntr->flags, EntityFlag_Alive))
				{
					if (entityPntr->type == EntityType_Player)
					{
						PlayerData_t* player = GetPlayerData(entityPntr);
						if (!game->use3dView) { PlayerUpdate(currentRoom, entityPntr); }
						EntityUpdate(currentRoom, entityPntr, !player->climbingLadder);
					}
					else
					{
						EntityUpdate(currentRoom, entityPntr, true);
					}
				}
			}
			currentRoom->loopingOverEntities--;
			RoomAssimilateEntities(currentRoom);
		}
	}
	
	// +--------------------------------------------------------------+
	// |                         Game Render                          |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |         Render Setup         |
		// +==============================+
		r32 sinValue = (SinR32((platform->programTime/355.0f)*6.0f) + 1.0f) / 2.0f;
		{
			RcBegin(NewRec(Vec2_Zero, WindowSize), &app->mainShader, nullptr);
			RcClearDepthBuffer(1.0f);
			Color_t backgroundColor = Black;
			RcClearColorBuffer(backgroundColor);
			RcBindFont(&app->mainFont);
			RcSetFontSize(12);
		}
		
		Room_t* currentRoom = GetCurrentRoom(&game->tower);
		v3 currentRoomPos = NewVec3(0, TowerGetRoomHeight(&game->tower, game->tower.focusRoom), 0);
		
		// +==============================+
		// |         Drop Shadow          |
		// +==============================+
		for (u32 pIndex = 0; pIndex < MAX_DROP_SHADOWS; pIndex++)
		{
			Entity_t* player = GetPlayer(currentRoom, pIndex);	
			if (player == nullptr) { break; }
			RcSetShadow(pIndex, currentRoomPos + NewVec3(player->position.x, player->position.y + 2, player->position.z), player->radius+2);
		}
		
		if (game->use3dView)
		{
			View3dApply(&game->view3d);
		}
		else
		{
			GameViewApply(&game->view);
		}
		TowerDrawRooms(&game->tower, Vec3_Zero, game->use3dView);
		
		if (game->use3dView)
		{
			// RcDrawGradientTop(NewVec3(-1000, -1, -1000), NewVec2(2000, 2000), NewColor(Color_Red), NewColor(Color_Blue), Dir2_Right);
			RcDrawCubeOutline(currentRoomPos, NewVec3(currentRoom->size), PureYellow);
			
			// game->testRot = NewQuat(Vec3_Right, 0);
			// quat testQuat = NewQuat(Vec3Normalize(game->view3d.position - pos), Oscillate(0, Pi32, 2000));
			// quat startQuat = NewQuat(Vec3_Right, Pi32/8);
			// quat endQuat = NewQuat(Vec3_Up, Pi32/8);
			// quat testQuat = QuatLerp(startQuat, endQuat, OscillateSaw(0, 1, 2000));
			// game->testRot = QuatLerp(NewQuat(Vec3_Right, ToRadians(0)), NewQuat(Vec3_Right, ToRadians(90)), OscillateSaw(0, 1, 2000));
			// game->testRot = NewQuat(Vec3_Right, 0*Pi32);
			if (ButtonDown(Button_Right)) game->testRot = QuatLocalRot(game->testRot,  Vec3_Up,    ToRadians( 1));
			if (ButtonDown(Button_Left))  game->testRot = QuatLocalRot(game->testRot,  Vec3_Up,    ToRadians(-1));
			if (ButtonDown(Button_Up))    game->testRot = QuatLocalRot(game->testRot,  Vec3_Right, ToRadians( 1));
			if (ButtonDown(Button_Down))  game->testRot = QuatLocalRot(game->testRot,  Vec3_Right, ToRadians(-1));
			if (ButtonDown(Button_L))     game->testRot = QuatGlobalRot(game->testRot, Vec3_Up,    ToRadians( 1));
			if (ButtonDown(Button_J))     game->testRot = QuatGlobalRot(game->testRot, Vec3_Up,    ToRadians(-1));
			if (ButtonDown(Button_I))     game->testRot = QuatGlobalRot(game->testRot, Vec3_Right, ToRadians( 1));
			if (ButtonDown(Button_K))     game->testRot = QuatGlobalRot(game->testRot, Vec3_Right, ToRadians(-1));
			// game->testRot = QuatMult(game->testRot, NewQuat(Vec3_Up, ToRadians(1)), true);
			// game->testRot = QuatMult(game->testRot, NewQuat(Vec3_Right, ToRadians(2)), true);
			// game->testRot = QuatMult(game->testRot, NewQuat(Vec3_Forward, ToRadians(-1)), true);
			RcDrawQuatCubeFaces(game->testPos, NewVec3(50, 25, 40), game->testRot, SideBit_All, PureRed, ColorOrange, PureYellow, PureGreen, PureBlue, White);
			RcDrawLine3D(game->testPos, game->testPos + QuatGetAxis(game->testRot)*100, 1, ColorCyan);
			
			RcClearDepthBuffer(1.0f);
			v3 right = Vec3_Right;
			v3 up = Vec3_Up;
			v3 forward = Vec3_Forward;
			right   = Mat4MultiplyVec3(Mat4Quaternion(game->testRot), right);
			up      = Mat4MultiplyVec3(Mat4Quaternion(game->testRot), up);
			forward = Mat4MultiplyVec3(Mat4Quaternion(game->testRot), forward);
			RcDrawLine3D(game->testPos, game->testPos + right*10,   1, PureRed);
			RcDrawLine3D(game->testPos, game->testPos + up*10,      1, PureGreen);
			RcDrawLine3D(game->testPos, game->testPos + forward*10, 1, PureBlue);
		}
		
		// +==============================+
		// |             HUD              |
		// +==============================+
		{
			RcBindShader(&app->mainShader);
			RcSetViewMatrix(Mat4_Identity);
			RcSetViewport(NewRec(Vec2_Zero, WindowSize));
			RcClearDepthBuffer(1.0f);
			RcBindFont(&app->mainFont);
		}
		
		// RcDrawGradient(NewRec(100, 100, 1024, 1024), ColorTransparent(1.0f), ColorTransparent(NewColor(Color_Red), 0.0f), Dir2_Right);
		
		// RcDrawRectangle(NewRec(100, 100, 10, OscillateSaw(10, 100, 2000)), White);
		
		// RcDrawCircle(game->screenPos, 10, NewColor(Color_OrangeRed));
		
		RcPrintString(NewVec2(5, 5 + RcGetLineHeight()), White, "Pixel Scaling: %d", game->view.pixelScaling);
		if (game->use3dView)
		{
			RcPrintString(NewVec2(5, 5*2 + RcGetLineHeight()*2), White,
				"ViewPos: (%.1f, %.1f, %.1f) Rotation: (%.2f, %.2f)",
				game->view3d.position.x, game->view3d.position.y, game->view3d.position.z,
				game->view3d.rotationHori, game->view3d.rotationVert
			);
			// RcPrintString(NewVec2(5, 5*3 + RcGetLineHeight()*3), White,
			// 	"fovAngleY = %.1f zNear = %.1f zFar = %.1f",
			// 	game->view3d.fovAngleY, game->view3d.zNear, game->view3d.zFar
			// );
			RcPrintString(NewVec2(5, 5*3 + RcGetLineHeight()*3), White,
				"(%.1f,%.1f,%.1f) %.3f",
				game->testRot.x, game->testRot.y, game->testRot.z, game->testRot.angle
			);
		}
		RcPrintString(NewVec2(5, 5*3 + RcGetLineHeight()*4), White,
			"Room (%.1f,%.1f,%.1f)",
			game->roomPos.x, game->roomPos.y, game->roomPos.z
		);
		RcPrintString(NewVec2(5, 5*3 + RcGetLineHeight()*5), White,
			"World (%.1f,%.1f)",
			game->worldPos.x, game->worldPos.y
		);
		RcPrintString(NewVec2(5, 5*3 + RcGetLineHeight()*6), White,
			"View (%.1f,%.1f)",
			game->screenPos.x, game->screenPos.y
		);
	}
	
	return newAppState;
}
