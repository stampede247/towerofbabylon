/*
File:   game_collision.cpp
Author: Taylor Robbins
Date:   09\10\2018
Description: 
	** Holds the functions that handle the collision detection algorithm and general movement techniques 
*/

Range_t GetColliderProjection(Collider_t* collider, v3 colliderPos, v3 axis)
{
	v3 verts[] = {
		{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
		{0, 0, 1}, {1, 0, 1}, {0, 1, 1}, {1, 1, 1}
	};
	Range_t result = {};
	for (u32 vIndex = 0; vIndex < ArrayCount(verts); vIndex++)
	{
		r32 value = 0;
		v3 vert = colliderPos + (verts[vIndex] * (r32)COLLIDER_SIZE);
		if (axis == Vec3_Left || axis == Vec3_Right) { value = vert.x; }
		else if (axis == Vec3_Up || axis == Vec3_Down) { value = vert.y; }
		else if (axis == Vec3_Forward || axis == Vec3_Backward) { value = vert.z; }
		else
		{
			value = Vec3Dot(vert, axis);
		}
		
		if (vIndex == 0 || value < result.min)
		{
			result.min = value;
		}
		if (vIndex == 0 || value > result.max)
		{
			result.max = value;
		}
	}
	return result;
}

Range_t GetCylinderProjection(v3 cylinderPos, r32 radius, r32 height, v3 axis)
{
	Range_t result = {};
	
	if (axis == Vec3_Left || axis == Vec3_Right)
	{
		result.min = cylinderPos.x - radius;
		result.max = cylinderPos.x + radius;
	}
	else if (axis == Vec3_Up || axis == Vec3_Down)
	{
		result.min = cylinderPos.y;
		result.max = cylinderPos.y + height;
	}
	else if (axis == Vec3_Forward || axis == Vec3_Backward)
	{
		result.min = cylinderPos.z - radius;
		result.max = cylinderPos.z + radius;
	}
	else
	{
		v3 verts[4];
		verts[0] = cylinderPos;
		verts[1] = NewVec3(cylinderPos.x, cylinderPos.y + height, cylinderPos.z);
		if (Vec2Length(NewVec2(axis.x, axis.z)) > 0)
		{
			v2 axisNormalized = Vec2Normalize(NewVec2(axis.x, axis.z));
			verts[2] = cylinderPos + NewVec3(axisNormalized.x * radius, 0, axisNormalized.y * radius);
			verts[3] = cylinderPos - NewVec3(axisNormalized.x * radius, 0, axisNormalized.y * radius);
		}
		else
		{
			verts[2] = cylinderPos;
			verts[3] = cylinderPos;
		}
		
		for (u8 vIndex = 0; vIndex < ArrayCount(verts); vIndex++)
		{
			r32 value = Vec3Dot(verts[vIndex], axis);
			if (vIndex == 0 || value < result.min)
			{
				result.min = value;
			}
			if (vIndex == 0 || value > result.max)
			{
				result.max = value;
			}
		}
	}
	
	return result;
}

bool GetIntersectionDepth(Range_t r1, Range_t r2, r32* offsetOut = nullptr)
{
	if (r1.max < r2.min)
	{
		if (offsetOut != nullptr) { *offsetOut = r2.min - r1.max; } //positive
		return false;
	}
	if (r2.max < r1.min)
	{
		if (offsetOut != nullptr) { *offsetOut = r2.max - r1.min; } //negative
		return false;
	}
	
	r32 negOffset = r2.min - r1.max; Assert(negOffset <= 0);
	r32 posOffset = r2.max - r1.min; Assert(posOffset >= 0);
	if (AbsR32(negOffset) <= AbsR32(posOffset))
	{
		if (offsetOut != nullptr) { *offsetOut = negOffset; } //negative
		return true;
	}
	else
	{
		if (offsetOut != nullptr) { *offsetOut = posOffset; } //positive
		return true;
	}
}

r32 GetCylinderColliderIntersectionDepth(Collider_t* collider, v3 colliderPos, v3 cylinderPos, r32 cylinderRadius, r32 cylinderHeight, v3* axisOut = nullptr)
{
	v3 colliderVerts[] = {
		{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
		// {0, 0, 1}, {1, 0, 1}, {0, 1, 1}, {1, 1, 1} //NOTE: We only care about the horizontal differences to compare to cylinder. Top vertices are not needed here
	};
	u32 numColliderVerts = ArrayCount(colliderVerts);
	u32 numAxes = 3 + numColliderVerts;
	
	TempPushMark();
	v3* axes = PushArray(TempArena, v3, numAxes);
	axes[0] = Vec3_Up;
	axes[1] = Vec3_Right;
	axes[2] = Vec3_Forward;
	for (u32 vIndex = 0; vIndex < numColliderVerts; vIndex++)
	{
		v3 vert = colliderPos + colliderVerts[vIndex]*(r32)COLLIDER_SIZE;
		axes[3+vIndex] = vert - cylinderPos;
		axes[3+vIndex].y = 0;
		axes[3+vIndex] = Vec3Normalize(axes[3+vIndex]);
	}
	
	bool resultIsPrefered = false;
	r32 result = 0;
	v3 resultAxis = Vec3_Zero;
	u32 resultAxisIndex = 0;
	for (u32 aIndex = 0; aIndex < numAxes; aIndex++)
	{
		Range_t colProjection = GetColliderProjection(collider, colliderPos, axes[aIndex]);
		Range_t cylProjection = GetCylinderProjection(cylinderPos, cylinderRadius, cylinderHeight, axes[aIndex]);
		
		r32 offset = 0;
		if (GetIntersectionDepth(cylProjection, colProjection, &offset))
		{
			bool isAxisPrefered = true;
			if (aIndex == 0 && offset < 0 && !IsFlagSet(collider->sides, SideBit_Bottom))  { isAxisPrefered = false; }
			if (aIndex == 0 && offset > 0 && !IsFlagSet(collider->sides, SideBit_Top))     { isAxisPrefered = false; }
			if (aIndex == 1 && offset < 0 && !IsFlagSet(collider->sides, SideBit_Left))    { isAxisPrefered = false; }
			if (aIndex == 1 && offset > 0 && !IsFlagSet(collider->sides, SideBit_Right))   { isAxisPrefered = false; }
			if (aIndex == 2 && offset < 0 && !IsFlagSet(collider->sides, SideBit_Back))    { isAxisPrefered = false; }
			if (aIndex == 2 && offset > 0 && !IsFlagSet(collider->sides, SideBit_Front))   { isAxisPrefered = false; }
			if (aIndex >= 3 && axes[aIndex].y == 0)
			{
				if (axes[aIndex].x >= 0 && axes[aIndex].x > AbsR32(axes[aIndex].z)         && !IsFlagSet(collider->sides, SideBit_Left))  { isAxisPrefered = false; }
				if (axes[aIndex].x <= 0 && AbsR32(axes[aIndex].x) > AbsR32(axes[aIndex].z) && !IsFlagSet(collider->sides, SideBit_Right)) { isAxisPrefered = false; }
				if (axes[aIndex].z <= 0 && AbsR32(axes[aIndex].z) > AbsR32(axes[aIndex].x) && !IsFlagSet(collider->sides, SideBit_Front)) { isAxisPrefered = false; }
				if (axes[aIndex].z >= 0 && axes[aIndex].z > AbsR32(axes[aIndex].x)         && !IsFlagSet(collider->sides, SideBit_Back))  { isAxisPrefered = false; }
			}
			
			if (aIndex == 0 || (AbsR32(offset) < result-SMALL_NUMBER && (isAxisPrefered || !resultIsPrefered)))
			{
				result = AbsR32(offset);
				resultAxis = SignOfR32(offset) * axes[aIndex];
				resultAxisIndex = aIndex;
				resultIsPrefered = isAxisPrefered;
			}
		}
		else
		{
			//Seperating axis means no intersection
			//TODO: Do we want to actually return how far away we are from intersecting?
			TempPopMark();
			return 0;
		}
	}
	
	TempPopMark();
	if (result != 0 && !resultIsPrefered) { DEBUG_PrintLine("Resolved on unprefered axis %u (%.1f, %.1f, %.f)", resultAxisIndex, resultAxis.x, resultAxis.y, resultAxis.z); }
	if (axisOut != nullptr) { *axisOut = resultAxis; }
	return result;
}

void RoomGenerateColliders(Tower_t* tower, Room_t* room)
{
	Assert(room != nullptr);
	
	if (room->colliders != nullptr)
	{
		Assert(room->allocArena != nullptr);
		ArenaPop(room->allocArena, room->colliders);
		room->colliders = nullptr;
	}
	
	room->colGridSize = room->size / COLLIDER_SIZE;
	if ((room->size.width % COLLIDER_SIZE) != 0)  { room->colGridSize.x++; }
	if ((room->size.height % COLLIDER_SIZE) != 0) { room->colGridSize.y++; }
	if ((room->size.depth % COLLIDER_SIZE) != 0)  { room->colGridSize.z++; }
	
	if (room->colGridSize.x < 0 || room->colGridSize.y < 0 || room->colGridSize.z < 0)
	{
		room->colGridSize = Vec3i_Zero;
	}
	if (room->colGridSize == Vec3i_Zero) { return; }
	
	u32 numColliders = room->colGridSize.width * room->colGridSize.height * room->colGridSize.depth;
	room->colliders = PushArray(room->allocArena, Collider_t, numColliders);
	Assert(room->colliders != nullptr);
	//initialize colliders array
	for (i32 yPos = 0; yPos < room->colGridSize.height; yPos++)
	{
		for (i32 zPos = 0; zPos < room->colGridSize.depth; zPos++)
		{
			for (i32 xPos = 0; xPos < room->colGridSize.width; xPos++)
			{
				Collider_t* collider = RoomGetCollider(room, NewVec3i(xPos, yPos, zPos));
				Assert(collider != nullptr);
				ClearPointer(collider);
				collider->sides = SideBit_All;
			}
		}
	}
	
	//Loop through all tiles and have them generate colliders
	for (u32 tIndex = 0; tIndex < room->numTiles; tIndex++)
	{
		Tile_t* tilePntr = &room->tiles[tIndex];
		if (IsFlagSet(tilePntr->flags, TileFlag_Solid))
		{
			v3i gridPos1 = tilePntr->position / COLLIDER_SIZE;
			v3i gridPos2 = (tilePntr->position + tilePntr->size) / COLLIDER_SIZE;
			if (((tilePntr->position.x + tilePntr->size.width)  % COLLIDER_SIZE) != 0) { gridPos2.x++; }
			if (((tilePntr->position.y + tilePntr->size.height) % COLLIDER_SIZE) != 0) { gridPos2.y++; }
			if (((tilePntr->position.z + tilePntr->size.depth)  % COLLIDER_SIZE) != 0) { gridPos2.z++; }
			if (gridPos1.x < 0) { gridPos1.x = 0; } if (gridPos1.x > room->size.width)  { gridPos1.x = room->size.width;  }
			if (gridPos1.y < 0) { gridPos1.y = 0; } if (gridPos1.y > room->size.height) { gridPos1.y = room->size.height; }
			if (gridPos1.z < 0) { gridPos1.z = 0; } if (gridPos1.z > room->size.depth)  { gridPos1.z = room->size.depth;  }
			if (gridPos2.x < 0) { gridPos2.x = 0; } if (gridPos2.x > room->size.width)  { gridPos2.x = room->size.width;  }
			if (gridPos2.y < 0) { gridPos2.y = 0; } if (gridPos2.y > room->size.height) { gridPos2.y = room->size.height; }
			if (gridPos2.z < 0) { gridPos2.z = 0; } if (gridPos2.z > room->size.depth)  { gridPos2.z = room->size.depth;  }
			Assert(gridPos2.x >= gridPos1.x);
			Assert(gridPos2.y >= gridPos1.y);
			Assert(gridPos2.z >= gridPos1.z);
			v3i gridSpaceSize = gridPos2 - gridPos1;
			if (gridSpaceSize != Vec3i_Zero)
			{
				TileGenerateColliders(room, tilePntr, gridPos1, gridSpaceSize);
			}
		}
	}
	
	Assert(tower->rooms != nullptr);
	Assert(room >= tower->rooms && room < tower->rooms + tower->numRooms);
	u32 roomIndex = (u32)(room - tower->rooms);
	
	for (u32 eIndex = 0; eIndex < tower->numElevators; eIndex++)
	{
		Elevator_t* elevator = &tower->elevators[eIndex];
		if (roomIndex >= elevator->startFloor && roomIndex <= elevator->endFloor)
		{
			for (i32 yPos = 0; yPos < (roomIndex == elevator->endFloor ? 2 : room->colGridSize.height); yPos++)
			{
				for (i32 zPos = 0; zPos*COLLIDER_SIZE < elevator->size.height; zPos++)
				{
					for (i32 xPos = 0; xPos*COLLIDER_SIZE < elevator->size.width; xPos++)
					{
						v3i gridPos = NewVec3i(elevator->position.x/COLLIDER_SIZE + xPos, yPos, elevator->position.y/COLLIDER_SIZE + zPos);
						RoomFillCollider(room, gridPos);
					}
				}
			}
		}
	}
}

v3 RoomFindFloor(Room_t* room, v3 position)
{
	Assert(room != nullptr);
	
	v3i gridStart = Vec3Floori(position / (r32)COLLIDER_SIZE);
	if (gridStart.y < 0) { return position; }
	if (gridStart.y >= room->colGridSize.height) { gridStart.y = room ->colGridSize.height-1; }
	if (gridStart.x < 0 || gridStart.x >= room->colGridSize.width) { return NewVec3(position.x, 0, position.z); }
	if (gridStart.z < 0 || gridStart.z >= room->colGridSize.depth) { return NewVec3(position.x, 0, position.z); }
	v3 gridOffset = NewVec3(position.x - (gridStart.x * (r32)COLLIDER_SIZE), position.y - (gridStart.y * (r32)COLLIDER_SIZE), position.z - (gridStart.z * (r32)COLLIDER_SIZE));
	
	u32 iter = 0;
	for (v3i gridPos = gridStart; gridPos.y >= 0; gridPos.y--)
	{
		Collider_t* collider = RoomGetCollider(room, gridPos);
		if (collider != nullptr && collider->solid)
		{
			return NewVec3(position.x, (gridPos.y+1)*(r32)COLLIDER_SIZE, position.z);
		}
		iter++;
	}
	
	return NewVec3(position.x, -100, position.z); //return below bottom of the map at that pos
}

v3 RoomFindFooting(Room_t* room, v3 position, r32 radius)
{
	Assert(room != nullptr);
	
	r32 belowRadius = radius-SMALL_NUMBER;
	v3i minGridPos = Vec3Floori((position - NewVec3(belowRadius, 0, belowRadius)) / (r32)COLLIDER_SIZE);
	v3i maxGridPos = Vec3Floori((position + NewVec3(belowRadius, 0, belowRadius)) / (r32)COLLIDER_SIZE);
	//TODO: Clamp these values to the room grid?
	
	for (i32 yPos = minGridPos.y; yPos >= 0; yPos--)
	{
		bool foundResult = false;
		v3 result = Vec3_Zero;
		r32 resultMeetingDist = 0;
		for (i32 zPos = minGridPos.z; zPos <= maxGridPos.z; zPos++)
		{
			for (i32 xPos = minGridPos.x; xPos <= maxGridPos.x; xPos++)
			{
				v3i gridPos = NewVec3i(xPos, yPos, zPos);
				Collider_t* collider = RoomGetCollider(room, gridPos);
				if (collider != nullptr && collider->solid)
				{
					rec colliderRec = NewRec(gridPos.x*(r32)COLLIDER_SIZE, gridPos.z*(r32)COLLIDER_SIZE, COLLIDER_SIZE, COLLIDER_SIZE);
					v2 meetingPoint = Vec2_Zero;
					if (RecOverlapsCircle(colliderRec, NewVec2(position.x, position.z), belowRadius, &meetingPoint))
					{
						r32 meetingDist = Vec2Length(meetingPoint - NewVec2(position.x, position.z));
						if (!foundResult || meetingDist < resultMeetingDist)
						{
							resultMeetingDist = meetingDist;
							result = NewVec3(meetingPoint.x, (gridPos.y+1)*(r32)COLLIDER_SIZE, meetingPoint.y);
							foundResult = true;
						}
					}
				}
			}
		}
		
		if (foundResult)
		{
			return result;
		}
	}
	
	return NewVec3(position.x, -100, position.z); //return below bottom of the map at that pos
}

