/*
File:   game_view3d.cpp
Author: Taylor Robbins
Date:   09\12\2018
Description: 
	** Holds the functions that handle making a 3D projection view 
*/

void View3dInit(View3d_t* view, r32 fovAngleY, r32 zNear, r32 zFar)
{
	Assert(view != nullptr);
	ClearPointer(view);
	
	view->fovAngleY = fovAngleY;
	view->zNear = zNear;
	view->zFar = zFar;
	
	view->position = Vec3_Zero;
}

void View3dLookAt(View3d_t* view, v3 targetPos)
{
	Assert(view != nullptr);
	
	v3 forwardVec = targetPos - view->position;
	r32 horiDistance = Vec2Length(NewVec2(targetPos.x, targetPos.z) - NewVec2(view->position.x, view->position.z));
	
	view->rotationVert = AtanR32(forwardVec.y, horiDistance);
	view->rotationHori = AtanR32(forwardVec.z, forwardVec.x);
	
	while (view->rotationHori > 2*Pi32) { view->rotationHori -= 2*Pi32; }
	while (view->rotationHori < 0) { view->rotationHori += 2*Pi32; }
	if (view->rotationVert > Pi32/2 - 0.01f)
	{
		view->rotationVert = Pi32/2 - 0.01f;
	}
	if (view->rotationVert < -Pi32/2 + 0.01f)
	{
		view->rotationVert = -Pi32/2 + 0.01f;
	}
}

void View3dSync(View3d_t* view)
{
	Assert(view != nullptr);
	view->forwardVecHori = NewVec3(CosR32(view->rotationHori), 0, SinR32(view->rotationHori));
	view->rightVecHori = NewVec3(-view->forwardVecHori.z, 0, view->forwardVecHori.x); //TODO: Is this backwards?
	view->forwardVec = view->forwardVecHori*CosR32(view->rotationVert) + NewVec3(0, SinR32(view->rotationVert), 0);
	view->forwardVec = Vec3Normalize(view->forwardVec);
	view->upVector = Vec3Cross(view->forwardVec, Vec3_Up);
	view->upVector = Vec3Cross(view->upVector, view->forwardVec);
	
	// DEBUG_PrintLine("forwardVec: (%.2f, %.2f, %.2f) Length: %f", view->forwardVec.x, view->forwardVec.y, view->forwardVec.z, Vec3Length(view->forwardVec));
	
	// inline mat4 Mat4LookAt(const v3& position, const v3& lookAt, const v3& upVector)
	view->viewMatrix = Mat4LookAt(view->position, view->position + view->forwardVec*10, view->upVector);
	view->projMatrix = Mat4PerspectiveFOV(view->fovAngleY, WindowSize.width / WindowSize.height, view->zNear, view->zFar);
}

void View3dUpdate(View3d_t* view, bool doWasdMovement = false, bool doMouseMovement = false)
{
	Assert(view != nullptr);
	//NOTE: We need proper forwardVecHori and forwardVec values generated first for view movement to work
	if (doWasdMovement || doMouseMovement) { View3dSync(view); }
	
	if (doWasdMovement)
	{
		r32 moveSpeed = 2;
		if (ButtonDown(Button_Shift)) { moveSpeed *= 2; }
		if (ButtonDown(Button_Control)) { moveSpeed /= 15; }
		if (ButtonDown(Button_W))
		{
			view->position += view->forwardVecHori * moveSpeed;
		}
		if (ButtonDown(Button_A))
		{
			view->position -= view->rightVecHori * moveSpeed;
		}
		if (ButtonDown(Button_S))
		{
			view->position -= view->forwardVecHori * moveSpeed;
		}
		if (ButtonDown(Button_D))
		{
			view->position += view->rightVecHori * moveSpeed;
		}
		if (ButtonDown(Button_E))
		{
			view->position += Vec3_Up * moveSpeed;
		}
		if (ButtonDown(Button_Q))
		{
			view->position -= Vec3_Up * moveSpeed;
		}
	}
	
	if (doMouseMovement)
	{
		v2 delta = input->mousePosDelta;
		// DEBUG_PrintLine("Moved (%.1f, %.1f)", delta.x, delta.y);
		r32 rotSpeed = ToRadians(0.1f);
		view->rotationHori += delta.x * rotSpeed;
		view->rotationVert += -delta.y * rotSpeed;
	}
	
	while (view->rotationHori > 2*Pi32) { view->rotationHori -= 2*Pi32; }
	while (view->rotationHori < 0) { view->rotationHori += 2*Pi32; }
	if (view->rotationVert > Pi32/2 - 0.01f)
	{
		view->rotationVert = Pi32/2 - 0.01f;
	}
	if (view->rotationVert < -Pi32/2 + 0.01f)
	{
		view->rotationVert = -Pi32/2 + 0.01f;
	}
}

void View3dApply(View3d_t* view)
{
	Assert(view != nullptr);
	RcSetViewMatrix(view->viewMatrix);
	RcSetProjectionMatrix(view->projMatrix);
}
