/*
File:   app_;oading_functions.cpp
Author: Taylor Robbins
Date:   08\31\2018
Description: 
	** Includes a collection of functions that manage loading
	** various sorts of resources 
*/

// +--------------------------------------------------------------+
// |                           Textures                           |
// +--------------------------------------------------------------+
Texture_t CreateTexture(const u8* bitmapData, i32 width, i32 height, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	
	result.width = width;
	result.height = height;
	
	glGenTextures(1, &result.id);
	glBindTexture(GL_TEXTURE_2D, result.id);
	
	glTexImage2D(
		GL_TEXTURE_2D,    //bound texture type
		0,                //image level
		GL_RGBA,          //internal format
		width,            //image width
		height,           //image height
		0,                //border
		GL_RGBA,          //format
		GL_UNSIGNED_BYTE, //type
		bitmapData        //data
	);
	
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, pixelated ? GL_NEAREST_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, pixelated ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	return result;
}

Texture_t LoadTexture(const char* fileName, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	
	FileInfo_t textureFile = platform->ReadEntireFile(fileName);
	
	if (textureFile.content == nullptr)
	{
		textureFile = platform->ReadEntireFile(TEXTURES_FOLDER MISSING_TEXTURE_NAME);
		Assert(textureFile.content != nullptr);
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels, 4);
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	result = CreateTexture(imageData, width, height, pixelated, repeat);
	
	stbi_image_free(imageData);
	platform->FreeFileMemory(&textureFile);
	
	return result;
}

void DestroyTexture(Texture_t* texturePntr)
{
	Assert(texturePntr != nullptr);
	glDeleteTextures(1, &texturePntr->id);
	ClearPointer(texturePntr);
}

// +--------------------------------------------------------------+
// |                        Sprite Sheets                         |
// +--------------------------------------------------------------+
//TODO: This doesn't work if padding is 0.
SpriteSheet_t LoadSpriteSheet(const char* fileName, u32 padding, v2i numFrames, bool pixelated = true)
{
	SpriteSheet_t result = {};
	
	TempPushMark();
	FileInfo_t textureFile = platform->ReadEntireFile(fileName);
	
	i32 numChannels;
	v2i bitmapSize;
	u32* bitmapData = (u32*)stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&bitmapSize.width, &bitmapSize.height, &numChannels, 4);
	
	v2i paddingVec = NewVec2i(padding);
	v2i frameSize = NewVec2i(bitmapSize.width / numFrames.x, bitmapSize.height / numFrames.y);
	v2i newFrameSize = frameSize + paddingVec*2;
	v2i newSize = Vec2iMultiply(newFrameSize, numFrames);
	
	u32* newData = PushArray(TempArena, u32, newSize.width*newSize.height);
	memset(newData, 0x00, sizeof(u32)*newSize.width*newSize.height);
	for (i32 frameY = 0; frameY < numFrames.y; frameY++)
	{
		for (i32 frameX = 0; frameX < numFrames.x; frameX++)
		{
			v2i frame = NewVec2i(frameX, frameY);
			v2i framePos = Vec2iMultiply(frameSize, frame);
			v2i newFramePos = Vec2iMultiply(newFrameSize, frame) + paddingVec;
			
			for (i32 yPos = 0; yPos < frameSize.y; yPos++)
			{
				for (i32 xPos = 0; xPos < frameSize.x; xPos++)
				{
					v2i sourcePos = framePos + NewVec2i(xPos, yPos);
					u32* source = &bitmapData[(sourcePos.y*bitmapSize.width) + sourcePos.x];
					
					v2i newPos = newFramePos + NewVec2i(xPos, yPos);
					newData[(newPos.y * newSize.width) + newPos.x] = *source;
					
					if (xPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(0, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(0, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					
					if (xPos == 0 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == 0 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(-1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
				}
			}
		}
	}
	
	result.texture = CreateTexture((u8*)newData, newSize.width, newSize.height, pixelated, false);
	result.padding = paddingVec;
	result.frameSize = newFrameSize;
	result.innerFrameSize = newFrameSize - paddingVec*2;
	result.numFrames = numFrames;
	
	stbi_image_free((u8*)bitmapData);
	platform->FreeFileMemory(&textureFile);
	TempPopMark();
	
	return result;
}

void DestroySpriteSheet(SpriteSheet_t* sheetPntr)
{
	Assert(sheetPntr != nullptr);
	DestroyTexture(&sheetPntr->texture);
	ClearPointer(sheetPntr);
}

// +--------------------------------------------------------------+
// |                        Vertex Buffers                        |
// +--------------------------------------------------------------+
VertexBuffer_t CreateVertexBuffer(const Vertex_t* vertices, u32 numVertices)
{
	VertexBuffer_t result = {};
	result.numVertices = numVertices;
	
	glGenBuffers(1, &result.id);
	glBindBuffer(GL_ARRAY_BUFFER, result.id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * numVertices, vertices, GL_STATIC_DRAW);
	
	return result;
}

void DestroyVertexBuffer(VertexBuffer_t* bufferPntr)
{
	Assert(bufferPntr != nullptr);
	glDeleteBuffers(1, &bufferPntr->id);
	ClearPointer(bufferPntr);
}

// +--------------------------------------------------------------+
// |                        Frame Buffers                         |
// +--------------------------------------------------------------+
FrameBuffer_t CreateFrameBuffer(v2i size)
{
	FrameBuffer_t result = {};
	
	glGenFramebuffers(1, &result.id);
	glBindFramebuffer(GL_FRAMEBUFFER, result.id);
	
	result.texture.size = size;
	glGenTextures(1, &result.texture.id);
	glBindTexture(GL_TEXTURE_2D, result.texture.id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glGenRenderbuffers(1, &result.depthBuffId);
	glBindRenderbuffer(GL_RENDERBUFFER, result.depthBuffId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.width, size.height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, result.depthBuffId);
	
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result.texture.id, 0);
	GLenum drawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, drawBuffers);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		DEBUG_PrintLine("Failed to create framebuffer. Status = %d", status);
		Assert(false);
	}
	
	return result;
}

void DestroyFrameBuffer(FrameBuffer_t* frameBufferPntr)
{
	Assert(frameBufferPntr != nullptr);
	glDeleteTextures(1, &frameBufferPntr->texture.id);
	glDeleteRenderbuffers(1, &frameBufferPntr->depthBuffId);
	glDeleteFramebuffers(1, &frameBufferPntr->id);
	ClearPointer(frameBufferPntr);
}

// +--------------------------------------------------------------+
// |                           Shaders                            |
// +--------------------------------------------------------------+
Shader_t LoadShader(const char* vertShaderFileName, const char* fragShaderFileName)
{
	Shader_t result = {};
	GLint compiled;
	int logLength;
	char* logBuffer;
	
	Assert(true);
	
	FileInfo_t vertexShaderFile = platform->ReadEntireFile(vertShaderFileName);
	FileInfo_t fragmentShaderFile = platform->ReadEntireFile(fragShaderFileName);
	
	result.vertId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(result.vertId, 1, (const char* const*)&vertexShaderFile.content, NULL);
	glCompileShader(result.vertId);
	
	glGetShaderiv(result.vertId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.vertId, GL_INFO_LOG_LENGTH, &logLength);
	DEBUG_PrintLine("%s: Compiled %s : %d byte log",
		vertShaderFileName, compiled ? "Successfully" : "Unsuccessfully", logLength);
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.vertId, logLength, NULL, logBuffer);
		DEBUG_PrintLine("Log: \"%s\"", logBuffer);
	}
	
	result.fragId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(result.fragId, 1, (const char* const*)&fragmentShaderFile.content, NULL);
	glCompileShader(result.fragId);
	
	glGetShaderiv(result.fragId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.fragId, GL_INFO_LOG_LENGTH, &logLength);
	DEBUG_PrintLine("%s: Compiled %s : %d byte log",
		fragShaderFileName, compiled ? "Successfully" : "Unsuccessfully", logLength);
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.fragId, logLength, NULL, logBuffer);
		DEBUG_PrintLine("Log: \"%s\"", logBuffer);
	}
	
	platform->FreeFileMemory(&vertexShaderFile);
	platform->FreeFileMemory(&fragmentShaderFile);
	
	result.programId = glCreateProgram();
	glAttachShader(result.programId, result.fragId);
	glAttachShader(result.programId, result.vertId);
	glLinkProgram(result.programId);
	
	glGetProgramiv(result.programId, GL_LINK_STATUS, &compiled);
	glGetProgramiv(result.programId, GL_INFO_LOG_LENGTH, &logLength);
	DEBUG_PrintLine("Shader: Linked %s : %d byte log",
		compiled ? "Successfully" : "Unsuccessfully", logLength);
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetProgramInfoLog(result.programId, logLength, NULL, logBuffer);
		DEBUG_PrintLine("Log: \"%s\"", logBuffer);
	}
	
	result.locations.positionAttrib    = glGetAttribLocation(result.programId, "inPosition");
	result.locations.colorAttrib       = glGetAttribLocation(result.programId, "inColor");
	result.locations.texCoordAttrib    = glGetAttribLocation(result.programId, "inTexCoord");
	
	result.locations.worldMatrix       = glGetUniformLocation(result.programId, "WorldMatrix");
	result.locations.viewMatrix        = glGetUniformLocation(result.programId, "ViewMatrix");
	result.locations.projectionMatrix  = glGetUniformLocation(result.programId, "ProjectionMatrix");
	
	result.locations.texture           = glGetUniformLocation(result.programId, "Texture");
	result.locations.textureSize       = glGetUniformLocation(result.programId, "TextureSize");
	result.locations.ditherTexture     = glGetUniformLocation(result.programId, "DitherTexture");
	result.locations.sourceRectangle   = glGetUniformLocation(result.programId, "SourceRectangle");
	result.locations.sourceRecSkew     = glGetUniformLocation(result.programId, "SourceRecSkew");
	result.locations.maskRectangle     = glGetUniformLocation(result.programId, "MaskRectangle");
	result.locations.shiftVec          = glGetUniformLocation(result.programId, "ShiftVec");
	
	result.locations.primaryColor      = glGetUniformLocation(result.programId, "PrimaryColor");
	result.locations.secondaryColor    = glGetUniformLocation(result.programId, "SecondaryColor");
	result.locations.replaceColor1     = glGetUniformLocation(result.programId, "ReplaceColor1");
	result.locations.replaceColor2     = glGetUniformLocation(result.programId, "ReplaceColor2");
	result.locations.replaceColor3     = glGetUniformLocation(result.programId, "ReplaceColor3");
	result.locations.replaceColor4     = glGetUniformLocation(result.programId, "ReplaceColor4");
	
	result.locations.gradientEnabled   = glGetUniformLocation(result.programId, "GradientEnabled");
	result.locations.circleRadius      = glGetUniformLocation(result.programId, "CircleRadius");
	result.locations.circleInnerRadius = glGetUniformLocation(result.programId, "CircleInnerRadius");
	result.locations.saturation        = glGetUniformLocation(result.programId, "Saturation");
	result.locations.brightness        = glGetUniformLocation(result.programId, "Brightness");
	result.locations.time              = glGetUniformLocation(result.programId, "Time");
	
	result.locations.shadows           = glGetUniformLocation(result.programId, "Shadows");
	
	glGenVertexArrays(1, &result.vertexArray);
	glBindVertexArray(result.vertexArray);
	glEnableVertexAttribArray(result.locations.positionAttrib);
	glEnableVertexAttribArray(result.locations.colorAttrib);
	glEnableVertexAttribArray(result.locations.texCoordAttrib);
	
	return result;
}

void DestroyShader(Shader_t* shaderPntr)
{
	Assert(shaderPntr != nullptr);
	glDeleteVertexArrays(1, &shaderPntr->vertexArray);
	glDeleteProgram(shaderPntr->programId);
	glDeleteShader(shaderPntr->vertId);
	glDeleteShader(shaderPntr->fragId);
	ClearPointer(shaderPntr);
}

// +--------------------------------------------------------------+
// |                            Sounds                            |
// +--------------------------------------------------------------+
#if USE_OPEN_AL
Sound_t LoadSound(const char* fileName)
{
	Sound_t result = {};
	
	FileInfo_t wavFile = platform->ReadEntireFile(fileName);
	if (wavFile.content == nullptr)
	{
		Assert(false);
		return result;
	}
	
	WAV_Header_t* wavHeader = (WAV_Header_t*)wavFile.content;
	Assert(wavHeader->RIFFID == WAV_ChunkID_RIFF);
	Assert(wavHeader->WAVEID == WAV_ChunkID_WAVE);
	
	u32 sampleDataSize = 0;
	i16* sampleData = nullptr;
	bool foundFormat = false;
	
	for (RiffIterator_t iter = ParseChunkAt(wavHeader + 1, (u8*)(wavHeader+1) + wavHeader->size-4); 
		IsValid(iter);
		iter = NextChunk(iter))
	{
		switch (GetType(iter))
		{
			case WAV_ChunkID_fmt:
			{
				WAV_FormatChunk_t* format = (WAV_FormatChunk_t*)GetChunkData(iter);
				// Assert(format->nBlockAlign == sizeof(i16)*format->numChannels);
				
				if (format->formatTag != 0x01)
				{
					DEBUG_PrintLine("\"%s\" is not a PCM (non-compressed) WAV file!", fileName);
					return result;
				}
				
				result.frequency = format->numSamplesPerSecond;
				
				if (format->bitsPerSample == 16)
				{
					if (format->numChannels == 1)
					{
						result.format = AL_FORMAT_MONO16;
						result.channelCount = 1;
					}
					else if (format->numChannels == 2)
					{
						result.format = AL_FORMAT_STEREO16;
						result.channelCount = 2;
					}
					else
					{
						DEBUG_PrintLine("We don't support %u channel wav files!", format->numChannels);
						return result;
					}
				}
				else
				{
					DEBUG_PrintLine("We don't support %ubit wav files!", format->bitsPerSample);
					return result;
				}
				
				foundFormat = true;
			} break;
			
			case WAV_ChunkID_data:
			{
				sampleData = (i16*)GetChunkData(iter);
				sampleDataSize = GetChunkDataSize(iter);
			} break;
			
			default:
			{
				// DEBUG_PrintLine("Unknown chunk ID in wav file: \"%.4s\"", (char*)iter.pntr);
			};
		}
	}
	
	if (!foundFormat)
	{
		DEBUG_WriteLine("We did not find the format chunk in the WAV file");
		return result;
	}
	if (sampleData == nullptr || sampleDataSize == 0)
	{
		DEBUG_WriteLine("We did not find the data chunk in the WAV file");
		return result;
	}
	
	result.sampleCount = sampleDataSize / (result.channelCount*sizeof(i16));
	
	alGenBuffers(1, &result.id);
	
	alGetError();
	alBufferData(result.id, result.format, sampleData, sampleDataSize, result.frequency);
	ALCenum error = alGetError();
	if (error != AL_NO_ERROR)
	{
		DEBUG_PrintLine("Error after alBufferData: %d", error);
	}
	
	#if 0
	//Allocate the data into the memoryArena
	// i16* soundBuffer = PushArray(arena, i16, sampleDataSize);
	i16* soundBuffer = (i16*)malloc(sizeof(i16) * sampleDataSize);
	Assert(soundBuffer != nullptr);
	memcpy(soundBuffer, sampleData, sampleDataSize);
	
	if (channelCount == 1)
	{
		result.samples[0] = soundBuffer;
		result.samples[1] = nullptr;
	}
	else if (channelCount == 2)
	{
		result.samples[0] = soundBuffer;
		result.samples[1] = soundBuffer + result.sampleCount;
		
		for (u32 sampleIndex = 0; sampleIndex < result.sampleCount; sampleIndex++)
		{
			i16 right = soundBuffer[sampleIndex + 1];
			soundBuffer[sampleIndex + 1] = soundBuffer[(sampleIndex+1)*2];
			soundBuffer[(sampleIndex+1)*2] = right;
		}
		
		//TODO: Unswizzle right channel
		result.channelCount = 1;
	}
	else
	{
		//We don't support more channels right now
		Assert(false);
	}
	#endif
	
	platform->FreeFileMemory(&wavFile);
	
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	//TODO: Implement me!
}
#else
Sound_t LoadSound(const char* fileName)
{
	Sound_t result = {};
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	//Do nothing
}
#endif
