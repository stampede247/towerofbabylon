/*
File:   app_sound.h
Author: Taylor Robbins
Date:   08\31\2018
*/

#ifndef _APP_SOUND_H
#define _APP_SOUND_H

//appSound.h
#if !USE_OPEN_AL
#define PlayGameSound(sound, ...) nullptr
#define StopSoundInstance(instance)
#define StopSound(sound)
#define SetSoundVolume(instance, volume, ...)
#define GetMusicTransitionAmount() 1.0f
#define GetMusicTransitionVolumes(volume1, volume2)
#define QueueMusicChange(newSong, ...)
#define SoftQueueMusicChange(newSong, ...)
#define UpdateSounds()
#endif

#endif //  _APP_SOUND_H
