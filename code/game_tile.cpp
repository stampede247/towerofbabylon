/*
File:   game_tile.cpp
Author: Taylor Robbins
Date:   09\05\2018
Description: 
	** Holds the functions that handle tasks related to World Tiles (Tile_t).
	** Tiles are the level geometry expressed in 3D AABB boxes rendered with sprites
*/

Tile_t* CreateTileBlock(Room_t* room, v3i position, v3i size, v2i sheetCell)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	result->position = position;
	result->size = size;
	result->flags = (TileFlag_Solid | TileFlag_TopFace | TileFlag_FrontFace);
	result->sourceRecTop = NewRec((r32)sheetCell.x * TILE_SIZE/2, (r32)sheetCell.y * TILE_SIZE/2, (r32)size.width, (r32)size.depth);
	result->sourceRecFront = NewRec((r32)sheetCell.x * TILE_SIZE/2, (r32)sheetCell.y * TILE_SIZE/2 + (r32)size.depth, (r32)size.width, (r32)size.height);
	
	return result;
}

Tile_t* CreateTileWall(Room_t* room, v3i position, v2i size, v2i sheetCell)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	result->position = position + NewVec3i(0, 0, -COLLIDER_SIZE);
	result->size = NewVec3i(size.width, size.height, COLLIDER_SIZE);
	result->flags = (TileFlag_Solid | TileFlag_FrontFace);
	result->sourceRecFront = NewRec((r32)sheetCell.x * TILE_SIZE/2, (r32)sheetCell.y * TILE_SIZE/2, (r32)size.width, (r32)size.height);
	
	return result;
}

Tile_t* CreateTileWallDecor(Room_t* room, v3i position, rec sourceRec)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	result->position = position + NewVec3i(0, 1, 0);
	result->size = NewVec3i((i32)sourceRec.width, (i32)sourceRec.height, 1);
	result->flags = (TileFlag_FrontFace);
	result->sourceRecFront = sourceRec;
	
	return result;
}

Tile_t* CreateTileFloor(Room_t* room, v3i position, v2i size, v2i sheetCell)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	result->position = position + NewVec3i(0, -COLLIDER_SIZE, 0);
	result->size = NewVec3i(size.width, COLLIDER_SIZE, size.height);
	result->flags = (TileFlag_Solid | TileFlag_TopFace);
	result->sourceRecTop = NewRec((r32)sheetCell.x * TILE_SIZE/2, (r32)sheetCell.y * TILE_SIZE/2, (r32)size.width, (r32)size.height);
	
	return result;
}

Tile_t* CreateTileFloorDecor(Room_t* room, v3i position, rec sourceRec)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	result->position = position + NewVec3i(0, 1, 1); //TODO: Make this based off heightRatio
	result->size = NewVec3i((i32)sourceRec.width, 1, (i32)sourceRec.height);
	result->flags = (TileFlag_TopFace);
	result->sourceRecTop = sourceRec;
	
	return result;
}

Tile_t* CreateTileTree(Room_t* room, v3i bottomCenter, v2i colliderSize, rec sourceRec)
{
	Assert(room != nullptr);
	
	Tile_t* result = RoomAddTile(room);
	ClearPointer(result);
	if ((colliderSize.x%2) == 0)
	{
		DEBUG_PrintLine("Creating tree (%d, %d)", colliderSize.width/2, colliderSize.height);
		result->position = Vec3Roundi(NewVec3(bottomCenter.x / (r32)COLLIDER_SIZE, 0, bottomCenter.z / (r32)COLLIDER_SIZE)) * COLLIDER_SIZE;
		result->position -= NewVec3i(colliderSize.width/2, 0, colliderSize.width/2) * COLLIDER_SIZE;
	}
	else
	{
		result->position = bottomCenter - NewVec3i((bottomCenter.x % COLLIDER_SIZE) + (colliderSize.width/2)*COLLIDER_SIZE, 0, bottomCenter.z % COLLIDER_SIZE + (colliderSize.width/2)*COLLIDER_SIZE);
	}
	result->position.y = bottomCenter.y;
	
	result->size = NewVec3i(colliderSize.width * COLLIDER_SIZE, colliderSize.height * COLLIDER_SIZE, colliderSize.width * COLLIDER_SIZE);
	result->flags = (TileFlag_Solid | TileFlag_FrontFace);
	result->sourceRecFront = sourceRec;
	result->frontOffset = NewVec2i(bottomCenter.x - (i32)(sourceRec.width/2), bottomCenter.z) - NewVec2i(result->position.x, result->position.z + result->size.depth);
	
	return result;
}

//TODO: This should generate different kinds of colliders for different kinds of tiles
void TileGenerateColliders(Room_t* room, Tile_t* tile, v3i gridPos, v3i gridSize)
{
	for (i32 yPos = gridPos.y; yPos < gridPos.y+gridSize.height; yPos++)
	{
		for (i32 zPos = gridPos.z; zPos < gridPos.z+gridSize.depth; zPos++)
		{
			for (i32 xPos = gridPos.x; xPos < gridPos.x+gridSize.width; xPos++)
			{
				v3i currentPos = NewVec3i(xPos, yPos, zPos);
				RoomFillCollider(room, currentPos);
			}
		}
	}
}

void DrawTile(Room_t* room, Tile_t* tile, v3 roomPos, r32 transitionAlpha, bool draw3d)
{
	Assert(tile != nullptr);
	v3 tilePos = roomPos + NewVec3(tile->position);
	v3 tileSize = NewVec3(tile->size);
	
	RcBindTexture(&app->tileSheet1);
	RcDrawTexturedTop(tilePos + NewVec3(0, (r32)tile->size.height, 0), tile->sourceRecTop.size, ColorTransparent(transitionAlpha), tile->sourceRecTop);
	RcDrawTexturedFace(tilePos + NewVec3((r32)tile->frontOffset.x, 0, (r32)tile->size.depth + (r32)tile->frontOffset.y), tile->sourceRecFront.size, ColorTransparent(transitionAlpha), tile->sourceRecFront);
	if (draw3d && IsFlagSet(tile->flags, TileFlag_FrontFace))
	{
		RcDrawTextureDebugCubePart(roomPos + NewVec3(tile->position), NewVec3(tile->size), ColorTransparent(transitionAlpha), tile->sourceRecFront);
	}
	if (false && !IsFlagSet(tile->flags, TileFlag_TopFace) && IsFlagSet(tile->flags, TileFlag_Solid))
	{
		RcDrawCubeOutline(NewVec3(tile->position), NewVec3(tile->size), ColorOrangeRed, 1);
	}
}

void RoomDrawTiles(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d)
{
	Assert(room != nullptr);
	
	for (u32 tIndex = 0; tIndex < room->numTiles; tIndex++)
	{
		Tile_t* tile = &room->tiles[tIndex];
		DrawTile(room, tile, roomPos, transitionAlpha, draw3d);
	}
}

void DrawCollider(Collider_t* collider, v3 position, v3 size, r32 transitionAlpha, bool draw3d)
{
	Assert(collider != nullptr);
	if (collider->solid)
	{
		RcDrawCubeFaces(position, size, collider->sides, ColorCyan, PureBlue, ColorLime, PureGreen, PureYellow, ColorOrange);
	}
}

void RoomDrawColliders(Room_t* room, v3 roomPos, r32 transitionAlpha, bool draw3d)
{
	Assert(room != nullptr);
	
	if (room->colliders != nullptr)
	{
		for (i32 yPos = 0; yPos < room->colGridSize.height; yPos++)
		{
			for (i32 zPos = 0; zPos < room->colGridSize.depth; zPos++)
			{
				for (i32 xPos = 0; xPos < room->colGridSize.width; xPos++)
				{
					v3i currentPos = NewVec3i(xPos, yPos, zPos);
					Collider_t* collider = RoomGetCollider(room, currentPos);
					if (collider != nullptr && collider->solid)
					{
						DrawCollider(collider, roomPos + NewVec3((r32)xPos, (r32)yPos, (r32)zPos)*(r32)COLLIDER_SIZE, NewVec3(COLLIDER_SIZE, COLLIDER_SIZE, COLLIDER_SIZE), transitionAlpha, draw3d);
					}
				}
			}
		}
	}
}

void TowerAddElevatorTiles(Tower_t* tower, Room_t* room)
{
	Assert(tower != nullptr);
	Assert(room >= tower->rooms && room < tower->rooms + tower->numRooms);
	u32 roomIndex = (u32)(room - tower->rooms);
	for (u32 eIndex = 0; eIndex < tower->numElevators; eIndex++)
	{
		Elevator_t* elevator = &tower->elevators[eIndex];
		if (roomIndex == elevator->startFloor)
		{
			DEBUG_PrintLine("Adding elevator start at (%d, %d)", elevator->position.x, elevator->position.y);
			Tile_t* newTile = CreateTileWall(room, NewVec3i(elevator->position.x, TILE_SIZE/2, elevator->position.y + elevator->size.height), NewVec2i(elevator->size.width, TILE_SIZE*2), NewVec2i(16, 4));
		}
		else if (roomIndex == elevator->endFloor)
		{
			DEBUG_PrintLine("Adding elevator end at (%d, %d)", elevator->position.x, elevator->position.y);
			Tile_t* newTile = CreateTileBlock(room, NewVec3i(elevator->position.x, TILE_SIZE/2, elevator->position.y), NewVec3i(elevator->size.width, TILE_SIZE, elevator->size.height), NewVec2i(18, 4));
		}
		else if (roomIndex > elevator->startFloor && roomIndex < elevator->endFloor)
		{
			DEBUG_PrintLine("Adding elevator mid at (%d, %d)", elevator->position.x, elevator->position.y);
			Tile_t* newTile = CreateTileWall(room, NewVec3i(elevator->position.x, TILE_SIZE/2, elevator->position.y + elevator->size.height), NewVec2i(elevator->size.width, TILE_SIZE*2), NewVec2i(20, 4));
		}
	}
}
